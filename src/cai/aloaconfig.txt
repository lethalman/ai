[ALOA]
# GAME
playerBlack=1
# firstBlack ignored for network games
firstBlack=1

# NETWORK
network=false
# ip to connect
ip=127.0.0.1
# either connect or listen to port
port=12345

# LIMITS
openbook=true
pondering=false
debug=false
timer=true
# maxelapsed ignored without timer
maxelapsed=819
# maxdepth ignored with timer
maxdepth=5
maxtransposition=50000
maxnodes=5000

# FORWARD PRUNING
forwardPruning=false
# if forward pruning is disabled, the features below are ignored
mcc=true
mca=true
nmh=true
lmr=true
