#include <stdio.h>
#include <glib.h>
#include <gio/gio.h>
#include <math.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#ifdef _WIN32
#define IPPROTO_TCP 6
#define TCP_NODELAY 0x0001
extern int setsockopt (int sockfd, int level, int optname,
		       const void *optval, int optlen);
#else
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <linux/tcp.h>
#endif

typedef unsigned long long int uint64;
typedef signed long long int int64;


#define max(a,b) (a > b ? a : b)
#define min(a,b) (a > b ? b : a)
#define MAX_VALUE 65000
#define PV_NODE 0
#define CUT_NODE 1
#define ALL_NODE -1
#define stopSearch (timesUp || changedRoot)

typedef struct {
    GArray* moves[2];
    GArray* tacticalMoves[2];
    guint8* comDistancesWhite; // available only for the first 8 lines
    guint8* comDistancesBlack; // available only for the first 8 lines
    double mobilityWhite;
    double mobilityBlack;
} Configuration;

typedef struct {
    uint64 me;
    uint64 adv;
    int black;
    int refcount;
    int lowerbound;
    int upperbound;
    int depth;
    int pv;
    int standpat;
    gboolean cached;
    gint16* lines;
    int centralisationMe;
    int centralisationAdv;
    int stonesMe;
    int stonesAdv;
    int comIAxisMe;
    int comJAxisMe;
    int comIAxisAdv;
    int comJAxisAdv;
    int euler4Me, euler4Adv;
    int q3Me, q3Adv;
    int q4Me, q4Adv;
} Node;

typedef struct {
    int moves[12*8];
    int size;
} ChildrenList;

typedef struct {
    int black;
    uint64 me;
    uint64 adv;
} BookEntry;

static gboolean USE_TIMER = FALSE;
static int MAX_DEPTH = 5;
static glong MAX_ELAPSED = 900;
static int MAX_TRANSPOSITION_ENTRIES = 100000;
static int MAX_MEMORY_NODES = 2000000;
static gboolean DEBUG = TRUE;
static gboolean PONDERING = TRUE;
static gboolean MULTICUT_CUT = FALSE;
static gboolean MULTICUT_ALL = FALSE;
static gboolean NULLMOVE_HEURISTIC = FALSE;
static gboolean LATE_MOVE_REDUCTIONS = FALSE;

static int euler4Table[512];
static int q3Table[512];
static int q4Table[512];

static Configuration linesConf[32][6561];
static volatile gboolean networkPlay;

static GMainLoop* loop;
static guint32 timer_source = 0;
static int firstBlack;
static int playerBlack;
static int sideToMoveMultiplier;
static Node* root;
static int adversaryMove;
static volatile gboolean timesUp = FALSE;
static volatile gboolean changedRoot = FALSE;
static GMutex* exchangerLock;
static GCond* exchangerCond;
static gint exchanged = 0;
static GSocket* networkSocket;
static int nodeCount = 0;
static int leafCount = 0;
static int qCount = 0;
static int moveCount = 0;
static const double weights[] = {3, 0.19320688423639118, -0.22532208357000463, 1.978249618209707, 0.15843100589666084, 0.3262643765178706, 1.037848193346331, 1.5461326883590238, 4.746392718104192, 0.1238797320125999};
static int reachedDepth;
static int historyHeuristic[64*64];
static int butterflyHeuristic[64*64];

/* LINES POOL */
static gint16** linesPool;
static int linesPoolTop = 0;

static gint16* createLines () {
    if (linesPoolTop == 0) {
	return g_slice_alloc (sizeof (gint16) * 32);
    } else {
	return linesPool[--linesPoolTop];
    }
}

static void releaseLines (gint16* c) {
    if (linesPoolTop == MAX_MEMORY_NODES) {
	g_slice_free1 (sizeof (gint16) * 32, c);
    } else {
	linesPool[linesPoolTop++] = c;
    }
}

/* NODE POOL */
static Node** nodePool;
static int nodePoolTop = 0;

static Node* createNode (int black, uint64 me, uint64 adv) {
    Node* res;
    if (nodePoolTop == 0) {
	res = g_slice_alloc (sizeof (Node));
    } else {
	res = nodePool[--nodePoolTop];
    }
    res->black = black;
    res->me = me;
    res->adv = adv;
    res->cached = FALSE;
    res->standpat = res->depth = res->pv = 0;
    res->refcount = 1;
    res->lowerbound = -MAX_VALUE;
    res->upperbound = MAX_VALUE;
    res->lines = NULL;
    return res;
}

static void node_unref (Node* this) {
    if (--(this->refcount) == 0) {
	if (this->lines != NULL) {
	    releaseLines (this->lines);
	    this->lines = NULL;
	}
	if (nodePoolTop == MAX_MEMORY_NODES) {
	    g_slice_free1 (sizeof (Node), this);
	} else {
	    nodePool[nodePoolTop++] = this;
	}
    }
}

/* CHILDRENLIST POOL */
static ChildrenList** childrenPool;
static int childrenPoolTop = 0;

static ChildrenList* createChildrenList (void) {
    ChildrenList* res;
    if (TRUE || childrenPoolTop == 0) {
	res = g_slice_alloc (sizeof (ChildrenList));
    } else {
	res = childrenPool[--childrenPoolTop];
    }
    res->size = 0;
    return res;
}

static void releaseChildrenList (ChildrenList* c) {
    if (childrenPoolTop == MAX_MEMORY_NODES) {
	g_slice_free1 (sizeof (ChildrenList), c);
    } else {
	childrenPool[childrenPoolTop++] = c;
    }
}

// from the pdf
static const int centralisationTable[] = {
    -80, -25, -20, -20, -20, -20, -25, -80,
    -25, 10, 10, 10, 10, 10, 10, -25,
    -20, 10, 25, 25, 25, 25, 10, -20,
    -20, 10, 25, 50, 50, 25, 10, -20,
    -20, 10, 25, 50, 50, 25, 10, -20,
    -20, 10, 25, 25, 25, 25, 10, -20,
    -25, 10, 10, 10, 10, 10, 10, -25,
    -80, -25, -20, -20, -20, -20, -25, -80 };

static void printboard (uint64 b) {
    printf ("bitboard %llu\n", b);
    for (int i=0; i < 8; i++) {
	for (int j=0; j < 8; j++) {
	    printf (" %d", (int)((b >> (i*8+j))&1));
	}
	printf("\n");
    }
}

static const uint64 k1 = 0x5555555555555555ULL;
static const uint64 k2 = 0x3333333333333333ULL;
static const uint64 k4 = 0x0f0f0f0f0f0f0f0fULL;
static const uint64 h01 = 0x0101010101010101ULL;

static int popcount (uint64 x) {
    x -= (x >> 1) & k1;
    x = (x & k2) + ((x >> 2) & k2);
    x = (x + (x >> 4)) & k4;
    return (int)((x * h01) >> 56);
}

static char* moveToString (int move) {
    int from = move & 63;
    int to = (move >> 6) & 63;
    char fromcol = (char)((from%8) + 'A');
    char fromrow = (char)((7-from/8) + '1');
    char tocol = (char)((to%8) + 'A');
    char torow = (char)((7-to/8) + '1');
    return g_strdup_printf ("%c%c-%c%c", fromcol, fromrow, tocol, torow);
}

static uint64 transpose (uint64 b) {
    uint64 res = 0ULL;
    while (b != 0) {
	uint64 posbit = b & -b;
	b &= ~posbit;
	int pos = (int) log2 (posbit);
	int i = pos/8;
	int j = pos%8;
	res |= 1ULL << (j*8+i);
    }
    return res;
}

static GHashTable* transposition;
static GQueue* transposition_list;
static GHashTable* openingBook = NULL;

static GArray* sortedMoves (GArray* list) {
    int* r = g_new (int, list->len);
    int size = 0;
    for (int i=0; i < list->len; i++) {
	int move = g_array_index (list, int, i);
	int eval = move >> 16;

	// insertion sort
	int j=0;
	for (; j < size; j++) {
	    int val = r[j] >> 16;
	    if (eval > val) {
		// shift
		for (int k=size; k > j; k--) {
		    r[k] = r[k-1];
		}
		break;
	    }
	}
	r[j] = move;
	size++;
    }
    GArray* ret = g_array_new (FALSE, FALSE, sizeof (int));
    ret->data = (gchar*) r;
    ret->len = size;
    return ret;
}

static void fillMove (Configuration* c, int conf, GArray* moves, GArray* tmoves, int color, int rowstart, int colstart, int rowdir, int coldir, int from, int to, gboolean border, int source, int dest, int dir) {
    int sourcecolor = (conf/(int)pow (3, source))%3;
    int destcolor = (conf/(int)pow (3, dest))%3;
    if (destcolor == color) {
	return;
    }
    for (int j=source+dir; j != dest; j+=dir) {
	int middle = (conf/(int)pow (3, j))%3;
	if (middle != 0 && middle != color) {
	    // adversary in the middle
	    return;
	}
    }
    double mobility = 1;
    int sourcepos = (rowstart+(source-from)*rowdir)*8 + (colstart+(source-from)*coldir);
    int destpos = (rowstart+(dest-from)*rowdir)*8 + (colstart+(dest-from)*coldir);
    int move = sourcepos | (destpos << 6);
    int eval = -centralisationTable[sourcepos] + centralisationTable[destpos];
    gboolean tmove = FALSE;
    if (destcolor != 0) { // capture
	move |= 1 << 12;
	mobility *= 2;
	eval += centralisationTable[destpos];
	eval += 150;
	tmove = TRUE;
    }
    if (TRUE) {
	if (dest-1 >= from && dest-1 != source) {
	    int near_ = (conf/(int)pow (3, dest-1))%3;
	    if (near_ == color) {
		// possible grouping
		eval += 50;
		tmove = TRUE;
	    }
	}
	if (dest+1 <= to && dest+1 != source) {
	    int near_ = (conf/(int)pow (3, dest+1))%3;
	    if (near_ == color) {
		// possible grouping
		eval += 50;
		tmove = TRUE;
	    }
	    }
    }
    if (border || dest == from || dest == to) {
	mobility /= 2;
	eval -= 100;
    } else if (!border && (dest == from+1 || dest == to-1)) {
	//eval += 50;
    }
    if (sourcecolor == 1) {
	c->mobilityWhite += mobility;
    } else {
	c->mobilityBlack += mobility;
    }

    move |= eval << 16;
    g_array_append_val (moves, move);
    if (tmove) {
	g_array_append_val (tmoves, move);
    }
}

static void fillMoves (Configuration* c, int conf, GArray* moves, GArray* tmoves, int color, int rowstart, int colstart, int rowdir, int coldir, int from, int to, gboolean border) {
    // count stones
    int stones = 0;
    for (int i=from; i <= to; i++) {
	int value = (conf/(int)pow (3, i))%3;
	if (value > 0) {
	    stones++;
	}
    }
    for (int source=from; source <= to; source++) {
	int value = (conf/(int)pow (3, source))%3;
	if (value == color) {
	    // compute possible moves
	    if (source-stones >= from) {
		fillMove (c, conf, moves, tmoves, color, rowstart, colstart, rowdir, coldir, from, to, border, source, source-stones, -1);
	    }
	    if (source+stones <= to) {
		fillMove (c, conf, moves, tmoves, color, rowstart, colstart, rowdir, coldir, from, to, border, source, source+stones, 1);
	    }
	}
    }
}

static void fillConf (Configuration* c, int conf, GArray* wmoves, GArray* bmoves, GArray* twmoves, GArray* tbmoves, int rowstart, int colstart, int rowdir, int coldir, int from, int to, gboolean border) {
    // white=1, black=2
    fillMoves (c, conf, wmoves, twmoves, 1, rowstart, colstart, rowdir, coldir, from, to, border);
    fillMoves (c, conf, bmoves, tbmoves, 2, rowstart, colstart, rowdir, coldir, from, to, border);
}

static guint node_hash (gconstpointer ptr) {
    const Node* n = ptr;
    return (guint)((n->me | n->adv) ^ (~(n->me | n->adv) >> 32));
}

static gboolean node_equal (gconstpointer a, gconstpointer b) {
    const Node* n1 = a;
    const Node* n2 = b;
    return (n1->black == n2->black && n1->me == n2->me && n1->adv == n2->adv);
}

static guint bookentry_hash (gconstpointer ptr) {
    const BookEntry* n = ptr;
    return (guint)((n->me | n->adv) ^ (~(n->me | n->adv) >> 32));
}

static gboolean bookentry_equal (gconstpointer a, gconstpointer b) {
    const BookEntry* n1 = a;
    const BookEntry* n2 = b;
    return (n1->black == n2->black && n1->me == n2->me && n1->adv == n2->adv);
}


// thanks tony
static gboolean checkConnected (uint64 x) {
    uint64 reached = x & -x; // find first
    while (TRUE) {
	uint64 f = reached | (reached << 8) | (reached >> 8);
	f |= ((f >> 1) & ~0x8080808080808080ULL) | ((f << 1) & ~0x101010101010101ULL);
	f &= x;
	if (f == reached) {
	    return f == x;
	}
	reached = f;
    }
}

static void exchange (void) {
    g_mutex_lock (exchangerLock);
    if (exchanged == 1) {
	exchanged = 2;
	g_cond_signal (exchangerCond);
    } else if (exchanged == 0) {
	exchanged = 1;
	while (exchanged != 2) {
	    g_cond_wait (exchangerCond, exchangerLock);
	}
	exchanged = 0;
    }
    g_mutex_unlock (exchangerLock);
}

static void node_print (Node* this) {
    printf ("---\n");
    printf ("Bitboard: me=%lluULL; adv=%lluULL\n", this->me, this->adv);
    printf ("Euler: me=%d; adv=%d\n", this->euler4Me/4, this->euler4Adv/4);
    printf ("Q3: me=%d, adv=%d\n", this->q3Me, this->q3Adv);
    printf ("Q4: me=%d, adv=%d\n", this->q4Me, this->q4Adv);
    printf ("COM: me=(%d, %d), adv=(%d, %d)\n", this->comIAxisMe/this->stonesMe, this->comJAxisMe/this->stonesMe, this->comIAxisAdv/this->stonesAdv, this->comJAxisAdv/this->stonesAdv);
    for (int i=0; i < 8; i++) {
	for (int j=0; j < 8; j++) {
	    uint64 player = ((this->me >> (i*8+j)) & 1) | (((this->adv >> (i*8+j)) & 1) << 1);
	    printf (" %c", (player==0?'.':((((player&1)^this->black)==0?'X':'O'))));
	}
	printf ("\n");
    }
}

static void node_ref (Node* this) {
    (this->refcount)++;
}

static Node* transposition_get (Node* x) {
    return g_hash_table_lookup (transposition, x);
}

static void transposition_put (Node* x) {
    // remove eldest
    node_ref (x);
    int size = g_queue_get_length (transposition_list);
    if (size > MAX_TRANSPOSITION_ENTRIES) {
	Node* n;
	if (g_queue_peek_tail (transposition_list) != root) {
	    n = g_queue_pop_tail (transposition_list);
	} else {
	    n = g_queue_pop_nth (transposition_list, size-2);
	}
	g_hash_table_remove (transposition, n);
	node_unref (n);
    }
    g_hash_table_replace (transposition, x, x);
    g_queue_push_head (transposition_list, x);
}

static void applyQuadDiff (Node* this, gboolean tome, uint64 board, int pos) {
    int i = pos/8;
    int j = pos%8;
    if (j == 0) { board = (board & 0x7F7F7F7F7F7F7F7FULL) << 1; ++pos; }
    else if (j == 7) { board = (board & 0xFEFEFEFEFEFEFEFEULL) >> 1; --pos; }
    if (i == 0) { board <<= 8; pos += 8; }
    board >>= pos-9;

    int quads = (int)(board & 7);
    quads |= (int)((board >> 5) & 56);
    quads |= (int)((board >> 10) & 448);

    if (tome) {
	this->euler4Me += euler4Table[quads];
	this->q3Me += q3Table[quads];
	this->q4Me += q4Table[quads];
    } else {
	this->euler4Adv += euler4Table[quads];
	this->q3Adv += q3Table[quads];
	this->q4Adv += q4Table[quads];
    }
}

static Node* node_applymove (const Node* this, int move) {
    int from = move & 63;
    int to = (move >> 6) & 63;
    uint64 movemask = (1ULL << from) | (1ULL << to);
    Node* child = createNode (1-this->black, this->adv & ~movemask, this->me ^ movemask);
    moveCount++;
    Node* e = transposition_get (child);
    if (e != NULL) {
	node_ref (e);
	node_unref (child);
	return e;
    }

    // ME
    // source configuration
    child->lines = createLines ();
    memcpy (child->lines, this->lines, sizeof(gint16)*32);
    int ifrom = from/8;
    int jfrom = from%8;
    int ifrompow = (this->black+1)*(int)pow (3, ifrom);
    int jfrompow = (this->black+1)*(int)pow (3, jfrom);
    child->lines[ifrom] -= jfrompow;
    child->lines[jfrom+8] -= ifrompow;
    child->lines[(7-ifrom+jfrom)%8+16] -= jfrompow;
    child->lines[(ifrom+jfrom)%8+24] -= ifrompow;
    int ito = to/8;
    int jto = to%8;
    int itopow = (int)pow (3, ito);
    int jtopow = (int)pow (3, jto);
    short torowconf = child->lines[ito];
    short tocolconf = child->lines[jto+8];
    short tomdconf = child->lines[(7-ito+jto)%8+16];
    short tosdconf = child->lines[(ito+jto)%8+24];

    // centralisation
    child->centralisationAdv = this->centralisationMe - centralisationTable[from] + centralisationTable[to];
    // quads
    child->euler4Adv = this->euler4Me;
    child->q3Adv = this->q3Me;
    child->q4Adv = this->q4Me;
    applyQuadDiff (child, FALSE, this->me, from);
    applyQuadDiff (child, FALSE, this->me & ~(1ULL << from), to);
    // com
    child->comIAxisAdv = this->comIAxisMe - (from/8) + (to/8);
    child->comJAxisAdv = this->comJAxisMe - (from%8) + (to%8);
    child->stonesAdv = this->stonesMe;

    // ADVERSARY
    child->euler4Me = this->euler4Adv;
    child->q3Me = this->q3Adv;
    child->q4Me = this->q4Adv;
    if (this->adv != child->me) { // capture
	// dest configuration
	torowconf -= (2-this->black)*jtopow;
	tocolconf -= (2-this->black)*itopow;
	tomdconf -= (2-this->black)*jtopow;
	tosdconf -= (2-this->black)*itopow;
	// centralisation
	child->centralisationMe = this->centralisationAdv - centralisationTable[to];
	// quads
	applyQuadDiff (child, TRUE, this->adv, to);
	// com
	child->comIAxisMe = this->comIAxisAdv - (to/8);
	child->comJAxisMe = this->comJAxisAdv - (to%8);
	child->stonesMe = this->stonesAdv - 1;
    } else {
	child->centralisationMe = this->centralisationAdv;
	child->comIAxisMe = this->comIAxisAdv;
	child->comJAxisMe = this->comJAxisAdv;
	child->stonesMe = this->stonesAdv;
    }
    // dest configuration
    child->lines[ito] = (short)(torowconf + (this->black+1)*jtopow);
    child->lines[jto+8] = (short)(tocolconf + (this->black+1)*itopow);
    child->lines[(7-ito+jto)%8+16] = (short)(tomdconf + (this->black+1)*jtopow);
    child->lines[(ito+jto)%8+24] = (short)(tosdconf + (this->black+1)*itopow);

    return child;
}

static gboolean timerFired (gpointer userdata) {
    timesUp = TRUE;
    return FALSE;
}

static void stopTimer (void) {
    if (timer_source != 0) {
	g_source_remove (timer_source);
    }
    timer_source = 0;
}

static void startTimer (void) {
    stopTimer ();
    timesUp = FALSE;
    timer_source = g_timeout_add (MAX_ELAPSED, timerFired, NULL);
}

static void addChild (ChildrenList* r, int move) {
    // Relative History Heuristic
    move = (move & 0xFFF) | (((move >> 16) + 100*historyHeuristic[move & 0xFFF]/(2+butterflyHeuristic[move & 0xFFF])) << 16);
    int eval = move >> 16;

    // insertion sort
    int i=0;
    for (; i < r->size; i++) {
	int val = (r->moves[i] >> 16);
	if (eval > val) {
	    // shift
	    for (int j=r->size; j > i; j--) {
		r->moves[j] = r->moves[j-1];
	    }
	    break;
	}
    }
    r->moves[i] = move;
    r->size++;
}


static ChildrenList* node_children (Node* this) {
    ChildrenList* r = createChildrenList ();
    for (int line=0; line < 32; line++) {
	GArray* moves = linesConf[line][this->lines[line]].moves[this->black];
	for (int i=0; i < moves->len; i++) {
	    int move = g_array_index (moves, int, i);
	    addChild (r, move);
	}
    }
    return r;
}

static ChildrenList* node_tacticalChildren (Node* this) {
    ChildrenList* r = createChildrenList ();
    for (int line=0; line < 32; line++) {
	GArray* moves = linesConf[line][this->lines[line]].tacticalMoves[this->black];
	for (int i=0; i < moves->len; i++) {
	    int move = g_array_index (moves, int, i);
	    addChild (r, move);
	}
    }
    return r;
}

static double normalize (double me, double adv) {
    if (me == adv) {
	return 0;
    }
    return (me - adv)/(fabs(me) + fabs(adv));
}

static int evaluate (Node* this) {
    if (this->standpat != 0) {
	return this->standpat;
    }
    const int stonesMe = this->stonesMe;
    const int stonesAdv = this->stonesAdv;

    const double quads3 = normalize (this->q3Me, this->q3Adv);
    const double quads4 = normalize (this->q4Me, this->q4Adv);
    const double connectedness = normalize (1.0/max (1, this->euler4Me/4), 1.0/max (1, this->euler4Adv/4));
    const int sidetomove = (1 - (firstBlack ^ this->black))*sideToMoveMultiplier;

    // centralisation
    double centralisation = normalize (this->centralisationMe/(double)stonesMe, (this->centralisationAdv/(double)stonesAdv));

    // com
    int icom = this->comIAxisMe / stonesMe;
    int jcom = this->comJAxisMe / stonesMe;
    const int comMe = icom*8+jcom;
    icom = this->comIAxisAdv / stonesAdv;
    jcom = this->comJAxisAdv / stonesAdv;
    const int comAdv = icom*8+jcom;
    
    // com centralisation
    const double comCentralisation = normalize (centralisationTable[comMe], centralisationTable[comAdv]);

    // mobility + com distances
    int wcom = comMe;
    int bcom = comAdv;
    if (this->black == 1) {
	wcom = comAdv;
	bcom = comMe;
    }
    int wdistances = 0;
    int bdistances = 0;
    double wmobility = 0;
    double bmobility = 0;
    for (int i=0; i < 32; i++) {
	Configuration* c = &linesConf[i][this->lines[i]];
	wmobility += c->mobilityWhite;
	bmobility += c->mobilityBlack;
	if (i < 8) {
	    wdistances += c->comDistancesWhite[wcom];
	    bdistances += c->comDistancesBlack[bcom];
	}
    }
    int distancesMe;
    int distancesAdv;
    double mobility;
    if (this->black == 1) {
	// precomputed values assume we are white
	mobility = normalize (bmobility/(double)stonesMe, wmobility/(double)stonesAdv);
	distancesMe = bdistances;
	distancesAdv = wdistances;
    } else {
	mobility = normalize (wmobility/(double)stonesMe, bmobility/(double)stonesAdv);
	distancesMe = wdistances;
	distancesAdv = bdistances;
    }
    // concentration
    int minDistances;
    if (stonesMe < 10) {
	minDistances = stonesMe-1;
    } else {
	minDistances = (stonesMe<<1)-10;
    }
    double concentrationMe = 1.0/max(1, distancesMe-minDistances);
    if (stonesAdv < 10) {
	minDistances = stonesAdv-1;
    } else {
	minDistances = (stonesAdv<<1)-10;
    }
    double concentrationAdv = 1.0/max(1, distancesAdv-minDistances);
    double concentration = normalize (concentrationMe, concentrationAdv);

    // material
    double material = normalize (stonesMe, stonesAdv);
    this->standpat = (int)(1000*(weights[1]*centralisation +
				 weights[2]*comCentralisation +
				 weights[3]*concentration +
				 weights[4]*quads3 +
				 weights[5]*quads4 +
				 weights[6]*connectedness +
				 weights[7]*mobility +
				 weights[8]*material +
				 weights[9]*sidetomove));
    return this->standpat;
}

static int qsearch (Node* this, int depth, int alpha, int beta) {
    qCount++;
    // first check adversary, regolamento unical
    if (this->euler4Adv/4 <= 1 && checkConnected (this->adv)) {
	return -MAX_VALUE;
    } else if (this->euler4Me/4 <= 1 && checkConnected (this->me)) {
	return MAX_VALUE;
    }
    int g = evaluate (this);
    if (depth == 0) {
	return g;
    }
    if (g >= beta) {
	return g;
    }
    alpha = max (alpha, g);
    ChildrenList* const children = node_tacticalChildren (this);
    const int size = children->size;
    int a = alpha;
    int b = beta;
    gboolean firstChild = TRUE;
    for (int i=0; i < size; i++) {
	Node* const child = node_applymove (this, children->moves[i]);

	int score = -qsearch (child, depth-1, -b, -a);
	if (stopSearch) {
	    node_unref (child);
	    break;
	}
	if (a < score && score < beta && !firstChild) {
	    score = -qsearch (child, depth-1, -beta, -a);
	    if (stopSearch) {
		node_unref (child);
		break;
	    }
	}
	moveCount--;
	node_unref (child);

	if (score > g) {
	    g = score;
	    a = max (score, a);
	}
	if (beta <= a) {
	    break;
	}
	firstChild = FALSE;
	b = a + 1;
    }
    releaseChildrenList (children);

    return g;
}

static Node* nullmove (Node* this) {
    Node* child = createNode (1-this->black, this->adv, this->me);
    Node* e = transposition_get (this);
    if (e != NULL) {
	node_ref (e);
	node_ref (child);
	return e;
    }
    child->lines = createLines ();
    memcpy (child->lines, this->lines, 32*sizeof (gint16));
    child->centralisationMe = this->centralisationAdv;
    child->centralisationAdv = this->centralisationMe;
    child->stonesMe = this->stonesAdv;
    child->stonesAdv = this->stonesMe;
    child->comIAxisMe = this->comIAxisAdv;
    child->comJAxisMe = this->comJAxisAdv;
    child->comIAxisAdv = this->comIAxisMe;
    child->comJAxisAdv = this->comJAxisMe;
    child->euler4Me = this->euler4Adv;
    child->euler4Adv = this->euler4Me;
    child->q3Me = this->q3Adv;
    child->q3Adv = this->q3Me;
    child->q4Me = this->q4Adv;
    child->q4Adv = this->q4Me;
    return child;
}

static int negascout (Node* this, int depth, int alpha, int beta, int nodeType) {
    nodeCount++;

    if (depth == 0) {
	leafCount++;
	return qsearch (this, (int)weights[0], alpha, beta);
    } else if ((this->euler4Adv/4 <= 1 && checkConnected (this->adv)) || moveCount/2 > 100) {
	return -MAX_VALUE;
    } else if (this->euler4Me/4 <= 1 && checkConnected (this->me)) {
	return MAX_VALUE;
    }

    // transposition table lookup
    if (this->depth >= depth) {
	// good hit
	if (this->lowerbound >= beta || this->lowerbound == this->upperbound) {
	    return this->lowerbound;
	}
	if (this->upperbound <= alpha) {
	    return this->upperbound;
	}
	alpha = max (alpha, this->lowerbound);
	beta = min (beta, this->upperbound);
    }

    // NULL MOVE
    if (NULLMOVE_HEURISTIC && nodeType != PV_NODE && depth >= 4) {
	// enabled for search depth >= 4
	Node* const child = nullmove (this);
	const int score = -negascout (child, depth-3, -beta, -alpha, -nodeType);
	node_unref (child);
	if (stopSearch) {
	    return score;
	}
	if (score >= beta) {
	    return beta;
	}
    }

    ChildrenList* const children = node_children (this);
    const int size = children->size;
    if (size == 0) {
	releaseChildrenList (children);
	return -MAX_VALUE;
    }

    if (MULTICUT_CUT && nodeType == CUT_NODE && depth >= 4) {
	// MULTI CUT - CUT NODES, M=10, C=3, R=2
	// enabled since search depth >= 5
	int c = 0, m = 0;
	for (int i=0; i < size && m < 10; i++, m++) {
	    Node* const child = node_applymove (this, children->moves[i]);

	    int score = -negascout (child, depth-3, -beta, -alpha, -nodeType);
	    moveCount--;
	    node_unref (child);
	    if (stopSearch) {
		releaseChildrenList (children);
		return score;
	    }
	    if (score >= beta && ++c >= 5) {
		releaseChildrenList (children);
		return beta;
	    }
	}
    } else if (MULTICUT_ALL && nodeType == ALL_NODE && depth >= 4) {
	// MULTI CUT - ALL NODES, M=10, C=5, R=2
	// enabled since search depth >= 6
	int c = 0, m = 0;
	for (int i=0; i < size && m < 10; i++, m++) {
	    Node* const child = node_applymove (this, children->moves[i]);

	    int score = -negascout (child, depth-3, -beta, -alpha, -nodeType);
	    moveCount--;
	    node_unref (child);
	    if (stopSearch) {
		releaseChildrenList (children);
		return score;
	    }
	    if (score >= beta) {
		// possible beta cutoff
		break;
	    }
	    if (score <= alpha && ++c >= 5) {
		releaseChildrenList (children);
		return alpha;
	    }
	}
    }

    int g = -MAX_VALUE;
    int a = alpha;
    int b = beta;
    int pv = this->pv;
    if (pv == 0) {
	pv = children->moves[0];
    }
    const int oldpv = pv;
    gboolean firstChild = TRUE;
    for (int i=0; i < size; i++) {
	const int move = firstChild ? pv : children->moves[i];
	if (!firstChild && move == oldpv) {
	    continue;
	}
	Node* const child = node_applymove (this, move);

	int score;
	if (LATE_MOVE_REDUCTIONS && depth >= 4 && i >= 5 && historyHeuristic[move & 0xFFF] == 0 && !firstChild && nodeType != PV_NODE) {
	    score = -negascout (child, depth-3, -b, -a, -nodeType);
	    if (stopSearch) {
		moveCount--;
		node_unref (child);
		releaseChildrenList (children);
		return g;
	    }
	} else {
	    score = a+1; // hack to ensure the search below
	}
	if (score > a) {
	    score = -negascout (child, depth-1, -b, -a, firstChild ? -nodeType : (nodeType == CUT_NODE ? ALL_NODE : CUT_NODE));
	    if (stopSearch) {
		moveCount--;
		node_unref (child);
		releaseChildrenList (children);
		return g;
	    }
	    if (!firstChild && ((a < score && score < beta) || (nodeType == PV_NODE && score == beta && beta == a+1))) {
		if (score == a+1) {
		    score = a;
		}
		score = -negascout (child, depth-1, -beta, -score, nodeType);
		if (stopSearch) {
		    moveCount--;
		    node_unref (child);
		    releaseChildrenList (children);
		    return g;
		}
	    }
	}
	moveCount--;
	node_unref (child);

	if (score > g) {
	    g = score;
	    pv = move;
	    a = max (a, g);
	}
	if (beta <= a) {
	    historyHeuristic[move & 0xFFF]+=depth;
	    break;
	} else {
	    butterflyHeuristic[move & 0xFFF]+=depth;
	}
	if (firstChild) {
	    firstChild = FALSE;
	    i = -1;
	}
	b = a + 1;
    }
    releaseChildrenList (children);

    // transposition table storing
    if (!this->cached) {
	// first time seen
	transposition_put (this);
	this->cached = TRUE;
	this->depth = depth;
    } else {
	// bypass the hashmap
	if (depth < this->depth) {
	    // worsen information, don't store
	    return g;
	} else {
	    this->depth = depth;
	}
    }
    if (g <= alpha) {
	this->upperbound = g;
	this->lowerbound = -MAX_VALUE;
    } else if (g > alpha && g < beta) {
	this->lowerbound = this->upperbound = g;
    } else {
	this->lowerbound = g;
	this->upperbound = MAX_VALUE;
    }
    this->pv = pv;

    return g;
}

static int idnegascout (Node* this) {
    int d = 2;
    int g = -MAX_VALUE;
    int oldMoveCount = moveCount;
    if (this->depth > 2 && (this->lowerbound == this->upperbound)) {
	d = this->depth+1;
	g = this->lowerbound;
    }
    while (USE_TIMER || d <= MAX_DEPTH) {
	int tmp = negascout (this, d, -MAX_VALUE, MAX_VALUE, PV_NODE);
	if (stopSearch) {
	    reachedDepth = d-1;
	    break;
	}
	g = tmp;
	d++;
    }
    moveCount = oldMoveCount;
    return g;
}

static gpointer search_thread (gpointer userdata) {
    if (root->black != playerBlack) {
	// waiting for user input
	if (!networkPlay) {
	    if (DEBUG) {
		node_print (root);
	    }
	    printf ("X0-X0: \n");
	}
	exchange ();
	// apply adversary move
	Node* tmp = root;
	root = node_applymove (tmp, adversaryMove);
	node_unref (tmp);
    } else if (USE_TIMER) {
	startTimer ();
    }

    BookEntry bookEntry;
    Node* predicted = NULL;
    int predictionHits = 0;
    int bookHits = 0;
    while (TRUE) {
	Node* tmproot = root;
	changedRoot = FALSE;

	if (DEBUG && tmproot == predicted) {
	    printf ("=== PREDICTION HIT %d\n", ++predictionHits);
	}
	if (predicted != NULL) {
	    node_unref (predicted);
	    predicted = NULL;
	}

	// first check adversary
	ChildrenList* children = node_children (tmproot);
	if (children->size == 0 || checkConnected (tmproot->adv)) {
	    printf ("WE LOST!\n");
	    break;
	} else if (checkConnected (tmproot->me)) {
	    printf ("WE WON!\n");
	    break;
	} else if (moveCount / 2 >= 100) {
	    printf ("DRAW!\n");
	    break;
	}
	releaseChildrenList (children);

	int nextMove;

	// lookup the opening book
	bookEntry.black = tmproot->black;
	bookEntry.me = tmproot->me;
	bookEntry.adv = tmproot->adv;
	int bookMove = GPOINTER_TO_INT (g_hash_table_lookup (openingBook, &bookEntry));
	if (bookMove != 0) {
	    // OPENING BOOK
	    nextMove = bookMove;
	    if (DEBUG) {
		printf ("=== OPENING BOOK HIT %d\n", (++bookHits));
	    }
	} else {
	    // SEARCH
	    int64 start = 0LL;
	    if (DEBUG) {
		printf ("=== SEARCHING\n");
		node_print (tmproot);
		nodeCount = 0;
		leafCount = 0;
		qCount = 0;
		start = g_get_real_time ()/1000;
	    }

	    // SEARCH ALGORITHM
	    int result = idnegascout (tmproot);

	    if (DEBUG) {
		printf ("Negascout (%d): %lld\n", result, g_get_real_time ()/1000 - start);
		if (USE_TIMER) {
		    printf ("Reached depth: %d\n", reachedDepth);
		}
		printf ("Visited Nodes=%d; Leafs=%d; Quiet=%d\n", nodeCount, leafCount, qCount);
	    }

	    nextMove = tmproot->pv;
	}

	gboolean beforePondering = FALSE;
	if (!timesUp) {
	    if (PONDERING && moveCount/2 > 0) {
		// don't ponder the first move
		Node* tmp = tmproot;
		tmproot = node_applymove (tmproot, nextMove);
		node_unref (tmp);
		beforePondering = TRUE;
		if (tmproot->pv == 0) {
		    bookEntry.black = tmproot->black;
		    bookEntry.me = tmproot->me;
		    bookEntry.adv = tmproot->adv;
		    tmproot->pv = GPOINTER_TO_INT (g_hash_table_lookup (openingBook, &bookEntry));
		}
		if (tmproot->pv != 0) {
		    predicted = node_applymove (tmproot, tmproot->pv);
		    idnegascout (predicted);
		    moveCount--;
		    if (DEBUG) {
			printf ("[BEFORE MOVE PONDERING] Reached depth: %d\n", reachedDepth);
		    }
		} else {
		    stopTimer ();
		}
	    } else {
		stopTimer ();
	    }
	}

	if (networkPlay) {
	    char* str = moveToString (nextMove);
	    char* with_lineend = g_strdup_printf ("%s\n", str);
	    int to_send = strlen (with_lineend);
	    ssize_t sent = 0;
	    GError* e = NULL;
	    while (sent < to_send) {
		ssize_t cur = g_socket_send (networkSocket, with_lineend+sent, to_send-sent, NULL, &e);
		if (cur < 0) {
		    g_message (e->message);
		    networkPlay = FALSE;
		}
		sent += cur;
	    }
	    free (str);
	    free (with_lineend);
	}
	if (!beforePondering) {
	    // better call applymove after sending the move
	    Node* tmp = tmproot;
	    tmproot = node_applymove (tmproot, nextMove);
	    node_unref (tmp);
	}
	if (DEBUG || !networkPlay) {
	    char* str = moveToString (nextMove);
	    printf ("=== MOVE (%s)\n", str);
	    free (str);
	    printf ("=== NEW BOARD:\n");
	    node_print (tmproot);
	}

	// first check us
	children = node_children (tmproot);
	if (children->size == 0 || checkConnected (tmproot->adv)) {
	    printf ("WE WON!\n");
	    break;
	} else if (checkConnected (tmproot->me)) {
	    printf ("WE LOST!\n");
	    break;
	} else if (moveCount / 2 >= 100) {
	    printf ("DRAW!\n");
	    break;
	} else {
	    root = tmproot;
	    printf ("X0-X0: \n");
	}
	releaseChildrenList (children);

	if (PONDERING) {
	    if (tmproot->pv == 0) {
		bookEntry.black = tmproot->black;
		bookEntry.me = tmproot->me;
		bookEntry.adv = tmproot->adv;
		bookMove = GPOINTER_TO_INT (g_hash_table_lookup (openingBook, &bookEntry));
		if (bookMove != 0) {
		    tmproot->pv = bookMove;
		}
	    }
	    if (tmproot->pv != 0) {
		if (!beforePondering) {
		    // pondering before move already calculated the predicted node
		    predicted = node_applymove (tmproot, tmproot->pv);
		}
		timesUp = FALSE;
		if (USE_TIMER && !networkPlay) {
		    startTimer ();
		}
		idnegascout (predicted);
		moveCount--;
		if (DEBUG) {
		    printf ("[AFTER MOVE PONDERING] Reached depth: %d\n", reachedDepth);
		}
	    }
	}

	// waiting for input
	exchange ();
	// apply adversary move
	root = node_applymove (tmproot, adversaryMove);
	node_unref (tmproot);
    }
    exit (0);
    return NULL;
}

static void computeLines (Node* this) {
    this->lines = createLines ();
    memset (this->lines, 0, sizeof(gint16)*32);
    for (int i=0; i < 8; i++) {
	for (int j=0; j < 8; j++) {
	    gint16 color = 0;
	    int pos = i*8+j;
	    if ((this->me & (1ULL << pos)) != 0) {
		color = (gint16)(this->black+1);
	    } else if ((this->adv & (1ULL << pos)) != 0) {
		color = (gint16)(2-this->black);
	    }
	    this->lines[i] += color*(short)pow(3,j);
	    this->lines[j+8] += color*(short)pow(3,i);
	    this->lines[(7-i+j)%8+16] += color*(short)pow(3,j);
	    this->lines[(i+j)%8+24] += color*(short)pow(3,i);
	}
    }
}

static void computeQuads2 (Node* this, gboolean tome, uint64 me) {
    int e = 0;
    int q3 = 0;
    int q4 = 0;
    for (int i=0; i < 7; i++) {
	if (popcount ((me >> i) & 3) == 1) {
	    e++;
	}
	if (popcount ((me >> (7*8+i)) & 3) == 1) {
	    e++;
	}
	if (popcount ((me >> (i*8)) & 257) == 1) {
	    e++;
	}
	if (popcount ((me >> (i*8+7)) & 257) == 1) {
	    e++;
	}
	for (int j=0; j < 7; j++) {
	    uint64 quad = (me >> (i*8+j)) & 0x303;
	    int count = popcount (quad);
	    if (count == 1) {
		e++;
	    } else if (count == 3) {
		e--;
		q3++;
	    } else if (count == 4) {
		q4++;
	    } else if (quad == 513 || quad == 258) {
		e -= 2;
	    }
	}
    }
    e += popcount (me & (-9151314442816847743LL));
    if (tome) {
	this->euler4Me = e;
	this->q3Me = q3;
	this->q4Me = q4;
    } else {
	this->euler4Adv = e;
	this->q3Adv = q3;
	this->q4Adv = q4;
    }
}

static void computeQuads (Node* this) {
    computeQuads2 (this, TRUE, this->me);
    computeQuads2 (this, FALSE, this->adv);
}

static void computeComMe (Node* this) {
    this->comIAxisMe = 0;
    this->comJAxisMe = 0;
    this->stonesMe = popcount (this->me);
    uint64 tmp = this->me;
    while (tmp != 0) {
	// find our first occupied position
	uint64 posbit = tmp & -tmp;
	// unset the position for the next cycle
	tmp &= ~posbit;
	int pos = (int)log2 (posbit);
	int i = pos/8;
	int j = pos%8;
	this->comIAxisMe += i;
	this->comJAxisMe += j;
    }
}

static void computeComAdv (Node* this) {
    this->comIAxisAdv = 0;
    this->comJAxisAdv = 0;
    this->stonesAdv = popcount (this->adv);
    uint64 tmp = this->adv;
    while (tmp != 0) {
	// find our first occupied position
	uint64 posbit = tmp & -tmp;
	// unset the position for the next cycle
	tmp &= ~posbit;
	int pos = (int)log2 (posbit);
	int i = pos/8;
	int j = pos%8;
	this->comIAxisAdv += i;
	this->comJAxisAdv += j;
    }
}

static void computeCentralisation (Node* this) {
    this->centralisationMe = 0;
    this->centralisationAdv = 0;

    uint64 tmp = this->me;
    while (tmp != 0) {
	// find our first occupied position
	uint64 posbit = tmp & -tmp;
	// unset the position for the next cycle
	tmp &= ~posbit;
	int pos = (int)log2 (posbit);
	this->centralisationMe += centralisationTable[pos];
    }

    tmp = this->adv;
    while (tmp != 0) {
	// find first occupied position by the adversary
	uint64 posbit = tmp & -tmp;
	// unset the position for the next cycle
	tmp &= ~posbit;
	int pos = (int)log2 (posbit);
	this->centralisationAdv += centralisationTable[pos];
    }
}

Node* findmove (Node* this, int move, gboolean quiet) {
    ChildrenList* c = node_children (this);
    releaseChildrenList (c);
    move &= 0xFFF;
    for (int k=0; c->size; k++) {
	//cerco il figlio corrispondente alla mossa desiderata
	if (move == (c->moves[k] & 0xFFF)) {
	    Node* child = node_applymove (this, move);
	    if (!quiet) {
		char* str = moveToString (move);
		printf (" giocatore MOVE (%s)\n", str);
		free (str);
		printf ("NEW BOARD:\n");
		node_print (child);
	    }
	    return child;
	}
    }
    return NULL;
}

static int stringToMove (char* s) {
    int fromcol = (int)(s[0] - 'A');
    int fromrow = (int)(7 - (s[1] - '1'));
    int tocol = (int)(s[3] - 'A');
    int torow = (int)(7 - (s[4] - '1'));
    return (fromrow*8+fromcol) | ((torow*8+tocol) << 6);
}

static gpointer keyboard_input_thread (gpointer userdata) {
    while (TRUE) {
	char str[100];
	if (fgets (str, sizeof (str), stdin) == NULL) {
	    perror ("Keyboard input: ");
	    exit (1);
	}
	int move = stringToMove (str);
	// check legal move without using root.children() to avoid race conditions
	gboolean isLegal = FALSE;
	for (int line=0; line < 32; line++) {
	    GArray* moves = linesConf[line][root->lines[line]].moves[root->black];
	    for (int i=0; i < moves->len; i++) {
		if ((g_array_index (moves, int, i) & 0xFFF) == move) {
		    isLegal = TRUE;
		    break;
		}
	    }
	    if (isLegal) {
		break;
	    }
	}
	if (!isLegal) {
	    printf ("ILLEGAL MOVE %s\n", str);
	    continue;
	}

	networkPlay = FALSE;
	if (USE_TIMER) {
	    startTimer ();
	}
	adversaryMove = move;
	changedRoot = TRUE;
	exchange ();
    }
    return NULL;
}

static gpointer network_input_thread (gpointer userdata) {
    GIOChannel* chan = g_io_channel_unix_new (g_socket_get_fd (networkSocket));
    while (TRUE) {
	char* str = NULL;
	GError* e = NULL;
	if (!networkPlay) {
	    break;
	}
	while (g_io_channel_read_line (chan, &str, NULL, NULL, &e) == G_IO_STATUS_AGAIN);
	if (str == NULL) {
	    if (e == NULL) {
		g_message ("EOF");
	    } else {
		g_message (e->message);
	    }
	    networkPlay = FALSE;
	    break;
	}
	if (USE_TIMER) {
	    startTimer ();
	}
	printf ("GOT MOVE %s\n", str);

	adversaryMove = stringToMove (str);
	changedRoot = TRUE;
	exchange ();
    }
    return NULL;
}

static void aloa_init (void) {
    // exchanger
    exchangerLock = g_mutex_new ();
    exchangerCond = g_cond_new ();

    // transposition table
    transposition = g_hash_table_new (node_hash, node_equal);
    transposition_list = g_queue_new ();

    // main loop
    loop = g_main_loop_new (NULL, 0);

    int etable[512];
    int q3table[512];
    int q4table[512];
    for (int i=0; i < 512; i++) {
	int evalue = 0;
	int q3value = 0;
	int q4value = 0;
	int quad = i & 27;
	if (popcount (quad) == 1) {
	    evalue++;
	} else if (popcount (quad) == 3) {
	    evalue--;
	    q3value++;
	} else if (quad == 17 || quad == 10) {
	    evalue -= 2;
	} else if (popcount (quad) == 4) {
	    q4value++;
	}

	quad = i & 54;
	if (popcount (quad) == 1) {
	    evalue++;
	} else if (popcount (quad) == 3) {
	    evalue--;
	    q3value++;
	} else if (quad == 34 || quad == 20) {
	    evalue -= 2;
	} else if (popcount (quad) == 4) {
	    q4value++;
	}

	quad = i & 216;
	if (popcount (quad) == 1) {
	    evalue++;
	} else if (popcount (quad) == 3) {
	    evalue--;
	    q3value++;
	} else if (quad == 136 || quad == 80) {
	    evalue -= 2;
	} else if (popcount (quad) == 4) {
	    q4value++;
	}

	quad = i & 432;
	if (popcount (quad) == 1) {
	    evalue++;
	} else if (popcount (quad) == 3) {
	    evalue--;
	    q3value++;
	} else if (quad == 272 || quad == 160) {
	    evalue -= 2;
	} else if (popcount (quad) == 4) {
	    q4value++;
	}

	etable[i] = evalue;
	q3table[i] = q3value;
	q4table[i] = q4value;
    }

    for (int i=0; i < 512; i++) {
	euler4Table[i] = -etable[i] + etable[i ^ 16];
	q3Table[i] = -q3table[i] + q3table[i ^ 16];
	q4Table[i] = -q4table[i] + q4table[i ^ 16];
    }

    // configurations
    int n = 6561;
    GArray* wmoves = g_array_new (FALSE, FALSE, sizeof (int));
    GArray* bmoves = g_array_new (FALSE, FALSE, sizeof (int));
    GArray* twmoves = g_array_new (FALSE, FALSE, sizeof (int));
    GArray* tbmoves = g_array_new (FALSE, FALSE, sizeof (int));
    for (int conf=0; conf < n; conf++) {
	for (int i=0; i < 8; i++) {
	    // rows
	    Configuration* c = &linesConf[i][conf];
	    wmoves->len = 0;
	    bmoves->len = 0;
	    twmoves->len = 0;
	    tbmoves->len = 0;
	    fillConf (c, conf, wmoves, bmoves, twmoves, tbmoves, i, 0, 0, 1, 0, 7, i == 0 || i == 7);
	    c->moves[0] = sortedMoves (wmoves);
	    c->moves[1] = sortedMoves (bmoves);
	    c->tacticalMoves[0] = sortedMoves (twmoves);
	    c->tacticalMoves[1] = sortedMoves (tbmoves);

	    // centre of mass distances
	    c->comDistancesWhite = g_new (guint8, 8*8);
	    c->comDistancesBlack = g_new (guint8, 8*8);
	    for (int com=0; com < 64; com++) {
		int icom = com/8;
		int jcom = com%8;
		int wdistances = 0;
		int bdistances = 0;
		for (int j=0; j < 8; j++) {
		    int value = (conf/(int)pow (3,j))%3;
		    int difrow = fabs (icom - i);
		    int difcol = fabs (jcom - j);
		    if (value == 1) {
			wdistances += max (difrow, difcol);
		    } else if (value == 2) {
			bdistances += max (difrow, difcol);
		    }
		}
		c->comDistancesWhite[com] = (guint8)wdistances;
		c->comDistancesBlack[com] = (guint8)bdistances;
	    }

	    // columns
	    c = &linesConf[i+8][conf];
	    wmoves->len = 0;
	    bmoves->len = 0;
	    twmoves->len = 0;
	    tbmoves->len = 0;
	    fillConf (c, conf, wmoves, bmoves, twmoves, tbmoves, 0, i, 1, 0, 0, 7, i == 0 || i == 7);
	    c->moves[0] = sortedMoves (wmoves);
	    c->moves[1] = sortedMoves (bmoves);
	    c->tacticalMoves[0] = sortedMoves (twmoves);
	    c->tacticalMoves[1] = sortedMoves (tbmoves);

	    // main diagonal
	    c = &linesConf[i+16][conf];
	    wmoves->len = 0;
	    bmoves->len = 0;
	    twmoves->len = 0;
	    tbmoves->len = 0;
	    fillConf (c, conf, wmoves, bmoves, twmoves, tbmoves, 7-i, 0, 1, 1, 0, i, FALSE);
	    fillConf (c, conf, wmoves, bmoves, twmoves, tbmoves, 0, i+1, 1, 1, i+1, 7, FALSE);
	    c->moves[0] = sortedMoves (wmoves);
	    c->moves[1] = sortedMoves (bmoves);
	    c->tacticalMoves[0] = sortedMoves (twmoves);
	    c->tacticalMoves[1] = sortedMoves (tbmoves);

	    // secondary diagonal
	    c = &linesConf[i+24][conf];
	    wmoves->len = 0;
	    bmoves->len = 0;
	    twmoves->len = 0;
	    tbmoves->len = 0;
	    fillConf (c, conf, wmoves, bmoves, twmoves, tbmoves, 0, i, 1, -1, 0, i, FALSE);
	    fillConf (c, conf, wmoves, bmoves, twmoves, tbmoves, i+1, 7, 1, -1, i+1, 7, FALSE);
	    c->moves[0] = sortedMoves (wmoves);
	    c->moves[1] = sortedMoves (bmoves);
	    c->tacticalMoves[0] = sortedMoves (twmoves);
	    c->tacticalMoves[1] = sortedMoves (tbmoves);
	}
    }
    g_array_free (wmoves, TRUE);
    g_array_free (bmoves, TRUE);
    g_array_free (twmoves, TRUE);
    g_array_free (tbmoves, TRUE);
}

static void aloa_init_root (void) {
    // root
    uint64 w = 36452665219186944ULL;
    uint64 b = 9079256848778920062ULL;
    moveCount = 0;
    memset (historyHeuristic, 0, sizeof(int)*64*64);
    memset (butterflyHeuristic, 0, sizeof(int)*64*64);
    if (firstBlack == 0) {
	root = createNode (firstBlack, w, b);
    } else {
	root = createNode (firstBlack, b, w);
    }
    computeLines (root);
    computeCentralisation (root);
    computeQuads (root);
    computeComMe (root);
    computeComAdv (root);
}

static void aloa_load_openbook (int firstBlack) {
    FILE* f = fopen ("aloaopeningbook.cdb", "rb");
    if (f == NULL) {
	perror ("Opening book: ");
	openingBook = g_hash_table_new (bookentry_hash, bookentry_equal);
	return;
    }
    openingBook = g_hash_table_new (bookentry_hash, bookentry_equal);
    while (!feof (f)) {
	int black = 0;
	uint64 me;
	uint64 adv;
	int move;
	size_t r = fread (&black, 1, 1, f);
	if (r == 0) {
	    // EOF
	    break;
	}
	if (r < 0) {
	    perror ("Opening book (black): ");
	    exit (1);
	}
	if (fread (&me, 8, 1, f) <= 0) {
	    perror ("Opening book (me): ");
	    exit (1);
	}
	me = GINT64_FROM_BE (me);
	if (fread (&adv, 8, 1, f) <= 0) {
	    perror ("Opening book (adv): ");
	    exit (1);
	}
	adv = GINT64_FROM_BE (adv);
	if (fread (&move, 4, 1, f) <= 0) {
	    perror ("Opening book (move): ");
	    exit (1);
	}
	move = GINT32_FROM_BE (move);
	BookEntry* e = g_slice_alloc (sizeof (BookEntry));
	if (firstBlack == 0) {
	    e->black = 1-black;
	    e->me = transpose (me);
	    e->adv = transpose (adv);
	    int from = move & 63;
	    int to = (move >> 6) & 63;
	    int i = from/8;
	    int j = from%8;
	    from = j*8+i;
	    i = to/8;
	    j = to%8;
	    to = j*8+i;
	    move = from | (to << 6);
	} else {
	    e->black = black;
	    e->me = me;
	    e->adv = adv;
	}
	g_hash_table_insert (openingBook, e, GINT_TO_POINTER (move));
    }
    fclose (f);
}

static void aloa_configure (void) {
    GError* e = NULL;
    GKeyFile* prop = g_key_file_new ();
    if (!g_key_file_load_from_file (prop, "aloaconfig.txt", G_KEY_FILE_NONE, &e)) {
	g_error (e->message);
    }

    playerBlack = g_key_file_get_integer (prop, "ALOA", "playerBlack", NULL);
    networkPlay = g_key_file_get_boolean (prop, "ALOA", "network", NULL);
    firstBlack = networkPlay ? 0 : g_key_file_get_integer (prop, "ALOA", "firstBlack", NULL);
    sideToMoveMultiplier = playerBlack == firstBlack ? 1 : -1;

    if (g_key_file_get_boolean (prop, "ALOA", "openbook", NULL)) {
	aloa_load_openbook (firstBlack);
    } else {
	openingBook = g_hash_table_new (bookentry_hash, bookentry_equal);
    }

    USE_TIMER = g_key_file_get_boolean (prop, "ALOA", "timer", NULL);
    MAX_DEPTH = g_key_file_get_integer (prop, "ALOA", "maxdepth", NULL);
    MAX_ELAPSED = g_key_file_get_integer (prop, "ALOA", "maxelapsed", NULL);
    MAX_TRANSPOSITION_ENTRIES = g_key_file_get_integer (prop, "ALOA", "maxtransposition", NULL);
    MAX_MEMORY_NODES = g_key_file_get_integer (prop, "ALOA", "maxnodes", NULL);
    DEBUG = g_key_file_get_boolean (prop, "ALOA", "debug", NULL);
    PONDERING = g_key_file_get_boolean (prop, "ALOA", "pondering", NULL);

    // pools
    nodePool = g_new (Node*, MAX_MEMORY_NODES);
    childrenPool = g_new (ChildrenList*, MAX_MEMORY_NODES);
    linesPool = g_new (gint16*, MAX_MEMORY_NODES);

    if (g_key_file_get_boolean (prop, "ALOA", "forwardPruning", NULL)) {
	MULTICUT_CUT = g_key_file_get_boolean (prop, "ALOA", "mcc", NULL);
	MULTICUT_ALL = g_key_file_get_boolean (prop, "ALOA", "mca", NULL);
	NULLMOVE_HEURISTIC = g_key_file_get_boolean (prop, "ALOA", "nmh", NULL);
	LATE_MOVE_REDUCTIONS = g_key_file_get_boolean (prop, "ALOA", "lmr", NULL);
    }
    
    aloa_init_root ();

    if (networkPlay) {
	char* ip = g_key_file_get_string (prop, "ALOA", "ip", NULL);
	int port = g_key_file_get_integer (prop, "ALOA", "port", NULL);
	if (firstBlack == playerBlack) {
	    // we begin
	    GSocket* s = g_socket_new (G_SOCKET_FAMILY_IPV4, G_SOCKET_TYPE_STREAM, G_SOCKET_PROTOCOL_TCP, &e);
	    if (s == NULL) {
		g_error (e->message);
	    }
	    // bind
	    if (!g_socket_bind (s, g_inet_socket_address_new (g_inet_address_new_any (G_SOCKET_FAMILY_IPV4), port), TRUE, &e)) {
		g_error (e->message);
	    }
	    // listen
	    if (!g_socket_listen (s, &e)) {
		g_error (e->message);
	    }
	    printf ("OPENED LISTEN PORT %d\n", port);
	    // accept
	    networkSocket = g_socket_accept (s, NULL, &e);
	    if (networkSocket == NULL) {
		g_error (e->message);
	    }
	    g_object_unref (s);
	} else {
	    printf ("CONNECTING TO %s:%d\n", ip, port);
	    networkSocket = g_socket_new (G_SOCKET_FAMILY_IPV4, G_SOCKET_TYPE_STREAM, G_SOCKET_PROTOCOL_TCP, &e);
	    if (networkSocket == NULL) {
		g_error (e->message);
	    }
	    // connect
	    if (!g_socket_connect (networkSocket, g_inet_socket_address_new (g_inet_address_new_from_string (ip), port), NULL, &e)) {
		g_error (e->message);
	    }
	    printf ("CONNECTED\n");
	}
    }
}

int main (void) {
    g_thread_init (NULL);
    g_type_init ();

    aloa_init ();
    aloa_configure ();

    if (networkPlay) {
	g_thread_create (network_input_thread, NULL, FALSE, NULL);
    }
    g_thread_create (keyboard_input_thread, NULL, FALSE, NULL);
    g_thread_create (search_thread, NULL, FALSE, NULL);

    printf ("READY\n");
    g_main_loop_run (loop);
}
