package ai;

import java.util.*;

class Node {
    static Random random = new Random ();
    static long[][] adjacent = new long[8*8][8];
    static long[][] masks = new long[8*8][4];
    static long[] flipped = new long[8*8];

    static long hmirror (long b) {
	return ((b << 7) & 0x8080808080808080L) |
	    ((b << 5) & 0x4040404040404040L) |
	    ((b << 3) & 0x2020202020202020L) |
	    ((b << 1) & 0x1010101010101010L);
    }

    static long vmirror (long b) {
	return ((b << (7*8)) & 0xFF00000000000000L) |
	    ((b << (5*8)) & 0xFF000000000000L) |
	    ((b << (3*8)) & 0xFF0000000000L) |
	    ((b << (1*8)) & 0xFF00000000L);
    }

    /* X....... */
    static final long hb1 = 1;
    /* .X......
     * XX...... */
    static final long hb2 = (1L<<1)|(1L<<(1*8))|(1L<<(1*8+1));
    /* ..X.....
     * ........
     * X.X..... */
    static final long hb3 = (1L<<2)|(1L<<(2*8))|(1L<<(2*8+2));

    static final long corners[] = new long[]{
	hb1|hmirror(hb1)|vmirror(hb1|hmirror(hb1)), 60,
	//	hb2|hmirror(hb2)|vmirror(hb2|hmirror(hb2)), -40,
	//	hb3|hmirror(hb3)|vmirror(hb3|hmirror(hb3)), 80,
    };

    static final long border = 0xFFL | 0x101010101010101L | 0x8080808080808080L | 0xFF00000000000000L;

    // assume white=0, black=1
    static int[][] zobrist = new int[8*8][2];
    // whether to use zobrist or our home-made hashing
    static boolean useZobrist = false;

    static void printboard (long b) {
	System.out.println ("bitboard: "+b);
	for (int i=0; i < 8; i++) {
	    for (int j=0; j < 8; j++) {
		System.out.print (" "+((b >> (i*8+j))&1));
	    }
	    System.out.println ();
	}
    }

    static final long k1 = 0x5555555555555555L;
    static final long k2 = 0x3333333333333333L;
    static final long k4 = 0x0f0f0f0f0f0f0f0fL;

    static final double base2 = Math.log (2);

    static int popcount (long x) {
	x -= ((x >> 1) & k1);
	x = (x & k2) + ((x >> 2) & k2);
	x = (x + (x >> 4)) & k4;
	x += x >> 8;
	x += x >> 16;
	x += x >> 32;
	return (int) x & 255;
    }

    static final int MAX_ENTRIES = 1000000;
    static LinkedHashMap<Node, Long> transposition = new LinkedHashMap<Node, Long> (2097143) {
	@Override
	protected boolean removeEldestEntry (Map.Entry eldest) {
	    return size() > MAX_ENTRIES;
	}
    };

    static {
	// zobrist keys
	for (int i=0; i < 64; i++) {
	    zobrist[i][0] = random.nextInt ();
	    zobrist[i][1] = random.nextInt ();
	}

	// attack masks
	for (int i=0; i < 8; i++) {
	    for (int j=0; j < 8; j++) {
		long[] m = masks[i*8+j];
		long[] a = adjacent[i*8+j];
		// down (first bit)
		if (i+2 < 8) {
		    for (int ii=i+2; ii < 8; ii++) m[0] |= 1L << (ii*8+j);
		    a[0] |= 1L << ((i+1)*8+j);
		}
		// up (last bit)
		if (i-2 >= 0) {
		    for (int ii=i-2; ii >= 0; ii--) m[0] |= 1L << (ii*8+j);
		    a[1] |= 1L << ((i-1)*8+j);
		}

		// right (first bit)
		if (j+2 < 8) {
		    for (int jj=j+2; jj < 8; jj++) m[1] |= 1L << (i*8+jj);
		    a[2] |= 1L << (i*8+j+1);
		}
		// left (last bit)
		if (j-2 >= 0) {
		    for (int jj=j-2; jj >= 0; jj--) m[1] |= 1L << (i*8+jj);
		    a[3] |= 1L << (i*8+j-1);
		}

		// main diagonal down (first bit)
		if (i+2 < 8 && j+2 < 8) {
		    for (int ii=i+2, jj=j+2; ii < 8 && jj < 8; ii++, jj++) m[2] |= 1L << (ii*8+jj);
		    a[4] |= 1L << ((i+1)*8+j+1);
		}
		// main diagonal up (last bit)
		if (i-2 >= 0 && j-2 >= 0) {
		    for (int ii=i-2, jj=j-2; ii >= 0 && jj >= 0; ii--, jj--) m[2] |= 1L << (ii*8+jj);
		    a[5] |= 1L << ((i-1)*8+j-1);
		}

		// secondary diagonal down (first bit)
		if (i+2 < 8 && j-2 >= 0) {
		    for (int ii=i+2, jj=j-2; ii < 8 && jj >= 0; ii++, jj--) m[3] |= 1L << (ii*8+jj);
		    a[6] |= 1L << ((i+1)*8+j-1);
		}
		// secondary diagonal up (last bit)
		if (i-2 >= 0 && j+2 < 8) {
		    for (int ii=i-2, jj=j+2; ii >= 0 && jj < 8; ii--, jj++) m[3] |= 1L << (ii*8+jj);
		    a[7] |= 1L << ((i-1)*8+j+1);
		}
	    }
	}
    }

    long me = 0L;
    long adv = 0L;
    boolean maximizing;
    int black;
    long possibleMoves;
    int hash = 0;
    
    Node (boolean maximizing, int black, long me, long adv) {
	this.maximizing = maximizing;
	this.black = black;
	this.me = me;
	this.adv = adv;
    }

    Node createChild (long flipped, long emptySquare) {
	Node n = new Node (!maximizing, 1-black, adv & ~flipped, me | flipped);
	if (useZobrist) {
	    n.hash = hash;
	    // skip last, we must not unset the adversary because it's empty
	    flipped &= ~emptySquare;
	    while (flipped != 0) {
		// find first bit
		long z = flipped & -flipped;
		// unset for the next cycle
		flipped &= ~z;
		// bit index
		int zbit = (int)(Math.log (((double)(z-1))+1) / base2);
		// unset adversary
		n.hash ^= zobrist[zbit][1-black];
		// set me
		n.hash ^= zobrist[zbit][black];
	    }
	    // set me at the empty square
	    int emptySquareBit = (int)(Math.log (((double)(emptySquare-1))+1) / base2);
	    n.hash ^= zobrist[emptySquareBit][black];
	}
	return n;
    }

    void computeMoves () {
	possibleMoves = possibleMoves (me, adv);
    }

    static long possibleMoves (long me, long adv) {
	long empty = (~me) & (~adv);
	long attacksmask = 0L;

	long tmp = me;
	while (tmp != 0) {
	    // find our first occupied position
	    long bitvalue = tmp & -tmp;
	    // unset the position for the next cycle
	    tmp &= ~bitvalue;
	    // bit index occupied by me
	    int bit = (int)(Math.log (((double)(bitvalue-1))+1) / base2);

	    long[] masks = Node.masks[bit];
	    long[] adjacent = Node.adjacent[bit];
	    for (int z=0; z < 4; z++) {
		long mask = masks[z];
		long match = mask & ~adv;
		// first bit
		long adj = adjacent[z*2];
		if ((adv & adj) != 0) {
		    // search first bit, adjacent is adversary!
		    // get mask for the first bit
		    long mask1 = mask & ~(bitvalue - 1);
		    // get first non-adversary square
		    long m = match & mask1;
		    // don't spend any time if it's zero
		    if (m != 0) {
			m &= -m; // gosper's hack
			if ((empty & m) != 0) {
			    // empty position, we can go there!
			    int attackbit = (int)(Math.log (((double)(m-1))+1) / base2);
			    long localflip = adj | (((m<<1)-1) & mask1);
			    // clear garbage
			    if ((attacksmask & m) == 0) {
				flipped[attackbit] = localflip;
				attacksmask |= m;
			    } else {
				flipped[attackbit] |= localflip;
			    }
			}
		    }
		}
		adj = adjacent[z*2+1];
		if ((adv & adj) != 0) {
		    // search last bit, adjacent is adversary!
		    // get mask for the last bit
		    long mask2 = mask & (bitvalue -1);
		    // get first non-adversary square
		    long m = match & mask2;
		    // don't spend any time if it's zero
		    if (m != 0) {
			m |= (m >> 1);
			m |= (m >> 2);
			m |= (m >> 4);
			m |= (m >> 8);
			m |= (m >> 16);
			m |= (m >> 32);
			m -= m >> 1;
			if ((empty & m) != 0) {
			    // empty position, we can go there!
			    int attackbit = (int)(Math.log (((double)(m-1))+1) / base2);
			    long localflip = adj | ((~(m-1)) & mask2);
			    // clear garbage
			    if ((attacksmask & m) == 0) {
				flipped[attackbit] = localflip;
				attacksmask |= m;
			    } else {
				flipped[attackbit] |= localflip;
			    }
			}
		    }
		}
	    }
   	}
	return attacksmask;
    }

    ArrayList<Node> children () {
	ArrayList<Node> r = new ArrayList<Node> ();
	long attacksmask = possibleMoves (me, adv);
	// "attacksmask" = possible attacks,
	while (attacksmask != 0) {
	    // find our first attack
	    long bitvalue = attacksmask & -attacksmask;
	    // unset the position for the next cycle
	    attacksmask &= ~bitvalue;
	    // bit index of the attack
	    int bit = (int)(Math.log (((double)(bitvalue-1))+1) / base2);
	    r.add (createChild (flipped[bit], bitvalue));
	}
	return r;
    }

    static int heuristicType = 0;

    int heuristic () {
	if (heuristicType == 1) {
	    long popme = popcount (me);
	    long popadv = popcount (adv);
	    if (popme > popadv) {
		return 1;
	    } else if (popme < popadv) {
		return -1;
	    } else {
		return 0;
	    }
	} else {
	    int strategic = 0;
	    for (int i=0; i < corners.length; i+=2) {
		long h1 = corners[i];
		long h2 = corners[i+1];
		strategic += (int)(popcount (me & h1) * h2);
		strategic -= (int)(popcount (adv & h1) * h2);
	    }
	    //	    strategic += (int)(popcount (me & border))*3;
	    //	    strategic -= (int)(popcount (adv & border))*3;
	    int mobility = popcount (possibleMoves);
	    mobility -= popcount (possibleMoves (adv, me));
	    return popcount (me) - popcount (adv) + strategic + mobility*3;
	}
    }

    void print () {
	System.out.println ("---");
	for (int i=0; i < 8; i++) {
	    for (int j=0; j < 8; j++) {
		System.out.print (" ");
		long player = ((me >> (i*8+j)) & 1) | (((adv >> (i*8+j)) & 1) << 1);
		System.out.print (" "+(player==0?'.':((((player&1)^black)==0?'X':'O'))));
	    }
	    System.out.println ();
	}
    }

    Node nextmove () {
	Long e = transposition.get (this);
	if (e == null) {
	    return null;
	}
	int pv = (int)((e >> 32) & 0x1FFFFFFF);
	List<Node> c = children();
	if (c.size () == 0) {
	    return null;
	}
	Node child = c.get (pv);
	long movemask = (~(me | adv)) ^ (~(child.me | child.adv));
	int movebit = (int)(Math.log (((double)(movemask-1))+1) / base2);
	int i = movebit/8;
	int j = movebit%8;
	System.out.println ("MOVE (i="+i+", j="+j+"):");
	printboard (movemask);
	System.out.println ("NEW BOARD:");
	child.print ();
	return child;
    }

    Node findmove (int i, int j) {
	for (Node child: children ()) {
	    long movemask = (~(me | adv)) ^ (~(child.me | child.adv));
	    int movebit = (int)(Math.log (((double)(movemask-1))+1) / base2);
	    int ii = movebit/8;
	    int jj = movebit%8;
	    if (i == ii && j == jj) {
		return child;
	    }
	}
	return null;
    }

    @Override
    public boolean equals (Object o) {
	Node n = (Node) o;
	return (me == n.me && adv == n.adv) || (me == n.adv && adv == n.me);
    }

    @Override
    public int hashCode () {
	if (useZobrist) {
	    return hash;
	} else {
	    // very stupid...?? half occupied squares XOR'd with the other half
	    return (int)(~(me | adv) ^ ~(~(me | adv) >> 32));
	}
    }
}

public class OthelloBit {
    static int negascout (Node n, int depth, int alpha, int beta) {
	if (depth == 0) {
	    return n.heuristic ();
	}

	int b = beta;
	boolean firstChild = true;
	for (Node child: n.children ()) {
	    int score = -negascout (child, depth-1, -b, -alpha);
	    if (score > alpha && score < beta && !firstChild) {
		score = -negascout (child, depth-1, -beta, -alpha);
	    }
	    alpha = Math.max (alpha, score);
	    if (beta <= alpha) {
		break;
	    }
	    firstChild = false;
	    b = alpha + 1;
	}
	return alpha;
    }

    static int mtdf (Node root, int f, int depth) {
	int g = f;
	int upper = Short.MAX_VALUE;
	int lower = -upper;
	while (lower < upper) {
	    int beta;
	    if (g == lower) {
		beta = g+1;
	    } else {
		beta = g;
	    }
	    g = alphabetaPV (root, depth, beta-1, beta, false);
	    if (g < beta) {
		upper = g;
	    } else {
		lower = g;
	    }
	}
	return g;
    }

    static int alphabetaPV (Node n, int depth, int alpha, int beta, boolean nullmove) {
	n.computeMoves ();
	if (n.possibleMoves == 0) {
	    if (!nullmove) {
		return -alphabetaPV (n.createChild (0, 0), depth, -beta, -alpha, true);
	    } else {
		return n.heuristic ();
	    }
	}

	if (depth == 0) {
	    return n.heuristic ();
	}

	// principal variation
	int pv = 0;

	// transposition table lookup
	Long e = Node.transposition.get (n);
	if (e != null) {
	    int drift = (int)((e >> 61) & 3L);
	    if (drift >= depth) {
		// we have good information here!
		int lowerbound = (short)(e & 0xffff);
		int upperbound = (short)((e >> 16) & 0xffff);
		if (((e >> 63) & 1) != n.black) {
		    // othello is symmetric, we can take advantage of the same position with either side to move
		    int lower = lowerbound;
		    lowerbound = -upperbound;
		    upperbound = -lower;
		}
		if (lowerbound >= beta || lowerbound == upperbound) {
		    return lowerbound;
		}
		if (upperbound <= alpha) {
		    return upperbound;
		}
		alpha = Math.max (alpha, lowerbound);
		beta = Math.min (beta, upperbound);
	    }
	    pv = (int)((e >> 32) & 0x1FFFFFFF);
	}

	List<Node> children = n.children ();
	int size = children.size ();
	boolean firstChild = true;
	int g = -Short.MAX_VALUE;
	int a = alpha;
	for (int i=pv; i < size; i++) {
	    if (i == pv && !firstChild) {
		// skip PV
		continue;
	    }
	    Node child = children.get (i);
	    int score = -alphabetaPV (child, depth-1, -beta, -a, false);
	    if (score > g) {
		g = score;
		pv = i;
		a = Math.max (a, g);
	    }
	    if (beta <= a) {
		break;
	    }
	    if (firstChild) {
		i=0;
		firstChild = false;
	    }
	}

	// transposition table storing
	if (g <= alpha) {
	    e = ((g & 0xffffL) << 16) | ((long)(-Short.MAX_VALUE) & 0xffffL);
	} else if (g > alpha && g < beta) {
	    e = ((g & 0xffffL) << 16) | (g & 0xffffL);
	} else {
	    e = (((long)(Short.MAX_VALUE) & 0xffffL) << 16) | (g & 0xffffL);
	}
	e |= ((long)pv) << 32;
	e |= ((long)n.black) << 63;
	e |= ((long)depth) << 61;
	Node.transposition.put (n, e);

	return g;
    }

    static int negascoutPV (Node n, int depth, int alpha, int beta) {
	if (depth == 0) {
	    return n.heuristic ();
	}

	// principal variation
	int pv = 0;

	// transposition table lookup
	Long e = Node.transposition.get (n);
	if (e != null) {
	    int drift = (int)((e >> 61) & 3L);
	    if (drift >= depth) {
		// we have good information here!
		int lowerbound = (short)(e & 0xffff);
		int upperbound = (short)((e >> 16) & 0xffff);
		if (((e >> 63) & 1) != n.black) {
		    // othello is symmetric, we can take advantage of the same position with either side to move
		    int lower = lowerbound;
		    lowerbound = -upperbound;
		    upperbound = -lower;
		}
		if (lowerbound >= beta || lowerbound == upperbound) {
		    return lowerbound;
		}
		if (upperbound <= alpha) {
		    return upperbound;
		}
		alpha = Math.max (alpha, lowerbound);
		beta = Math.min (beta, upperbound);
	    }
	    pv = (int)((e >> 32) & 0x1FFFFFFF);
	}

	List<Node> children = n.children ();
	int size = children.size ();
	if (size == 0) {
	    if (Node.popcount (n.me) > Node.popcount (n.adv)) {
		return Short.MAX_VALUE;
	    } else {
		return -Short.MAX_VALUE;
	    }
	}

	boolean firstChild = true;
	int g = -Short.MAX_VALUE;
	int a = alpha;
	int b = beta;
	for (int i=pv; i < size; i++) {
	    if (i == pv && !firstChild) {
		// skip PV
		continue;
	    }
	    Node child = children.get (i);
	    int score = -negascoutPV (child, depth-1, -b, -a);
	    if (score > a && score < beta && !firstChild) {
		score = -negascoutPV (child, depth-1, -beta, -a);
	    }
	    if (score > g) {
		g = score;
		pv = i;
		a = Math.max (a, g);
	    }
	    if (beta <= a) {
		break;
	    }
	    if (firstChild) {
		i=0;
		firstChild = false;
	    }
	    b = a + 1;
	}

	// transposition table storing
	if (g <= alpha) {
	    e = ((g & 0xffffL) << 16) | ((long)(-Short.MAX_VALUE) & 0xffffL);
	} else if (g > alpha && g < beta) {
	    e = ((g & 0xffffL) << 16) | (g & 0xffffL);
	} else {
	    e = (((long)(Short.MAX_VALUE) & 0xffffL) << 16) | (g & 0xffffL);
	}
	e |= ((long)pv) << 32;
	e |= ((long)n.black) << 63;
	e |= ((long)depth) << 61;
	Node.transposition.put (n, e);

	return g;
    }

    static int iidnegascout (Node root, int depth) {
	for (int i=4; i <= depth-2; i++) {
	    negascoutPV (root, i, -Short.MAX_VALUE, Short.MAX_VALUE);
	}
	int g = negascoutPV (root, depth, -Short.MAX_VALUE, Short.MAX_VALUE);
	return g;
    }

    static int iidmtdf (Node root, int depth) {
	int g = -Short.MAX_VALUE;
	int u = Short.MAX_VALUE;
	if (Node.heuristicType == 1) {
	    u = 1;
	}
	for (int i=4; i <= 7; i++) {
	    g = alphabetaPV (root, i, -u, u, false);
	}
	g = mtdf (root, g, depth);
	return g;
    }

    static int iidalphabeta (Node root, int depth) {
	int upper = Short.MAX_VALUE;
	if (Node.heuristicType == 1) {
	    upper = 1;
	}
	for (int i=4; i <= 7; i++) {
	    alphabetaPV (root, i, -upper, upper, false);
	}
	return alphabetaPV (root, depth, -upper, upper, false);
    }

    static int iidalphascout (Node root, int depth) {
	for (int i=4; i <= depth-2; i++) {
	    alphabetaPV (root, i, -Short.MAX_VALUE, Short.MAX_VALUE, false);
	}
	int g = negascoutPV (root, depth, -Short.MAX_VALUE, Short.MAX_VALUE);
	return g;
    }

    public static void main (String[] args) {
	// black move first
	long adv = 1L << (3*8+3);
	long me = 1L << (3*8+4);
	me |= 1L << (4*8+3);
	adv |= 1L << (4*8+4);
	Node root = new Node (true, 1, me, adv);
	/*
	root.hash ^= Node.zobrist[3*8+3][0];
	root.hash ^= Node.zobrist[3*8+4][1];
	root.hash ^= Node.zobrist[4*8+3][1];
	root.hash ^= Node.zobrist[4*8+4][0];*/

	final int maxdepth = 9;
	long start;
	int result;
	Node.useZobrist = false;
	Node.heuristicType = 0;

	Scanner sc = new Scanner (System.in);

	boolean passed = false;
	int depthleft = 60;
	while (true) {
	    System.out.println ("SEARCHING MOVE:");
	    root.print ();

	    // ITERATIVE ALPHABETA
	    start = System.currentTimeMillis ();
	    if (depthleft <= 15) {
		// exaustive search
		if (Node.heuristicType == 0) {
		    // clear old data
		    Node.transposition.clear ();
		}
		Node.heuristicType = 1;
		System.out.println ("EXAUSTIVE");
		result = iidalphabeta (root, depthleft+1);
	    } else {
		result = iidalphabeta (root, maxdepth);
	    }
	    System.out.println ("Alphabeta+IID ("+result+"): "+(System.currentTimeMillis()-start));
	    Node tmp = root.nextmove ();
	    if (tmp == null) {
		if (passed) {
		    System.out.println ("END GAME!");
		    break;
		}
		root = root.createChild (0, 0);
		passed = true;
		System.out.println ("WE PASS!");
	    } else {
		root = tmp;
		depthleft--;
		passed = false;
	    }

	    root.computeMoves ();
	    if (root.possibleMoves == 0) {
		// adversary pass
		if (passed) {
		    System.out.println ("END GAME!");
		    break;
		}
		root = root.createChild (0, 0);
		passed = true;
		System.out.println ("ADVERSARY PASS!");
		continue;
	    }
	    while (true) {
		System.out.println ("i j: ");
		int i = sc.nextInt ();
		int j = sc.nextInt ();
		tmp = root.findmove (i, j);
		if (tmp == null) {
		    System.out.println ("ILLEGAL MOVE");
		} else {
		    root = tmp;
		    depthleft--;
		    passed = false;
		    break;
		}
	    }
	}

	/*
	// NEGASCOUT
	Node.useZobrist = false;
	Node.transposition.clear ();
	// warm up
	negascout (root, depth, -Short.MAX_VALUE, Short.MAX_VALUE);
	start = System.currentTimeMillis ();
	result = negascout (root, depth, -Short.MAX_VALUE, Short.MAX_VALUE);
	System.out.println ("Negascout ("+result+"): "+(System.currentTimeMillis()-start));

	// ITERATIVE NEGASCOUT
	Node.useZobrist = false;
	Node.transposition.clear ();
	// warm up
	negascout (root, depth, -Short.MAX_VALUE, Short.MAX_VALUE);
	start = System.currentTimeMillis ();
	result = iidnegascout (root, depth);
	System.out.println ("Negascout+IID ("+result+"): "+(System.currentTimeMillis()-start));

	// ITERATIVE ALPHABETA + NEGASCOUT
	Node.useZobrist = false;
	Node.transposition.clear ();
	// warm up
	iidmtdf (root, depth);
	Node.transposition.clear ();
	start = System.currentTimeMillis ();
	result = iidalphascout (root, depth);
	System.out.println ("Alphascout ("+result+"): "+(System.currentTimeMillis()-start));

	// MTDF + ZOBRIST
	Node.useZobrist = true;
	Node.transposition.clear ();
	// warm up
	mtdf (root, 0, depth);
	Node.transposition.clear ();
	start = System.currentTimeMillis ();
	result = mtdf (root, 0, depth);
	System.out.println ("MTD(f)+Zobrist ("+result+"): "+(System.currentTimeMillis()-start));
	*/
    }
}
