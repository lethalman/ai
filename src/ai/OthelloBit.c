#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <limits.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

typedef uint64_t board;

typedef struct {
    board me;
    board adv;
    int maximizing;
    int black;
} Node;

static board masks[8*8][4];
static board adjacent[8*8][8];

static inline void print (Node* n) {
    printf ("---\n");
    int i, j;
    for (i=0; i < 8; i++) {
	for (j=0; j < 8; j++) {
	    printf (" ");
	    board player = ((n->me >> (i*8+j)) & 1) | (((n->adv >> (i*8+j)) & 1) << 1);
	    printf (" %c", (player==0?'.':((((player&1)^n->black)==0?'X':'O'))));
	}
	printf ("\n");
    }
}

static inline void printboard (board b) {
    printf ("bitboard: %llu\n", b);
    int i, j;
    for (i=0; i < 8; i++) {
	for (j=0; j < 8; j++) {
	    printf (" %d", ((b >> (i*8+j))&1));
	}
	printf ("\n");
    }
}

static const board k1 = 0x5555555555555555L;
static const board k2 = 0x3333333333333333L;
static const board k4 = 0x0f0f0f0f0f0f0f0fL;

static void init (void) {
    // attack masks
    int i, j, ii, jj;
    for (i=0; i < 8; i++) {
	for (j=0; j < 8; j++) {
	    board* m = &masks[i*8+j][0];
	    board* a = &adjacent[i*8+j][0];
	    // down (first bit)
	    if (i+2 < 8) {
		for (ii=i+2; ii < 8; ii++) m[0] |= 1L << (ii*8+j);
		a[0] |= 1L << ((i+1)*8+j);
	    }
	    // up (last bit)
	    if (i-2 >= 0) {
		for (ii=i-2; ii >= 0; ii--) m[0] |= 1L << (ii*8+j);
		a[1] |= 1L << ((i-1)*8+j);
	    }

	    // right (first bit)
	    if (j+2 < 8) {
		for (jj=j+2; jj < 8; jj++) m[1] |= 1L << (i*8+jj);
		a[2] |= 1L << (i*8+j+1);
	    }
	    // left (last bit)
	    if (j-2 >= 0) {
		for (jj=j-2; jj >= 0; jj--) m[1] |= 1L << (i*8+jj);
		a[3] |= 1L << (i*8+j-1);
	    }

	    // main diagonal down (first bit)
	    if (i+2 < 8 && j+2 < 8) {
		for (ii=i+2, jj=j+2; ii < 8 && jj < 8; ii++, jj++) m[2] |= 1L << (ii*8+jj);
		a[4] |= 1L << ((i+1)*8+j+1);
	    }
	    // main diagonal up (last bit)
	    if (i-2 >= 0 && j-2 >= 0) {
		for (ii=i-2, jj=j-2; ii >= 0 && jj >= 0; ii--, jj--) m[2] |= 1L << (ii*8+jj);
		a[5] |= 1L << ((i-1)*8+j-1);
	    }

	    // secondary diagonal down (first bit)
	    if (i+2 < 8 && j-2 >= 0) {
		for (ii=i+2, jj=j-2; ii < 8 && jj >= 0; ii++, jj--) m[3] |= 1L << (ii*8+jj);
		a[6] |= 1L << ((i+1)*8+j-1);
	    }
	    // secondary diagonal up (last bit)
	    if (i-2 >= 0 && j+2 < 8) {
		for (ii=i-2, jj=j+2; ii >= 0 && jj < 8; ii--, jj++) m[3] |= 1L << (ii*8+jj);
		a[7] |= 1L << ((i-1)*8+j+1);
	    }
	}
    }
}

static inline int popcount (board x) {
    x -= ((x >> 1) & k1);
    x = (x & k2) + ((x >> 2) & k2);
    x = (x + (x >> 4)) & k4;
    x += x >> 8;
    x += x >> 16;
    x += x >> 32;
    return (int) x & 255;
}
 
static inline void node_init (Node* n, int maximizing, int black, board me, board adv) {
    n->maximizing = maximizing;
    n->black = black;
    n->me = me;
    n->adv = adv;
}

static inline Node* node_children (Node* n, int* n_children) {
    int size = 10;
    Node* r = (Node*) malloc (sizeof (Node) * size);
    *n_children = 0;

    board me = n->me;
    board adv = n->adv;
    board empty = (~me) & (~adv);
    board tmp = me;
    while (tmp != 0) {
	// find our first occupied position
	board bitvalue = tmp & -tmp;
	// unset the position for the next cycle
	tmp &= ~bitvalue;
	// bit index
	int bit = (int)(log2 (bitvalue));

	board* curmasks = &masks[bit][0];
	board* curadjacent = &adjacent[bit][0];
	int z;
	for (z=0; z < 4; z++) {
	    board mask = curmasks[z];
	    board match = mask & ~adv;
	    // first bit
	    board adj = curadjacent[z*2];
	    if ((adv & adj) != 0) {
		// search first bit, adjacent is adversary!
		// get mask for the first bit
		board mask1 = mask & ~(bitvalue - 1);
		// get first non-adversary square
		board m = match & mask1;
		// don't spend any time if it's zero
		if (m != 0) {
		    m &= -m; // gosper's hack
		    if ((empty & m) != 0) {
			// empty position, we can go there!
			if (++(*n_children) > size) {
			    size *= 2;
			    r = realloc (r, sizeof (Node) * size);
			}
			board captured = adj | (((m<<1)-1) & mask1);
			node_init (&r[(*n_children)-1], !n->maximizing, !n->black, adv & ~captured, me | captured);
		    }
		}
	    }
	    adj = curadjacent[z*2+1];
	    if ((adv & adj) != 0) {
		// search last bit, adjacent is adversary!
		// get mask for the last bit
		board mask2 = mask & (bitvalue -1);
		// get first non-adversary square
		board m = match & mask2;
		// don't spend any time if it's zero
		if (m != 0) {
		    m |= (m >> 1);
		    m |= (m >> 2);
		    m |= (m >> 4);
		    m |= (m >> 8);
		    m |= (m >> 16);
		    m |= (m >> 32);
		    m -= m >> 1;
		    if ((empty & m) != 0) {
			// empty position, we can go there!
			if (++(*n_children) > size) {
			    size *= 2;
			    r = realloc (r, sizeof (Node) * size);
			}
			board captured = adj | ((~(m-1)) & mask2);
			node_init (&r[(*n_children)-1], !n->maximizing, !n->black, adv & ~captured, me | captured);
		    }
		}
	    }
	}
    }
    return r;
}

static inline int heuristic (Node* n) {
    return popcount (n->me) - popcount (n->adv);
}

static int negascout (Node* n, int depth, int alpha, int beta) {
    if (depth == 0) {
	return heuristic (n);
    }

    int n_children;
    Node* children = node_children (n, &n_children);
    int i;
    int b = beta;
    int first = 1;
    for (i=0; i < n_children; i++) {
	Node* child = children+i;
	int score = -negascout (child, depth-1, -b, -alpha);
	if (score > alpha && score < beta && !first) {
	    score = -negascout (child, depth-1, -beta, -alpha);
	}
	if (score > alpha) {
	    alpha = score;
	}
	if (beta <= alpha) {
	    break;
	}
	first = 0;
	b = alpha + 1;
    }
    free (children);
    return alpha;
}

void main (void) {
    init ();

    // black move first
    Node root;
    node_init (&root, 1, 1, 0, 0);
    root.adv |= 1L << (3*8+3);
    root.me |= 1L << (3*8+4);
    root.me |= 1L << (4*8+3);
    root.adv |= 1L << (4*8+4);

    const int depth = 12;
    struct timespec start, end;
    int result;

#define elapsed(a, b) (((a.tv_sec * 1000) + a.tv_nsec/1000000) - ((b.tv_sec * 1000) + b.tv_nsec/1000000))

    // NEGASCOUT
    // warm up
    negascout (&root, depth, -SHRT_MAX, SHRT_MAX);
    clock_gettime (CLOCK_MONOTONIC, &start);
    result = negascout (&root, depth, -SHRT_MAX, SHRT_MAX);
    clock_gettime (CLOCK_MONOTONIC, &end);
    printf ("%d %lu\n", result, elapsed (end, start));
}
