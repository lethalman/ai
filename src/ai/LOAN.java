package ai;

import java.util.*;

public class LOAN {
    // 0 = white
    // 1 = black
    // 2 = nobody

    public static short best_move;
    final static int[][] hashnumber = new int[2][8 * 8];
    final static int hashnumberblack;
    static int hashvalue = 0;
    // our player, 0 (white) or 1 (black)
    static int player;
    final static int[] hcount = new int[]{6, 2, 2, 2, 2, 2, 2, 6};
    final static int[] vcount = new int[]{6, 2, 2, 2, 2, 2, 2, 6};
    final static int[] mdcount = new int[]{0, 2, 2, 2, 2, 2, 2,
        0, 2, 2, 2, 2, 2, 2, 0};
    final static int[] sdcount = new int[]{0, 2, 2, 2, 2, 2, 2,
        0, 2, 2, 2, 2, 2, 2, 0};
    final static int[][] board = new int[][]{
        {2, 1, 1, 1, 1, 1, 1, 2},
        {0, 2, 2, 2, 2, 2, 2, 0},
        {0, 2, 2, 2, 2, 2, 2, 0},
        {0, 2, 2, 2, 2, 2, 2, 0},
        {0, 2, 2, 2, 2, 2, 2, 0},
        {0, 2, 2, 2, 2, 2, 2, 0},
        {0, 2, 2, 2, 2, 2, 2, 0},
        {2, 1, 1, 1, 1, 1, 1, 2},};
    final static int[][] stoneFromPos = new int[2][8 * 8];
    final static int[][] posFromStone = new int[2][12];
    final static int[] pedine = new int[]{12, 12};
    // white play first
    static int actual_player = 0;

    static {
        for (int s = 0; s < 6; s++) {
            posFromStone[0][s] = (s + 1) * 8;
            posFromStone[0][s + 6] = (s + 1) * 8 + 7;
            stoneFromPos[0][posFromStone[0][s]] = s;
            stoneFromPos[0][posFromStone[0][s + 6]] = s + 6;

            posFromStone[1][s] = s + 1;
            posFromStone[1][s + 6] = 7 * 8 + s + 1;
            stoneFromPos[1][posFromStone[1][s]] = s;
            stoneFromPos[1][posFromStone[1][s + 6]] = s + 6;
        }

        Random rand = new Random();
        for (int i = 0; i < 8 * 8; i++) {
            hashnumber[0][i] = rand.nextInt();
            hashnumber[1][i] = rand.nextInt();
        }
        hashnumberblack = rand.nextInt();
        for (int i = 0; i < 12; i++) {
            hashvalue ^= hashnumber[0][posFromStone[0][i]];
            hashvalue ^= hashnumber[1][posFromStone[1][i]];
        }
    }

    static class MoveSet {

        Move[] lista_mosse = new Move[12 * 8];
        int size = 0;
        private int[] evaluation = new int[12 * 8];
    }

    static class Move {

        short move;
        int value;

        private Move(short mov, int i) {
            move = mov;
            value = i;
        }
    }
    static boolean primoo = false;
    static int contachiamate = 0;
    static int stone = 0;
    static MoveSet generateMoves() {
        MoveSet ms = new MoveSet();
        int n = pedine[actual_player];
        for (stone = 0; stone < n; stone++) {
            int pos = posFromStone[actual_player][stone];
            int r = pos / 8;
            int c = pos % 8;
            int cnt = hcount[r];
            checkmove(r, c, cnt, 0, 1, ms);


            checkmove(r, c, cnt, 0, -1, ms);
            cnt = vcount[c];
            checkmove(r, c, cnt, 1, 0, ms);
            checkmove(r, c, cnt, -1, 0, ms);
            cnt = mdcount[7 - r + c];
            checkmove(r, c, cnt, 1, 1, ms);
            checkmove(r, c, cnt, -1, -1, ms);
            cnt = sdcount[r + c];
            checkmove(r, c, cnt, 1, -1, ms);
            checkmove(r, c, cnt, -1, 1, ms);
        }
       //empiricamente la cosa migliore è ordinare tutte le mosse
        //almeno per la prima mossa
        for (int k = 0; k < ms.size && k < 50; k++) {
            //trovo i primi m massimi
            boolean trovato = false;
            int pos_max = k;
            int valMax = ms.lista_mosse[k].value;
            for (int j = k; j < ms.size; j++) {
                if (ms.lista_mosse[j].value < valMax) {
                    pos_max = j;
                    valMax = ms.lista_mosse[j].value;
                    //serve per evitare calcoli inutili
                    trovato = true;
                }

            }
            // ho il massimo e la sua posizione
            //System.out.println("max " + valMax);
            if (trovato) {
                Move tmp = ms.lista_mosse[pos_max];//
                ms.lista_mosse[pos_max] = ms.lista_mosse[k];
                ms.lista_mosse[k] = tmp;
            }
            //ms.lista_mosse[j].move = move;
            //ms.lista_mosse[j].value = val;
        }



        
        if (primoo) {      for (int j = 0; j < ms.size/2; j++) {
        System.out.println("" + ms.lista_mosse[j].value);           }        primoo = false;
        }
        
        
         
        contachiamate++;
        return ms;
    }
    static int ordinamenti_fatti = 0;

    static void checkmove(int r, int c, int n, int dr, int dc, MoveSet ms) {
        short move = (short) (r * 8 + c);
        r += dr * n;
        c += dc * n;
        if (r < 0 || r >= 8 || c < 0 || c >= 8) {
            return;
        }
        if (board[r][c] == actual_player) {
            return;
        }
        move = (short) ((move << 7) | ((r * 8 + c) << 1));
        if (board[r][c] != 2) {
            move |= 1;
        }
        int i = 0;
        do {
            r -= dr;
            c -= dc;
            i++;
        } while (i < n && board[r][c] != 1 - actual_player);
        if (i < n) {
            return;
        }
        // cerco di ordinare le mosse secondo il valore di euristica
        //apply(move);
        //int val = evaluate();
       // undo(move);
        //calcolo incrementale euristica
        //r * 8 + c = posiz corrente 
        /*
         * for (int j = 0; j < posFromStone.length; j++) {
            System.out.println("pso"+posFromStone[actual_player][j]);
            
        }
         */
        //se non muovo come la calcolo l'euristica, che ho somma 0?!?
        //sono necessarie le bitboard ?!?
        //apply(move);
        
        short move1 = move;
        //int from = (move1 >> 7) & 0x3f;
        int to = (move1 >> 1) & 0x3f;
        // to oppure stoneFromPos[actual_player][posFromStone[actual_player][stone]] ???
        //int eval = -nodo_corretente.evaluetion + centralisation[posFromStone[actual_player][stone]] - centralisation[posFromStone[1-actual_player][stone]];
        int eval = -nodo_corretente.evaluetion + centralisation[posFromStone[actual_player][stone]] - centralisation[stoneFromPos[actual_player][posFromStone[actual_player][stone]]];
        //System.out.println("1 "+nodo_corretente.evaluetion+" 2 "+centralisation[posFromStone[actual_player][stone]]+" 3 "+centralisation[r * 8 + c]);
        //System.out.println("eval " + eval+" centr "+(r * 8 + c) + " from "+posFromStone[actual_player][stone]);
        //undo(move);
       //FARE APPLY E UNDO MI COSTA TANTO MA COME VALUTARE ALTRIMENTI?
        ms.lista_mosse[ms.size++] = new Move(move, eval);


        /*
        if (ordinamenti_fatti == 0 || ordinamenti_fatti > 14) {
        ms.lista_mosse[ms.size++] = new Move(move, val);
        } else {
        ++ordinamenti_fatti;
        int k = 0;
        //MoveSet ms_ordinato = new MoveSet();
        for (int j = 0; j < ms.size; j++) {
        if (ms.lista_mosse[j].value < val) {
        k = j;
        j = ms.size;
        }
        
        }
        Move tmp = new Move(move, val);
        Move tmp2;
        for (int j = k; j < ms.size; j++) {
        tmp2 = ms.lista_mosse[j];
        ms.lista_mosse[j] = tmp;
        tmp = tmp2;
        //ms.lista_mosse[j].move = move;
        //ms.lista_mosse[j].value = val;
        }
        ms.lista_mosse[ms.size++] = tmp;
        }
         * 
         */
    }

    static void apply(short move) {
        int from = (move >> 7) & 0x3f;
        int to = (move >> 1) & 0x3f;
        int fromi = from / 8;
        int fromj = from % 8;
        int toi = to / 8;
        int toj = to % 8;
        boolean capture = (move & 0x1) != 0;

        hashvalue ^= hashnumberblack;

        board[fromi][fromj] = 2; // nobody
        hcount[fromi]--;
        vcount[fromj]--;
        mdcount[7 - fromi + fromj]--;
        sdcount[fromi + fromj]--;

        int stone = stoneFromPos[actual_player][from];
        hashvalue ^= hashnumber[actual_player][from];
        stoneFromPos[actual_player][to] = stone;
        posFromStone[actual_player][stone] = to;
        hashvalue ^= hashnumber[actual_player][to];

        if (capture) {
            int adv = 1 - actual_player;
            hashvalue ^= hashnumber[adv][to];
            int capturedStone = stoneFromPos[adv][to];
            int last = --pedine[adv];
            int lastpos = posFromStone[adv][last];
            posFromStone[adv][capturedStone] = lastpos;
            stoneFromPos[adv][lastpos] = capturedStone;
        } else {
            hcount[toi]++;
            vcount[toj]++;
            mdcount[7 - toi + toj]++;
            sdcount[toi + toj]++;
        }
        board[toi][toj] = actual_player;

        actual_player = 1 - actual_player;
    }

    static void undo(short move) {
        int from = (move >> 7) & 0x3f;
        int to = (move >> 1) & 0x3f;
        int fromi = from / 8;
        int fromj = from % 8;
        int toi = to / 8;
        int toj = to % 8;
        boolean capture = (move & 0x1) != 0;

        hashvalue ^= hashnumberblack;
        actual_player = 1 - actual_player;

        board[fromi][fromj] = actual_player; // nobody
        hcount[fromi]++;
        vcount[fromj]++;
        mdcount[7 - fromi + fromj]++;
        sdcount[fromi + fromj]++;

        int stone = stoneFromPos[actual_player][to];
        hashvalue ^= hashnumber[actual_player][to];
        stoneFromPos[actual_player][from] = stone;
        posFromStone[actual_player][stone] = from;
        hashvalue ^= hashnumber[actual_player][from];

        if (capture) {
            int adv = 1 - actual_player;
            hashvalue ^= hashnumber[adv][to];
            int last = pedine[adv]++;
            posFromStone[adv][last] = to;
            stoneFromPos[adv][to] = last;
            board[toi][toj] = adv;
        } else {
            board[toi][toj] = 2; // nobody
            hcount[toi]--;
            vcount[toj]--;
            mdcount[7 - toi + toj]--;
            sdcount[toi + toj]--;
        }
    }

    static void print() {
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                System.out.print(" " + (board[i][j] == 2 ? '.' : (board[i][j] == 0 ? 'O' : 'X')));
            }
            System.out.println();
        }
    }

    static class Node {

        int hash;
        short move;
        int lowerbound;
        int upperbound;
        int depth;
        int pv;
         int evaluetion;

        public boolean equals(Object o) {
            return move == ((Node) o).move;
        }

        public int hashCode() {
            return hash;
        }
    }
    static final int[] centralisation = {
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 1, 1, 1, 1, 1, 1, 0,
        0, 1, 5, 5, 5, 5, 1, 0,
        0, 1, 5, 10, 10, 5, 1, 0,
        0, 1, 5, 10, 10, 5, 1, 0,
        0, 1, 5, 5, 5, 5, 1, 0,
        0, 1, 1, 1, 1, 1, 1, 0,
        0, 0, 0, 0, 0, 0, 0, 0};
    static final int weigthPDF[] = {
        -80, -25, -20, -20, -20, -20, -25, -80,
        -25, 10, 10, 10, 10, 10, 10, -25,
        -20, 10, 25, 25, 25, 25, 10, -20,
        -20, 10, 25, 50, 50, 25, 10, -20,
        -20, 10, 25, 50, 50, 25, 10, -20,
        -20, 10, 25, 25, 25, 25, 10, -20,
        -25, 10, 10, 10, 10, 10, 10, -25,
        -80, -25, -20, -20, -20, -20, -25, -80};

    static int evaluate() {

        /*
        int w[];
        if (weightpdf) {
        w = weigthPDF;
        } else {
        w = weight;
        }
         */
        int eval = 0;
        for (int i = 0; i < pedine[actual_player]; i++) {
            eval += centralisation[posFromStone[actual_player][i]];
        }
        int adv = 1 - actual_player;
        for (int i = 0; i < pedine[adv]; i++) {
            eval -= centralisation[posFromStone[adv][i]];
        }
        return eval;
    }
    static final int MAX_ENTRIES = 1000000;
    static LinkedHashMap<Node, Node> ttable = new LinkedHashMap<Node, Node>(2097143) {

        @Override
        protected boolean removeEldestEntry(Map.Entry eldest) {
            return size() > MAX_ENTRIES;
        }
    };
    static int conta_chiamateabPV = 0;

    static int alphabetaPV(Node n, int depth, int alpha, int beta) {
        if (depth == 0) {
            return evaluate();
        }
        conta_chiamateabPV++;
        // principal variation
        int pv = 0;
        nodo_corretente=n;
        // transposition table lookup
        Node e = ttable.get(n);
        if (e != null) {
            nodo_corretente=e;
            if (e.depth >= depth) {
                // good hit
                if (e.lowerbound >= beta || e.lowerbound == e.upperbound) {
                    return e.lowerbound;
                }
                if (e.upperbound <= alpha) {
                    return e.upperbound;
                }
                alpha = Math.max(alpha, e.lowerbound);
                beta = Math.min(beta, e.upperbound);
            }
            pv = e.pv;
        }
        
        //nodo_corretente=n;
        MoveSet moves = generateMoves();
        
        int[] evaluation = moves.evaluation;
        
        
        boolean firstChild = true;
        int g = -Short.MAX_VALUE;
        int a = alpha;
        int oldpv = pv;
        for (int i = pv; i < moves.size; i++) {
            if (i == oldpv && !firstChild) {
                // skip PV
                continue;
            }
            //prendo la move dall'aggetto Move che la contiene
            short move = moves.lista_mosse[i].move;
            apply(move);
            Node child = new Node();
            child.hash = hashvalue;
            child.move = move;
            
            child.evaluetion = moves.lista_mosse[i].value;//evaluation[i];
           
            //System.out.println("icrem "+child.evaluetion);
            int score = -alphabetaPV(child, depth - 1, -beta, -a);

            undo(move);
            if (score > g) {
                g = score;
                pv = i;
                //edit verificare o eliminare
                best_move = move;
                a = Math.max(a, g);
            }
            if (beta <= a) {
                break;
            }
            if (firstChild) {
                i = -1;
                firstChild = false;
            }
        }

        // transposition table storing
        if (e == null) {
            e = n;
            // first time seen
            ttable.put(e, e);
            e.depth = depth;
        } else {
            // bypass the hashmap
            if (depth < e.depth) {
                // worsen information, don't store
                return g;
            } else {
                e.depth = depth;
            }
        }
        if (g <= alpha) {
            e.upperbound = g;
            e.lowerbound = -Short.MAX_VALUE;
        } else if (g > alpha && g < beta) {
            e.lowerbound = e.upperbound = g;
        } else {
            e.lowerbound = g;
            e.upperbound = Short.MAX_VALUE;
        }
        e.pv = pv;


        return g;
    }

    static int iidalphabeta(Node root, int maxdepth) {
        int g = -Short.MAX_VALUE;
        int d = 2;
        Node e = ttable.get(root);
        if (e != null) {
            if (e.lowerbound == e.upperbound) {
                d = e.depth + 1;
            } else {
                d = e.depth;
            }
        }
        while (d <= maxdepth) {
            int tmp = alphabetaPV(root, d, -Short.MAX_VALUE, Short.MAX_VALUE);
            g = tmp;
            d++;
        }

        return g;
    }
    private static Node nodo_corretente;
    public static void main(String[] args) {
        player = 0; // white
        print();
        Node root = new Node();
        root.hash = hashvalue;
        root.evaluetion = evaluate();
        
        //System.out.println("+root"+root.evaluetion);
        long start = System.currentTimeMillis();
        int result = iidalphabeta(root, 5);
        System.out.println("Result " + result + "; tot=" + (System.currentTimeMillis() - start) + "ms");

        //applico la mossa trovata
        Node c = ttable.get(root);
        MoveSet moves = generateMoves();
        short bestmove = moves.lista_mosse[c.pv].move;
        // perché aplly best_move è diverso????
        apply(bestmove);

        print();
        System.out.println("Chiamate a GenMov " + contachiamate);
        System.out.println("Chiamate a abPV " + conta_chiamateabPV);
        conta_chiamateabPV = 0;
        contachiamate = 0;
        /* Node tmp = root.nextmove ();
        if (tmp == null) {
        System.out.println ("WE LOST!");
        break;
        } else {
        root = tmp;
        }
         * 
         */


    }
}
