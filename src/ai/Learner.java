package ai;

import java.util.*;
import java.io.*;

public class Learner {
    static ArrayList<ALOA> blackBreakers = new ArrayList<ALOA> ();
    static int blackIndex = 0;
    static ALOA w, b;
    static ALOA.Evaluation lasteval;
    static double[] deltaupdate;
    static double[] errorgradient;
    static double[] weightdelta;
    static int ndelta = 0;
    static ALOA bestWinner;

    static final int winvalue = 10;

    // learn from GM
    static void gmLearning () {
	HashSet<ALOA.Node> set = new HashSet<ALOA.Node> ();
	try {
	    readGMData ("/home/lethal/gamemaster/gm_1sec_1best.txt", set);
	    readGMData ("/home/lethal/gamemaster/gm_2sec_1best.txt", set);
	    readGMData ("/home/lethal/gamemaster/gm_1sec_2best.txt", set);
	    readGMData ("/home/lethal/gamemaster/gm_2sec_2best.txt", set);
	    readGMData ("/home/lethal/gamemaster/gm_1sec_1best_nullmove.txt", set);
	    readGMData ("/home/lethal/gamemaster/gm_2sec_1best_nullmove.txt", set);
	    readGMData ("/home/lethal/gamemaster/gm_2sec_1best_standard.txt", set);
	} catch (Exception e) {
	    e.printStackTrace ();
	    return;
	}
	// initial position
	ALOA a = new ALOA (1, 1, null);
	a.root.standpat = 201;
	set.add (a.root);

	// RPROP - BATCH LEARNING
	double[] weights = new double[]{0.0, 0.0, 0.4, 0.0, 3.2, 0.4, 0.0, 0.8, 1.6, 0.2, 0.1};
	System.out.println ("Started. Number of points: "+set.size ());
	final double learningFactor = 0.00000001;
	final double deltamin = 1E-15;
	final double deltamax = 0.01;
	final double deltainc = 1.2;
	final double deltadec = 0.5;
	final double[] deltaupdate = new double[weights.length];
	for (int i=0; i < deltaupdate.length; i++) {
	    deltaupdate[i] = deltamin;
	}
	double[] prevdelta = new double[weights.length];
	int epoch = 0;
	while (true) { // should be while (not converged)
	    epoch++;
	    double[] delta = new double[weights.length];
	    for (ALOA.Node point: set) {
		ALOA.Evaluation eval = point.learningEvaluation (weights);
		if (point.black == 0) {
		    // invert
		    eval.value = -eval.value;
		    for (int i=0; i < eval.gradient.length; i++) {
			eval.gradient[i] = -eval.gradient[i];
		    }
		}

		for (int i=0; i < eval.gradient.length; i++) {
		    delta[i] += (point.standpat - eval.value) * eval.gradient[i];
		}
	    }
	    // update weights
	    for (int i=0; i < weights.length; i++) {
		double mul = delta[i]*prevdelta[i];
		if (mul > 0) {
		    deltaupdate[i] = Math.min (deltaupdate[i]*deltainc, deltamax);
		    weights[i] += sign (delta[i])*deltaupdate[i];
		} else if (mul < 0) {
		    deltaupdate[i] = Math.max (deltaupdate[i]*deltadec, deltamin);
		    weights[i] += sign (delta[i])*deltaupdate[i];
		}
	    }
	    prevdelta = delta;
	    System.out.println ("Points: "+set.size()+". Epoch: "+epoch+". Root evaluation: "+a.root.learningEvaluation(weights).value+". New weights: "+Arrays.toString (weights));
	}
    }

    // read GM data
    static void readGMData (String filename, HashSet<ALOA.Node> set) throws Exception {
	BufferedReader r = new BufferedReader (new FileReader (new File (filename)));
	ALOA a = new ALOA (1, 1, new double[]{0, 0,
					      1, 1, 1, 1, 0, 1, 1, 1, 1});
	ALOA.Node root = a.root;

	boolean firstMove = true;
	System.out.println ("Reading file "+filename);
	while (true) {
	    String line = r.readLine ();
	    if (line == null) {
		break;
	    }
	    if (line.startsWith ("Iteration")) {
		String[] tokens = line.split (" ");
		int nmoves = Integer.parseInt (tokens[3].trim().replace(".", ""));
		if (nmoves == 1) {
		    if (firstMove) {
			// new match
			root = a.root;
			firstMove = false;
		    }
		} else {
		    firstMove = true;
		}
		if (nmoves <= 5) {
		    continue;
		}
		int value = Integer.parseInt (tokens[2].trim ());
		if (Math.abs (value) >= 9000) {
		    // noise, win/lose value
		    continue;
		}
		ALOA.Node tmp = root;
		// apply the principal variation
		for (int i=4; i < tokens.length; i++) {
		    String smove = tokens[i].trim().toUpperCase ();
		    smove = smove.replace ("X", "-");
		    if (!smove.contains ("-")) {
			continue;
		    }
		    int move = ALOA.stringToMove (smove);
		    tmp = tmp.applymove (move);
		}
		tmp.standpat = value;
		set.add (tmp);
	    } else if (line.startsWith ("Percentage")) {
		// apply move
		line = r.readLine ();
		if (line == null) {
		    break;
		}
		String smove = line.trim().toUpperCase ();
		smove = smove.replace ("X", "-");
		int move = ALOA.stringToMove (smove);
		root = root.applymove (move);
	    }
	}
	r.close ();
    }

    // randomize one component
    static ALOA randomLoa (int black, ALOA loa) {
	double[] weights = loa.getWeights ();
	weights = Arrays.copyOf (weights, weights.length);
	// between 1 and weights-1
	int c = (int)(Math.random()*(weights.length-2)+1.1);
	weights[c] += Math.random()*2-2; // between -2 and 2
	return new ALOA (0, black, weights);
    }

    static double sign (double x) {
	if (x > 0) {
	    return 1;
	} else {
	    return -1;
	}
    }

    // random tuning
    static void randomTuning () {
	try {
	    ObjectInputStream is = new ObjectInputStream (new FileInputStream ("blackbreakers"));
	    blackBreakers = (ArrayList<ALOA>) is.readObject ();
	} catch (Exception e) { }

	blackIndex = 0;
	bestWinner = w = new ALOA (0, 0, new double[]{0.0, 14.034646087861276, -0.49197571851707766, -1.9985995652579391, 1.0, -0.6096960250305485, -1.6379584491480839, -0.588029719966987, 1.0, 0.15848074564523662, -1.7650240963669275, 0.6269887244080856, 1.0, -0.7669297521940333, 1.0, -0.9871921626018145, 1.0, 1.0, 1.0, 0.1});
	b = randomLoa (1, w);
	while (true) {
	    printBest ();
	    System.out.println ("Black index: "+blackIndex);
	    if (challenge (w, b)) {
		if (blackIndex >= blackBreakers.size ()) {
		    b = randomLoa (1, w);
		} else {
		    b = blackBreakers.get (blackIndex);
		    b.reset (1, b.getWeights ());
		}
		if (blackIndex > blackBreakers.size ()) {
		    bestWinner = w;
		}
		blackIndex++;
	    } else {
		if (w == bestWinner) {
		    b.reset (0, b.getWeights ());
		    blackBreakers.add (b);
		    try {
			ObjectOutputStream os = new ObjectOutputStream (new FileOutputStream ("blackbreakers"));
			os.writeObject (blackBreakers);
			os.close ();
		    } catch (Exception e) {
			    e.printStackTrace ();
		    }
		}
		// try to beat this black
		do {
		    w = randomLoa (0, b);
		    printBest ();
		} while (!challenge (w, b));
		// restart beating all the blacks
		blackIndex = 0;
	    }
	}
    }

    public static void printBest () {
	System.out.println ("Best winner: "+Arrays.toString (bestWinner.getWeights ()));
    }

    // random trial and error
    public static int trialErrorChallenge (int comp) {
        double[] weights = w.getWeights ();
        if (comp >= weights.length-1) {
            boolean res = challenge (w, b);
            if (res) return 1;
            else return -1;
        }
        int res = trialErrorChallenge (comp+1);
        if (res > 0) return res;
        for (double delta=0.1; delta <= 0.1; delta += 1) {
            weights[comp] += delta;
            if (trialErrorChallenge (comp+1) > 0) return 2;
            weights[comp] -= delta*2;
            if (trialErrorChallenge (comp+1) > 0) return 2;
            weights[comp] += delta;
        }
        return -1;
    }

    public static void trialError () {
        while (true) {
            if (blackIndex >= blackBreakers.size()) {
                b = randomLoa (1, w);
            } else {
                b = blackBreakers.get (blackIndex++);
            }
            w.reset (0, w.getWeights ());
            b.reset (0, b.getWeights ());
            int result = trialErrorChallenge (1);
            if (result < 0) {
                // lost, reset to black weights
                System.out.println ("== TRIAL&ERROR: TOTAL LOST");
                w.reset (0, b.getWeights ());
                blackBreakers.add (b);
                blackIndex = 0;
            } else if (result == 2) {
                // win by changing weights
                System.out.println ("== TRIAL&ERROR: DELTA WEIGHTS");
                blackBreakers.add (b);
                blackIndex = 0;
            }
        }
    }

    public static void main (String[] args) {
        if (false) { // trial and error
	    w = new ALOA (0, 0, new double[]{0, 0.6029965607451921, -0.2260353595301059, 2.896233860086544, 1.926220294334629, 3.3214670803906334, 1.5320559297612362, 1.532645411202179, 1.3960896000560923, 0.40838882996447323});
            trialError ();
	}
	if (true) { // 2v2 challenge
	    ALOA ww = new ALOA (0, 0, new double[]{3, 300,
						   0.19320688423639118, -0.22532208357000463, 1.978249618209707, 0.15843100589666084, 0.3262643765178706, 1.037848193346331, 1.5461326883590238, 4.746392718104192, 0.1238797320125999});
	    ALOA bb = new ALOA (0, 0, new double[]{3, 300,
						   0.19320688423639118, -0.22532208357000463, 1.978249618209707, 0.15843100589666084, 0.3262643765178706, 1.037848193346331, 1.5461326883590238, 4.746392718104192, 0.1238797320125999});
	    ww.search (); // rodaggio per il GC
	    challenge (ww, bb);
	    challenge (bb, ww);
	    System.exit (0);
	}
	if (false) { // random tuning
	    randomTuning ();
	}
	if (true) { // learn from GM
	    gmLearning ();
	    return;
	}
    }

    static boolean challenge (ALOA w, ALOA b) {
	w.reset (0, w.getWeights ());
	b.reset (0, b.getWeights ());

	int nmoves = 0;
	long welapsed = 0;
	long belapsed = 0;
	System.out.println ("-> CHALLENGE");
	System.out.println ("White: "+Arrays.toString (w.getWeights ()));
	System.out.println ("Black: "+Arrays.toString (b.getWeights ()));

	while (true) {
	    if (nmoves > 50) {
		System.out.println ("STOP!");
		w.print ();
		return false;
	    }
	    if (w.blackConnected ()) {
		w.print ();
		System.out.println ("Black won. Moves: "+nmoves+". Search time: white="+welapsed+", black="+belapsed);
		return false;
	    } else if (w.whiteConnected ()) {
		w.print ();
		System.out.println ("White won. Moves: "+nmoves+". Search time: white="+welapsed+", black="+belapsed);
		return true;
	    }
	    long start = System.currentTimeMillis ();
	    int move = w.search ();
            if (move == 0) return false;
	    welapsed = Math.max (welapsed, System.currentTimeMillis()-start);
	    nmoves++;
	    if (w.blackConnected ()) {
		w.print ();
		System.out.println ("Black won. Moves: "+nmoves+". Search time: white="+welapsed+", black="+belapsed);
		return false;
	    } else if (w.whiteConnected ()) {
		w.print ();
		System.out.println ("White won. Moves: "+nmoves+". Search time: white="+welapsed+", black="+belapsed);
		return true;
	    }
	    b.apply (move);
	    start = System.currentTimeMillis ();
	    move = b.search ();
            if (move == 0) return true;
	    belapsed = Math.max (belapsed, System.currentTimeMillis()-start);
	    w.apply (move);
	}
    }
}
