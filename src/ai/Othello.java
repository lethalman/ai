package ai;

import java.util.*;

class State {
    // white = 1, black = 2
    static int[][] matrix = new int[8][8];
    static int player;

    static void print () {
	System.out.println ("---");
	for (int i=0; i < 8; i++) {
	    for (int j=0; j < 8; j++) {
		int player = matrix[i][j];
		System.out.print (" "+(player==0?'.':(player==1?'O':'X')));
	    }
	    System.out.println ();
	}
    }
}

class Node {
    boolean maximizing;
    int player;
    int adversary;

    int ifrom, jfrom;
    int ito, jto;
    
    Node (boolean maximizing) {
	this.maximizing = maximizing;
	this.player = maximizing ? State.player : 3-State.player;
	this.adversary = 3-player;
    }

    Node (boolean maximizing, int ifrom, int jfrom, int ito, int jto) {
	this.maximizing = maximizing;
	this.player = maximizing ? State.player : 3-State.player;
	this.adversary = 3-player;
	this.ifrom = ifrom;
	this.ito = ito;
	this.jfrom = jfrom;
	this.jto = jto;
    }

    void apply () {
	if (ifrom == ito) {
	    for (int j=Math.min(jfrom,jto); j <= Math.max(jfrom,jto); j++) {
		State.matrix[ifrom][j] = adversary;
	    }
	} else if (jfrom == jto) {
	    for (int i=Math.min(ifrom,ito); i <= Math.max(ifrom,ito); i++) {
		State.matrix[i][jfrom] = adversary;
	    }
	} else {
	    // TODO: diagonals
	}
    }

    void revert () {
	if (ifrom == ito) {
	    for (int j=Math.min(jfrom,jto); j <= Math.max(jfrom,jto); j++) {
		State.matrix[ifrom][j] = player;
	    }
	} else if (jfrom == jto) {
	    for (int i=Math.min(ifrom,ito); i <= Math.max(ifrom,ito); i++) {
		State.matrix[i][jfrom] = player;
	    }
	} else {
	    // TODO: diagonals
	}
	State.matrix[ifrom][jfrom] = adversary;
	State.matrix[ito][jto] = 0;
    }

    List<Node> children () {
	List<Node> r = new ArrayList<Node> ();
	for (int i=0; i < 8; i++) {
	    for (int j=0; j < 8; j++) {
		if (State.matrix[i][j] == player) {
		    // up
		    int ii, jj;
		    for (ii=i-1; ii >= 0 && State.matrix[ii][j] == adversary; ii--);
		    if (ii >= 0 && State.matrix[ii][j] == 0 && State.matrix[ii+1][j] == adversary) r.add (new Node (!maximizing, i, j, ii, j));
		    // down
		    for (ii=i+1; ii < 8 && State.matrix[ii][j] == adversary; ii++);
		    if (ii < 8 && State.matrix[ii][j] == 0 && State.matrix[ii-1][j] == adversary) r.add (new Node (!maximizing, i, j, ii, j));
		    // left
		    for (jj=j-1; jj >= 0 && State.matrix[i][jj] == adversary; jj--);
		    if (jj >= 0 && State.matrix[i][jj] == 0 && State.matrix[i][jj+1] == adversary) r.add (new Node (!maximizing, i, j, i, jj));
		    // right
		    for (jj=j+1; jj < 8 && State.matrix[i][jj] == adversary; jj++);
		    if (jj < 8 && State.matrix[i][jj] == 0 && State.matrix[i][jj-1] == adversary) r.add (new Node (!maximizing, i, j, i, jj));
		    // TODO: diagonals
		}
	    }
	}
	return r;
    }

    short heuristic () {
	short w=0, b=0;
	for (int i=0; i < 8; i++) {
	    for (int j=0; j < 8; j++) {
		if (State.matrix[i][j] == 1) {
		    w += 1;
		} else if (State.matrix[i][j] == 2) {
		    b += 1;
		}
	    }
	}

	if (player == 1) {
	    return (short)(w-b);
	} else {
	    return (short)(b-w);
	}
    }
}

public class Othello {
    static int minimax (Node n, int depth) {
	if (depth == 0) {
	    return n.heuristic ();
	}
	List<Node> children = n.children ();
	if (children.size () == 0) {
	    if (n.maximizing) {
		return -Short.MAX_VALUE;
	    } else {
		return Short.MAX_VALUE;
	    }
	}

	int score = -Short.MAX_VALUE;
	for (Node child: children) {
	    child.apply ();
	    score = Math.max (score, -minimax (child, depth-1));
	}
	return score;
    }

    static int negascout (Node n, int depth, int alpha, int beta) {
	if (depth == 0) {
	    return n.heuristic ();
	}
	List<Node> children = n.children ();
	if (children.size () == 0) {
	    if (n.maximizing) {
		return -Short.MAX_VALUE;
	    } else {
		return Short.MAX_VALUE;
	    }
	}

	int b = beta;
	boolean firstChild = true;
	for (Node child: n.children ()) {
	    child.apply ();
	    int score = -negascout (child, depth-1, -b, -alpha);
	    if (score > alpha && score < beta && !firstChild) {
		score = -negascout (child, depth-1, -beta, -alpha);
	    }
	    child.revert ();
	    alpha = Math.max (alpha, score);
	    if (beta <= alpha) {
		break;
	    }
	    firstChild = false;
	    b = alpha + 1;
	}
	return alpha;
    }

    public static void main (String[] args) {
	// black move first
	State.player = 2;
	// initial position
	State.matrix[3][3] = 1;
	State.matrix[3][4] = 2;
	State.matrix[4][3] = 2;
	State.matrix[4][4] = 1;

	long start = System.currentTimeMillis ();
	int result = negascout (new Node (true), 17, -Short.MAX_VALUE, Short.MAX_VALUE);
	System.out.println (result+" "+(System.currentTimeMillis()-start));
    }
}
