package ai;

import java.util.*;

// IMMUTABLE
interface Action {
    // apply changes to State
    public void apply ();
    // revert changes from State
    public void revert ();
}

abstract class Node {
    public Action action;
    public boolean maximizing;

    public abstract int heuristic ();
    public abstract List<Node> children ();
    public abstract Node createChild ();
    public abstract boolean rightSibling ();
}

class TrisState {
    // very important: 1 or 2
    public static int player;
    // perfect hash = no collisions
    public static int perfecthash = 0;
    // transposition table
    public static HashMap<Integer, Integer> cache = new HashMap<Integer, Integer> ();

    public static int get (int i, int j) {
	return (perfecthash >> i*3*2+j*2) & 3;
    }

    public static void print () {
	System.out.println ("---");
	for (int i=0; i < 3; i++) {
	    for (int j=0; j < 3; j++) {
		System.out.print (" "+(get (i, j)==0 ? "." : get (i, j)));
	    }
	    System.out.println ();
	}
    }
}

class Entry {
    int lowerbound;
    int upperbound;
}

class TrisAction implements Action {
    public int i, j;
    public int value;

    public TrisAction (int i, int j, int value) {
	this.i = i;
	this.j = j;
	this.value = value;
    }

    public void apply () {
	TrisState.perfecthash ^= value << i*3*2+j*2;
    }

    public void revert () {
	TrisState.perfecthash ^= value << i*3*2+j*2;
    }
}

class TrisNode extends Node {
    private int i, j;
    private int player, adversary;

    public TrisNode (boolean maximizing) {
	this.maximizing = maximizing;
	this.player = maximizing ? TrisState.player : 3-TrisState.player;
	this.adversary = 3-player;
    }

    // Luca: è la più stupida che mi è venuta in mente :)
    // X = number of possible tris for player X
    // O = number of possible tris for player O
    public int heuristic () {
	int X = 0, O = 0;
	int diag1 = 0;
	int diag2 = 0;
	for (int i=0; i < 3; i++) {
	    // diagonals
	    diag1 |= TrisState.get (i, i);
	    diag2 |= TrisState.get (i, 2-i);

	    // column or row
	    int col = 0;
	    int row = 0;
	    for (int j=0; j < 3; j++) {
		int x;
		col |= TrisState.get (i, j);
		row |= TrisState.get (j, i);
	    }
	    if (col != 3) {
		X += col & 1;
		O += col >> 1;
	    }
	    if (row != 3) {
		X += row & 1;
		O += row >> 1;
	    }
	}
	if (diag1 != 3) {
	    X += diag1 & 1;
	    O += diag1 >> 1;
	}
	if (diag2 != 3) {
	    X += diag2 & 1;
	    O += diag2 >> 1;
	}
	if (maximizing && TrisState.player == 1) {
	    return X-O;
	} else {
	    return O-X;
	}
    }

    public List<Node> children () {
	ArrayList<Node> list = new ArrayList<Node> ();
	for (int i=0; i < 3; i++) {
	    for (int j=0; j < 3; j++) {
		if (TrisState.get (i, j) == 0) {
		    Node child = new TrisNode (!maximizing);
		    child.action = new TrisAction (i, j, player);
		    list.add (child);
		}
	    }
	}
	return list;
    }

    public Node createChild () {
	return new TrisNode (!maximizing);
    }

    // returns false if we didn't move
    public boolean rightSibling () {
	if (i == 3) {
	    return false;
	}

	for (; i < 3; i++) {
	    for (; j < 3; j++) {
		if (TrisState.get (i, j) == 0) {
		    action = new TrisAction (i, j, adversary);
		    j++;
		    return true;
		}
	    }
	    j=0;
	}
	return false;
    }
}

public class Main {
    static int alphabeta1 (Node n, int depth, int alpha, int beta) {
	if (depth == 0) {
	    return n.heuristic ();
	}
	for (Node child: n.children ()) {
	    child.action.apply ();
	    int score = -alphabeta1 (child, depth-1, -beta, -alpha);
	    alpha = Math.max (alpha, score);
	    child.action.revert ();
	    if (beta <= alpha) {
		break;
	    }
	}
	return alpha;
    }

    static int alphabeta2 (Node n, int depth, int alpha, int beta) {
	if (depth == 0) {
	    return n.heuristic ();
	}
	Node child = n.createChild ();
	while (child.rightSibling ()) {
	    child.action.apply ();
	    int score = -alphabeta2 (child, depth-1, -beta, -alpha);
	    alpha = Math.max (alpha, score);
	    child.action.revert ();
	    if (beta <= alpha) {
		break;
	    }
	}
	return alpha;
    }

    static int negascout1 (Node n, int depth, int alpha, int beta) {
	if (depth == 0) {
	    return n.heuristic ();
	}
	int b = beta;
	boolean firstChild = true;
	for (Node child: n.children ()) {
	    child.action.apply ();
	    int score = -negascout1 (child, depth-1, -b, -alpha);
	    if (score > alpha && score < beta && !firstChild) {
		score = -negascout1 (child, depth-1, -beta, -alpha);
	    }
	    child.action.revert ();
	    alpha = Math.max (alpha, score);
	    if (beta <= alpha) {
		break;
	    }
	    firstChild = false;
	    b = alpha + 1;
	}
	return alpha;
    }

    static int negascout2 (Node n, int depth, int alpha, int beta) {
	if (depth == 0) {
	    return n.heuristic ();
	}
	int b = beta;
	boolean firstChild = true;
	Node child = n.createChild ();
	while (child.rightSibling ()) {
	    child.action.apply ();
	    int score = -negascout2 (child, depth-1, -b, -alpha);
	    if (score > alpha && score < beta && !firstChild) {
		score = -negascout2 (child, depth-1, -beta, -alpha);
	    }
	    child.action.revert ();
	    alpha = Math.max (alpha, score);
	    if (beta <= alpha) {
		break;
	    }
	    firstChild = false;
	    b = alpha + 1;
	}
	return alpha;
    }

    static int alphabetaMTD1 (Node n, int depth, int alpha, int beta) {
	// transposition table lookup
	Integer e = TrisState.cache.get (TrisState.perfecthash);
	if (e != null) {
	    int lowerbound = (short)(e & 0xffff);
	    if (lowerbound >= beta) {
		return lowerbound;
	    }
	    int upperbound = (short)(e >> 16);
	    if (upperbound <= alpha) {
		return upperbound;
	    }
	    e = null;
	    alpha = Math.max (alpha, lowerbound);
	    beta = Math.min (beta, upperbound);
	}

	int g;
	if (depth == 0) {
	    g = n.maximizing ? n.heuristic () : -n.heuristic ();
	} else if (n.maximizing) {
	    g = -Integer.MAX_VALUE;
	    int a = alpha;
	    for (Node child: n.children ()) {
		child.action.apply ();
		g = Math.max (g, alphabetaMTD1 (child, depth-1, a, beta));
		child.action.revert ();
		a = Math.max (a, g);
		if (g >= beta) {
		    break;
		}
	    }
	} else {
	    g = Integer.MAX_VALUE;
	    int b = beta;
	    for (Node child: n.children ()) {
		child.action.apply ();
		g = Math.min (g, alphabetaMTD1 (child, depth-1, alpha, b));
		child.action.revert ();
		b = Math.min (b, g);
		if (g <= alpha) {
		    break;
		}
	    }
	}

	// transposition table storing
	if (g <= alpha) {
	    e = ((g & 0xffff) << 16) | ((-Short.MAX_VALUE) & 0xffff);
	} else if (g > alpha && g < beta) {
	    e = ((g & 0xffff) << 16) | (g & 0xffff);
	} else {
	    e = (((Short.MAX_VALUE) & 0xffff) << 16) | (g & 0xffff);
	}
	TrisState.cache.put (TrisState.perfecthash, e);

	return g; 
    }

    static int mtdf1 (Node root, int f, int depth) {
	int g = f;
	int upper = Integer.MAX_VALUE;
	int lower = -upper;
	while (lower < upper) {
	    int beta;
	    if (g == lower) {
		beta = g+1;
	    } else {
		beta = g;
	    }
	    g = alphabetaMTD1 (root, depth, beta-1, beta);
	    if (g < beta) {
		upper = g;
	    } else {
		lower = g;
	    }
	}
	return g;
    }

    public static void main (String[] args) {
	TrisState.player = 1; // X
	TrisNode root = new TrisNode (true);

	// warm up
	alphabeta1 (root, 9, -Integer.MAX_VALUE, Integer.MAX_VALUE);

	long start;
	int result;

	start = System.currentTimeMillis ();
	result = alphabeta1 (root, 9, -Integer.MAX_VALUE, Integer.MAX_VALUE);
	System.out.println ("Alphabeta1 ("+result+"): "+(System.currentTimeMillis () - start));

	start = System.currentTimeMillis ();
	result = alphabeta2 (root, 9, -Integer.MAX_VALUE, Integer.MAX_VALUE);
	System.out.println ("Alphabeta2 ("+result+"): "+(System.currentTimeMillis () - start));

	start = System.currentTimeMillis ();
	result = negascout1 (root, 9, -Integer.MAX_VALUE, Integer.MAX_VALUE);
	System.out.println ("Negascout1 ("+result+"): "+(System.currentTimeMillis () - start));

	start = System.currentTimeMillis ();
	result = negascout2 (root, 9, -Integer.MAX_VALUE, Integer.MAX_VALUE);
	System.out.println ("Negascout2 ("+result+"): "+(System.currentTimeMillis () - start));

	start = System.currentTimeMillis ();
	result = mtdf1 (root, 4, 9); // worst case
	System.out.println ("MTD(f)1 ("+result+"): "+(System.currentTimeMillis() - start));

    }
}
