package ai;

import java.util.*;
import java.util.concurrent.*;

class Node {
    static long[][] adjacent = new long[8*8][8];
    static long[][] masks = new long[8*8][4];
    static long[] flipped = new long[8*8];

    static long hmirror (long b) {
	return ((b << 7) & 0x8080808080808080L) |
	    ((b << 5) & 0x4040404040404040L) |
	    ((b << 3) & 0x2020202020202020L) |
	    ((b << 1) & 0x1010101010101010L);
    }

    static long vmirror (long b) {
	return ((b << (7*8)) & 0xFF00000000000000L) |
	    ((b << (5*8)) & 0xFF000000000000L) |
	    ((b << (3*8)) & 0xFF0000000000L) |
	    ((b << (1*8)) & 0xFF00000000L);
    }

    /* X....... */
    static final long hb1 = 1;
    /* .X......
     * XX...... */
    static final long hb2 = (1L<<1)|(1L<<(1*8))|(1L<<(1*8+1));
    /* ..X.....
     * ........
     * X.X..... */
    static final long hb3 = (1L<<2)|(1L<<(2*8))|(1L<<(2*8+2));
    /* ...X....
     * ........
     * ........
     * X....... */
    static final long hb4 = (1L<<3)|(1L<<(3*8));

    static final long corners[] = new long[]{
	hb1|hmirror(hb1)|vmirror(hb1|hmirror(hb1)), 1000,
	hb2|hmirror(hb2)|vmirror(hb2|hmirror(hb2)), -50,
	hb3|hmirror(hb3)|vmirror(hb3|hmirror(hb3)), 40,
	hb4|hmirror(hb4)|vmirror(hb4|hmirror(hb4)), 40,
    };

    static final long border = 0xFFL | 0x101010101010101L | 0x8080808080808080L | 0xFF00000000000000L;

    static void printboard (long b) {
	System.out.println ("bitboard: "+b);
	for (int i=0; i < 8; i++) {
	    for (int j=0; j < 8; j++) {
		System.out.print (" "+((b >> (i*8+j))&1));
	    }
	    System.out.println ();
	}
    }

    static final long k1 = 0x5555555555555555L;
    static final long k2 = 0x3333333333333333L;
    static final long k4 = 0x0f0f0f0f0f0f0f0fL;

    static final double base2 = Math.log (2);

    static int popcount (long x) {
	x -= ((x >> 1) & k1);
	x = (x & k2) + ((x >> 2) & k2);
	x = (x + (x >> 4)) & k4;
	x += x >> 8;
	x += x >> 16;
	x += x >> 32;
	return (int) x & 255;
    }

    static final int MAX_ENTRIES = 1000000;
    static LinkedHashMap<Node, Node> transposition = new LinkedHashMap<Node, Node> (2097143) {
	@Override
	protected boolean removeEldestEntry (Map.Entry eldest) {
	    return size() > MAX_ENTRIES;
	}
    };

    static {
	// attack masks
	for (int i=0; i < 8; i++) {
	    for (int j=0; j < 8; j++) {
		long[] m = masks[i*8+j];
		long[] a = adjacent[i*8+j];
		// down (first bit)
		if (i+2 < 8) {
		    for (int ii=i+2; ii < 8; ii++) m[0] |= 1L << (ii*8+j);
		    a[0] |= 1L << ((i+1)*8+j);
		}
		// up (last bit)
		if (i-2 >= 0) {
		    for (int ii=i-2; ii >= 0; ii--) m[0] |= 1L << (ii*8+j);
		    a[1] |= 1L << ((i-1)*8+j);
		}

		// right (first bit)
		if (j+2 < 8) {
		    for (int jj=j+2; jj < 8; jj++) m[1] |= 1L << (i*8+jj);
		    a[2] |= 1L << (i*8+j+1);
		}
		// left (last bit)
		if (j-2 >= 0) {
		    for (int jj=j-2; jj >= 0; jj--) m[1] |= 1L << (i*8+jj);
		    a[3] |= 1L << (i*8+j-1);
		}

		// main diagonal down (first bit)
		if (i+2 < 8 && j+2 < 8) {
		    for (int ii=i+2, jj=j+2; ii < 8 && jj < 8; ii++, jj++) m[2] |= 1L << (ii*8+jj);
		    a[4] |= 1L << ((i+1)*8+j+1);
		}
		// main diagonal up (last bit)
		if (i-2 >= 0 && j-2 >= 0) {
		    for (int ii=i-2, jj=j-2; ii >= 0 && jj >= 0; ii--, jj--) m[2] |= 1L << (ii*8+jj);
		    a[5] |= 1L << ((i-1)*8+j-1);
		}

		// secondary diagonal down (first bit)
		if (i+2 < 8 && j-2 >= 0) {
		    for (int ii=i+2, jj=j-2; ii < 8 && jj >= 0; ii++, jj--) m[3] |= 1L << (ii*8+jj);
		    a[6] |= 1L << ((i+1)*8+j-1);
		}
		// secondary diagonal up (last bit)
		if (i-2 >= 0 && j+2 < 8) {
		    for (int ii=i-2, jj=j+2; ii >= 0 && jj < 8; ii--, jj++) m[3] |= 1L << (ii*8+jj);
		    a[7] |= 1L << ((i-1)*8+j+1);
		}
	    }
	}
    }

    long me = 0L;
    long adv = 0L;
    int black;
    long possibleMoves;
    int lowerbound;
    int upperbound;
    int depth;
    int pv;

    Node (int black, long me, long adv) {
	this.black = black;
	this.me = me;
	this.adv = adv;
    }

    Node createChild (long flipped, long emptySquare) {
	return new Node (1-black, adv & ~flipped, me | flipped);
    }

    void computeMoves () {
	possibleMoves = possibleMoves (me, adv);
    }

    static long possibleMoves (long me, long adv) {
	long empty = (~me) & (~adv);
	long attacksmask = 0L;

	long tmp = me;
	while (tmp != 0) {
	    // find our first occupied position
	    long bitvalue = tmp & -tmp;
	    // unset the position for the next cycle
	    tmp &= ~bitvalue;
	    // bit index occupied by me
	    int bit = (int)(Math.log (((double)(bitvalue-1))+1) / base2);

	    long[] masks = Node.masks[bit];
	    long[] adjacent = Node.adjacent[bit];
	    for (int z=0; z < 4; z++) {
		long mask = masks[z];
		long match = mask & ~adv;
		// first bit
		long adj = adjacent[z*2];
		if ((adv & adj) != 0) {
		    // search first bit, adjacent is adversary!
		    // get mask for the first bit
		    long mask1 = mask & ~(bitvalue - 1);
		    // get first non-adversary square
		    long m = match & mask1;
		    // don't spend any time if it's zero
		    if (m != 0) {
			m &= -m; // gosper's hack
			if ((empty & m) != 0) {
			    // empty position, we can go there!
			    int attackbit = (int)(Math.log (((double)(m-1))+1) / base2);
			    long localflip = adj | (((m<<1)-1) & mask1);
			    // clear garbage
			    if ((attacksmask & m) == 0) {
				flipped[attackbit] = localflip;
				attacksmask |= m;
			    } else {
				flipped[attackbit] |= localflip;
			    }
			}
		    }
		}
		adj = adjacent[z*2+1];
		if ((adv & adj) != 0) {
		    // search last bit, adjacent is adversary!
		    // get mask for the last bit
		    long mask2 = mask & (bitvalue -1);
		    // get first non-adversary square
		    long m = match & mask2;
		    // don't spend any time if it's zero
		    if (m != 0) {
			m |= (m >> 1);
			m |= (m >> 2);
			m |= (m >> 4);
			m |= (m >> 8);
			m |= (m >> 16);
			m |= (m >> 32);
			m -= m >> 1;
			if ((empty & m) != 0) {
			    // empty position, we can go there!
			    int attackbit = (int)(Math.log (((double)(m-1))+1) / base2);
			    long localflip = adj | ((~(m-1)) & mask2);
			    // clear garbage
			    if ((attacksmask & m) == 0) {
				flipped[attackbit] = localflip;
				attacksmask |= m;
			    } else {
				flipped[attackbit] |= localflip;
			    }
			}
		    }
		}
	    }
   	}
	return attacksmask;
    }

    ArrayList<Node> children () {
	ArrayList<Node> r = new ArrayList<Node> ();
	long attacksmask = possibleMoves;
	// "attacksmask" = possible attacks,
	while (attacksmask != 0) {
	    // find our first attack
	    long bitvalue = attacksmask & -attacksmask;
	    // unset the position for the next cycle
	    attacksmask &= ~bitvalue;
	    // bit index of the attack
	    int bit = (int)(Math.log (((double)(bitvalue-1))+1) / base2);
	    r.add (createChild (flipped[bit], bitvalue));
	}
	return r;
    }

    static int heuristicType = 0;

    int heuristic () {
	if (heuristicType == 1) {
	    return popcount (me) - popcount (adv);
	} else {
	    int strategic = 0;
	    for (int i=0; i < corners.length; i+=2) {
		long h1 = corners[i];
		long h2 = corners[i+1];
		strategic += (int)(popcount (me & h1) * h2);
		strategic -= (int)(popcount (adv & h1) * h2);
	    }
	    /*strategic += (int)(popcount (me & border));
	      strategic -= (int)(popcount (adv & border));*/
	    int mobility = popcount (possibleMoves);
	    mobility -= popcount (possibleMoves (adv, me));
	    return strategic + mobility*3;
	}
    }

    void print () {
	System.out.println ("---");
	for (int i=0; i < 8; i++) {
	    for (int j=0; j < 8; j++) {
		System.out.print (" ");
		long player = ((me >> (i*8+j)) & 1) | (((adv >> (i*8+j)) & 1) << 1);
		System.out.print (" "+(player==0?'.':((((player&1)^black)==0?'X':'O'))));
	    }
	    System.out.println ();
	}
    }

    Node nextmove (boolean quiet) {
	Node e = transposition.get (this);
	if (e == null) {
	    return null;
	}
	e.computeMoves ();
	if (e.possibleMoves == 0) {
	    return null;
	}
	Node child = e.children().get (e.pv);
	Node c = transposition.get (child);
	if (!quiet) {
	    long movemask = (~(me | adv)) ^ (~(child.me | child.adv));
	    int movebit = (int)(Math.log (((double)(movemask-1))+1) / base2);
	    int i = movebit/8;
	    int j = movebit%8;
	    System.out.println ("MOVE (i="+i+", j="+j+", pv="+e.pv+"):");
	    printboard (movemask);
	    System.out.println ("NEW BOARD:");
	    child.print ();
	}
	return child;
    }

    Node findmove (int i, int j) {
	computeMoves ();
	for (Node child: children ()) {
	    long movemask = (~(me | adv)) ^ (~(child.me | child.adv));
	    int movebit = (int)(Math.log (((double)(movemask-1))+1) / base2);
	    int ii = movebit/8;
	    int jj = movebit%8;
	    if (i == ii && j == jj) {
		return child;
	    }
	}
	return null;
    }

    @Override
    public boolean equals (Object o) {
	Node n = (Node) o;
	return (black == n.black && me == n.me && adv == n.adv);
    }

    @Override
    public int hashCode () {
	return (int)(~(me | adv) ^ ~(~(me | adv) >> 32));
    }
}

class SearchThread extends Thread {
    public void run () {
	boolean passed = false;
	while (true) {
	    Node root = OthelloTime.root;
	    OthelloTime.changedRoot = false;

	    System.out.println ("SEARCHING MOVE (me="+root.me+", adv="+root.adv+"):");
	    root.print ();

	    long start = System.currentTimeMillis ();
	    OthelloTime.TIME_LIMIT = start + OthelloTime.MAX_ELAPSED;
	    int result = OthelloTime.iidmtdf (root);
	    System.out.println ("MTD(f)+IID ("+result+"): "+(System.currentTimeMillis()-start));

	    Node tmp = root.nextmove (false);
	    if (tmp == null) {
		if (passed) {
		    System.out.println ("END GAME!");
		    break;
		}
		root = root.createChild (0, 0);
		passed = true;
		System.out.println ("WE PASS!");
	    } else {
		root = tmp;
		passed = false;
	    }

	    root.computeMoves ();
	    if (root.possibleMoves == 0) {
		// adversary pass
		if (passed) {
		    System.out.println ("END GAME!");
		    break;
		}
		root = root.createChild (0, 0);
		OthelloTime.root = root;
		passed = true;
		System.out.println ("ADVERSARY PASS!");
		continue;
	    } else {
		passed = false;
		OthelloTime.root = root;
		System.out.println ("i j: ");
	    }

	    start = System.currentTimeMillis ();
	    OthelloTime.TIME_LIMIT = start + OthelloTime.MAX_ELAPSED;
	    /* SEARCH WHILE IDLE
	     * NOTA: nel progetto finale il limite di tempo non servirà, ci serve solo sapere capire quando riceviamo la mossa dell'avversario
	     * in questo caso simuliamo la risposta dell'avversario suppendo che impieghi proprio OthelloTime.MAX_ELAPSED*/
	    OthelloTime.iidmtdf (root);
	    /* Caso in cui terminiamo il tempo ma l'utente non ha ancora inserito la mossa. NOTA: non servirà nel progetto finale */
	    if (!OthelloTime.changedRoot) {
		// waiting for user input
		try {
		    OthelloTime.mutex.acquire ();
		    OthelloTime.mutex.release ();
		} catch (Exception e) {
		    e.printStackTrace ();
		}
	    }
	}
    }
}

public class OthelloTime {
    static long TIME_LIMIT;
    static int REACHED_DEPTH;
    static final long MAX_ELAPSED = 500;
    static volatile boolean changedRoot;
    static Node root;
    // FIXME: non servirà nel progetto finale
    static Semaphore mutex = new Semaphore (1, true);

    static int negascout (Node n, int depth, int alpha, int beta) {
	n.computeMoves ();
	if (depth == 0) {
	    return n.heuristic ();
	}

	int b = beta;
	boolean firstChild = true;
	for (Node child: n.children ()) {
	    int score = -negascout (child, depth-1, -b, -alpha);
	    if (score > alpha && score < beta && !firstChild) {
		score = -negascout (child, depth-1, -beta, -alpha);
	    }
	    alpha = Math.max (alpha, score);
	    if (beta <= alpha) {
		break;
	    }
	    firstChild = false;
	    b = alpha + 1;
	}
	return alpha;
    }
    static boolean first8 = true;
    static int alphabetaPV (Node n, int depth, int alpha, int beta, boolean nullmove) {
	n.computeMoves ();
	if (n.possibleMoves == 0) {
	    if (!nullmove) {
		return -alphabetaPV (n.createChild (0, 0), depth, -beta, -alpha, true);
	    } else {
		return n.heuristic ();
	    }
	}
	if (depth == 0) {
	    return n.heuristic ();
	}

	// principal variation
	int pv = 0;

	// transposition table lookup
	Node e = Node.transposition.get (n);
	if (e != null) {
	    if (e.depth >= depth) {
		// good hit
		if (e.lowerbound >= beta || e.lowerbound == e.upperbound) {
		    return e.lowerbound;
		}
		if (e.upperbound <= alpha) {
		    return e.upperbound;
		}
		alpha = Math.max (alpha, e.lowerbound);
		beta = Math.min (beta, e.upperbound);
	    }
	    pv = e.pv;
	}
	if (first8 && depth==8)first8 = false;
	List<Node> children = n.children ();
	int size = children.size ();
	boolean firstChild = true;
	int g = -Short.MAX_VALUE;
	int a = alpha;
	for (int i=pv; i < size; i++) {
	    if (i == pv && !firstChild) {
		// skip PV
		continue;
	    }
	    Node child = children.get (i);
	    int score = -alphabetaPV (child, depth-1, -beta, -a, false);
	    if (changedRoot || System.currentTimeMillis() > TIME_LIMIT) {
		return g;
	    }
	    if (score > g) {
		g = score;
		pv = i;
		a = Math.max (a, g);
	    }
	    if (beta <= a) {
		break;
	    }
	    if (firstChild) {
		i=-1;
		firstChild = false;
	    }
	}

	// transposition table storing
	if (e == null) {
	    e = n;
	    // first time seen
	    Node.transposition.put (e, e);
	    e.depth = depth;
	} else { 
	    // bypass the hashmap
	    if (depth < e.depth) {
		// worsen information, don't store
		return g;
	    } else {
		e.depth = depth;
	    }
	}
	if (g <= alpha) {
	    e.upperbound = g;
	    e.lowerbound = -Short.MAX_VALUE;
	} else if (g > alpha && g < beta) {
	    e.lowerbound = e.upperbound = g;
	} else {
	    e.lowerbound = g;
	    e.upperbound = Short.MAX_VALUE;
	}
	e.pv = pv;

	return g;
    }

    static int mtdf (Node root, int f, int depth) {
	int g = f;
	int upper = Short.MAX_VALUE;
	if (Node.heuristicType == 1) {
	    upper = 1;
	}
	int lower = -upper;
	while (lower < upper) {
	    int beta;
	    if (g == lower) {
		beta = g+1;
	    } else {
		beta = g;
	    }
	    g = alphabetaPV (root, depth, beta-1, beta, false);
	    if (changedRoot || System.currentTimeMillis() > TIME_LIMIT) {
		return g;
	    }
	    if (g < beta) {
		upper = g;
	    } else {
		lower = g;
	    }
	}
	return g;
    }

    static int iidmtdf (Node root) {
	int g = -Short.MAX_VALUE;
	int d = 4;

	Node e = Node.transposition.get (root);
	if (e != null) {
	    if (e.lowerbound == e.upperbound) {
		// exact information!!!
		System.out.println ("YUPPIEEEEEEEEEEE");
		g = e.lowerbound;
		d = e.depth+1;
	    } else {
		d = e.depth;
	    }
	}
	while (true) {
	    int tmp = mtdf (root, g, d);
	    if (changedRoot || System.currentTimeMillis() > TIME_LIMIT) {
		REACHED_DEPTH = d-1;
		System.out.println ("DEPTH REACHED: "+(d-1));
		return g;
	    }
	    g = tmp;
	    d++;
	}
    }

    static int iidalphabeta (Node root) {
	int g = -Short.MAX_VALUE;
	int u = Short.MAX_VALUE;
	if (Node.heuristicType == 1) {
	    u = 1;
	}
	int d = 4;
	while (true) {
	    int tmp = alphabetaPV (root, d, -u, u, false);
	    if (System.currentTimeMillis() > TIME_LIMIT) {
		System.out.println ("DEPTH REACHED: "+d);
		return g;
	    }
	    g = tmp;
	    d++;
	}
    }

    public static void main (String[] args) {
	// black move first
	long adv = 1L << (3*8+3);
	long me = 1L << (3*8+4);
	me |= 1L << (4*8+3);
	adv |= 1L << (4*8+4);
	root = new Node (1, me, adv);
	
	me=17593012404544L; adv=4341241337245253310L;
	root = new Node (1, me, adv);

	SearchThread search = new SearchThread ();
	search.start ();

	Scanner sc = new Scanner (System.in);

	while (true) {
	    // FIXME: il mutex non servirà a fine progetto
	    try {
		mutex.acquire ();
	    } catch (Exception e) {
		e.printStackTrace ();
	    }
	    Node tmp;
	    while (true) {
		int i = sc.nextInt ();
		int j = sc.nextInt (); 
		tmp = root.findmove (i, j);
		if (tmp == null) {
		    System.out.println ("ILLEGAL MOVE");
		} else {
		    break;
		}
	    }
	    root = tmp;
	    changedRoot = true;
	    try {
		mutex.release ();
	    } catch (Exception e) {
		e.printStackTrace ();
	    }
	}
    }
}
