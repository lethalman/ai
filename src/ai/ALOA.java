package ai;

import java.util.*;
import java.util.concurrent.Exchanger;
import java.util.concurrent.TimeUnit;
import java.io.*;
import java.net.*;

public class ALOA implements Serializable {
    // whether to use a timer or a maximum depth
    private static boolean USE_TIMER = false;
    private static int MAX_DEPTH = 5;
    private static long MAX_ELAPSED = 900;
    private static int NODE_RESOLUTION = 3000;
    private static int MAX_TRANSPOSITION_ENTRIES = 100000;
    private static int MAX_MEMORY_NODES = 2000000;
    private static boolean DEBUG = true;
    private static boolean PONDERING = true;
    private static boolean MULTICUT_CUT = false;
    private static boolean MULTICUT_ALL = false;
    private static boolean NULLMOVE_HEURISTIC = false;
    private static boolean LATE_MOVE_REDUCTIONS = false;

    private static final int[] euler4Table = new int[512];
    private static final int[] q3Table = new int[512];
    private static final int[] q4Table = new int[512];
    private static final Configuration[][] linesConf = new Configuration[32][6561];
    private static HashMap<BookEntry, Integer> openingBook;

    private static volatile boolean networkPlay;

    private int firstBlack;
    private int playerBlack;
    public Node root;
    private int adversaryMove;
    private long deadline;
    private volatile boolean changedRoot;
    private final Exchanger inputExchanger = new Exchanger ();
    private int nodeCount = 0;
    private int leafCount = 0;
    private int qCount = 0;
    private int moveCount = 0;
    private int evaluationPhase = 0;
    private double[] weights;
    private int reachedDepth;
    private int[] historyHeuristic = new int[64*64];
    private int[] butterflyHeuristic = new int[64*64];
    private boolean stopSearch;

    /* NODE POOL */
    private final Node[] nodePool = new Node[MAX_MEMORY_NODES];
    private int nodePoolTop = 0;

    private final Node createNode (int black, long me, long adv) {
	if (nodePoolTop == 0) {
	    return new Node (black, me, adv);
	} else {
	    Node res = nodePool[--nodePoolTop];
	    res.black = black;
	    res.me = me;
	    res.adv = adv;
	    res.cached = false;
	    res.standpat = res.depth = res.pv = 0;
	    res.refcount = 1;
	    return res;
	}
    }

    /* CHILDRENLIST POOL */
    private final ChildrenList[] childrenPool = new ChildrenList[MAX_MEMORY_NODES];
    private int childrenPoolTop = 0;

    private final ChildrenList createChildrenList () {
	if (childrenPoolTop == 0) {
	    return new ChildrenList ();
	} else {
	    ChildrenList res = childrenPool[--childrenPoolTop];
	    res.size = 0;
	    return res;
	}
    }

    private final void releaseChildrenList (ChildrenList c) {
	childrenPool[childrenPoolTop++] = c;
    }

    /* LINES POOL */
    private final short[][] linesPool = new short[MAX_MEMORY_NODES][];
    private int linesPoolTop = 0;

    private final short[] createLines () {
	if (linesPoolTop == 0) {
	    return new short[32];
	} else {
	    return linesPool[--linesPoolTop];
	}
    }

    private final void releaseLines (short[] c) {
	linesPool[linesPoolTop++] = c;
    }

    private static class Configuration {
	int[][] moves = new int[2][];
	int[][] tacticalMoves = new int[2][];
	byte[] comDistancesWhite; // available only for the first 8 lines
	byte[] comDistancesBlack; // available only for the first 8 lines
	double mobilityWhite;
	double mobilityBlack;
    }

    //ASSEGNAMO I PESI ALLE POSIZIONI DELLA SCACCHIERA
    /* from LOA */
    private static final int[] centralisationLOAL = {
	0, 0, 0, 0, 0, 0, 0, 0,
	0, 1, 1, 1, 1, 1, 1, 0,
	0, 1, 5, 5, 5, 5, 1, 0,
	0, 1, 5, 10, 10, 5, 1, 0,
	0, 1, 5, 10, 10, 5, 1, 0,
	0, 1, 5, 5, 5, 5, 1, 0,
	0, 1, 1, 1, 1, 1, 1, 0,
	0, 0, 0, 0, 0, 0, 0, 0};

    /* from the pdf */
    private static final int centralisationTable[] = {
	-80, -25, -20, -20, -20, -20, -25, -80,
	-25, 10, 10, 10, 10, 10, 10, -25,
	-20, 10, 25, 25, 25, 25, 10, -20,
	-20, 10, 25, 50, 50, 25, 10, -20,
	-20, 10, 25, 50, 50, 25, 10, -20,
	-20, 10, 25, 25, 25, 25, 10, -20,
	-25, 10, 10, 10, 10, 10, 10, -25,
	-80, -25, -20, -20, -20, -20, -25, -80 };

    public static void printboard (long b) {
	System.out.println ("bitboard: "+b);
	for (int i=0; i < 8; i++) {
	    for (int j=0; j < 8; j++) {
		System.out.print (" "+((b >> (i*8+j))&1));
	    }
	    System.out.println ();
	}
    }
    //conta i bit di un long
    private static final long k1 = 0x5555555555555555L;
    private static final long k2 = 0x3333333333333333L;
    private static final long k4 = 0x0f0f0f0f0f0f0f0fL;
    private static final long h01 = 0x0101010101010101L;

    private static final double base2 = Math.log (2);

    private static int popcount (long x) {
	x -= (x >>> 1) & k1;
	x = (x & k2) + ((x >>> 2) & k2);
	x = (x + (x >>> 4)) & k4;
	return (int)((x * h01) >>> 56);
    }

    private static int popcount32 (int x) {
	x -= (x >>1) & 0x55555555;
	x = (x & 0x33333333) + ((x >>> 2) & 0x33333333);
	x = (x + (x >>> 4)) & 0x0f0f0f0f;
	x = x + (x >> 8);
	return (x + (x >> 16)) & 0x0000003F;
    }

    private static long transpose (long b) {
	// transpose
	long res = 0L;
	while (b != 0) {
	    long posbit = b & -b;
	    b &= ~posbit;
	    int pos = (int)(Math.log (((double)(posbit-1))+1) / base2);
	    int i = pos/8;
	    int j = pos%8;
	    res |= 1L << (j*8+i);
	}
	return res;
    }

    private final LinkedHashMap<Node, Node> transposition = new LinkedHashMap<Node, Node> (297143) {
	@Override
	protected boolean removeEldestEntry (Map.Entry eldest) {
	    Node n = (Node) eldest.getValue ();
	    if (n != root && size() > MAX_TRANSPOSITION_ENTRIES) {
		n.unref ();
		n.cached = false;
		return true;
	    }
	    return false;
	}
    };

    static {
	int[] etable = new int[512];
	int[] q3table = new int[512];
	int[] q4table = new int[512];
        for (int i=0; i < 512; i++) {
	    int evalue = 0;
	    int q3value = 0;
	    int q4value = 0;
	    int quad = i & 27;
	    if (popcount (quad) == 1) {
		evalue++;
	    } else if (popcount (quad) == 3) {
		evalue--;
		q3value++;
	    } else if (quad == 17 || quad == 10) {
		evalue -= 2;
	    } else if (popcount (quad) == 4) {
		q4value++;
	    }

	    quad = i & 54;
	    if (popcount (quad) == 1) {
		evalue++;
	    } else if (popcount (quad) == 3) {
		evalue--;
		q3value++;
	    } else if (quad == 34 || quad == 20) {
		evalue -= 2;
	    } else if (popcount (quad) == 4) {
		q4value++;
	    }

	    quad = i & 216;
	    if (popcount (quad) == 1) {
		evalue++;
	    } else if (popcount (quad) == 3) {
		evalue--;
		q3value++;
	    } else if (quad == 136 || quad == 80) {
		evalue -= 2;
	    } else if (popcount (quad) == 4) {
		q4value++;
	    }

	    quad = i & 432;
	    if (popcount (quad) == 1) {
		evalue++;
	    } else if (popcount (quad) == 3) {
		evalue--;
		q3value++;
	    } else if (quad == 272 || quad == 160) {
		evalue -= 2;
	    } else if (popcount (quad) == 4) {
		q4value++;
	    }

	    etable[i] = evalue;
	    q3table[i] = q3value;
	    q4table[i] = q4value;
	}

	for (int i=0; i < 512; i++) {
	    euler4Table[i] = -etable[i] + etable[i ^ 16];
	    q3Table[i] = -q3table[i] + q3table[i ^ 16];
	    q4Table[i] = -q4table[i] + q4table[i ^ 16];
	}

	// configurations
	int n = 6561;
	for (int conf=0; conf < n; conf++) {
	    for (int i=0; i < 8; i++) {
		// rows
		Configuration c = new Configuration ();
		ArrayList<Integer> wmoves = new ArrayList<Integer> ();
		ArrayList<Integer> bmoves = new ArrayList<Integer> ();
		ArrayList<Integer> twmoves = new ArrayList<Integer> ();
		ArrayList<Integer> tbmoves = new ArrayList<Integer> ();
		fillConf (c, conf, wmoves, bmoves, twmoves, tbmoves, i, 0, 0, 1, 0, 7, i == 0 || i == 7);
		c.moves[0] = sortedMoves (wmoves);
		c.moves[1] = sortedMoves (bmoves);
		c.tacticalMoves[0] = sortedMoves (twmoves);
		c.tacticalMoves[1] = sortedMoves (tbmoves);
		linesConf[i][conf] = c;
		// centre of mass distances
		c.comDistancesWhite = new byte[8*8];
		c.comDistancesBlack = new byte[8*8];
		for (int com=0; com < 64; com++) {
		    int icom = com/8;
		    int jcom = com%8;
		    int wdistances = 0;
		    int bdistances = 0;
		    for (int j=0; j < 8; j++) {
			int value = (conf/(int)Math.pow (3,j))%3;
			int difrow = Math.abs (icom - i);
			int difcol = Math.abs (jcom - j);
			if (value == 1) {
			    wdistances += Math.max (difrow, difcol);
			} else if (value == 2) {
			    bdistances += Math.max (difrow, difcol);
			}
		    }
		    c.comDistancesWhite[com] = (byte)wdistances;
		    c.comDistancesBlack[com] = (byte)bdistances;
		}

		// columns
		c = new Configuration ();
		wmoves = new ArrayList<Integer> ();
		bmoves = new ArrayList<Integer> ();
		twmoves = new ArrayList<Integer> ();
		tbmoves = new ArrayList<Integer> ();
		fillConf (c, conf, wmoves, bmoves, twmoves, tbmoves, 0, i, 1, 0, 0, 7, i == 0 || i == 7);
		c.moves[0] = sortedMoves (wmoves);
		c.moves[1] = sortedMoves (bmoves);
		c.tacticalMoves[0] = sortedMoves (twmoves);
		c.tacticalMoves[1] = sortedMoves (tbmoves);
		linesConf[i+8][conf] = c;

		// main diagonal
		c = new Configuration ();
		wmoves = new ArrayList<Integer> ();
		bmoves = new ArrayList<Integer> ();
		twmoves = new ArrayList<Integer> ();
		tbmoves = new ArrayList<Integer> ();
		fillConf (c, conf, wmoves, bmoves, twmoves, tbmoves, 7-i, 0, 1, 1, 0, i, false);
		fillConf (c, conf, wmoves, bmoves, twmoves, tbmoves, 0, i+1, 1, 1, i+1, 7, false);
		c.moves[0] = sortedMoves (wmoves);
		c.moves[1] = sortedMoves (bmoves);
		c.tacticalMoves[0] = sortedMoves (twmoves);
		c.tacticalMoves[1] = sortedMoves (tbmoves);
		linesConf[i+16][conf] = c;

		// secondary diagonal
		c = new Configuration ();
		wmoves = new ArrayList<Integer> ();
		bmoves = new ArrayList<Integer> ();
		twmoves = new ArrayList<Integer> ();
		tbmoves = new ArrayList<Integer> ();
		fillConf (c, conf, wmoves, bmoves, twmoves, tbmoves, 0, i, 1, -1, 0, i, false);
		fillConf (c, conf, wmoves, bmoves, twmoves, tbmoves, i+1, 7, 1, -1, i+1, 7, false);
		c.moves[0] = sortedMoves (wmoves);
		c.moves[1] = sortedMoves (bmoves);
		c.tacticalMoves[0] = sortedMoves (twmoves);
		c.tacticalMoves[1] = sortedMoves (tbmoves);
		linesConf[i+24][conf] = c;
	    }
	}
    }

    static int[] sortedMoves (ArrayList<Integer> list) {
	int[] r = new int[list.size()];
	int size = 0;
	for (int i=0; i < r.length; i++) {
	    int move = list.get (i);
	    int from = move & 63;
	    int to = (move >> 6) & 63;
	    int eval = move >> 16;

	    // insertion sort
	    int j=0;
	    for (; j < size; j++) {
		int val = r[j] >> 16;
		if (eval > val) {
		    // shift
		    for (int k=size; k > j; k--) {
			r[k] = r[k-1];
		    }
		    break;
		}
	    }
	    r[j] = move;
	    size++;
	}
	return r;
    }

    static void fillConf (Configuration c, int conf, ArrayList<Integer> wmoves, ArrayList<Integer> bmoves, ArrayList<Integer> twmoves, ArrayList<Integer> tbmoves, int rowstart, int colstart, int rowdir, int coldir, int from, int to, boolean border) {
	// white=1, black=2
	fillMoves (c, conf, wmoves, twmoves, 1, rowstart, colstart, rowdir, coldir, from, to, border);
	fillMoves (c, conf, bmoves, tbmoves, 2, rowstart, colstart, rowdir, coldir, from, to, border);
    }

    static void fillMove (Configuration c, int conf, ArrayList<Integer> moves, ArrayList<Integer> tmoves, int color, int rowstart, int colstart, int rowdir, int coldir, int from, int to, boolean border, int source, int dest, int dir) {
	int sourcecolor = (conf/(int)Math.pow (3, source))%3;
	int destcolor = (conf/(int)Math.pow (3, dest))%3;
	if (destcolor == color) {
	    return;
	}
	for (int j=source+dir; j != dest; j+=dir) {
	    int middle = (conf/(int)Math.pow (3, j))%3;
	    if (middle != 0 && middle != color) {
		// adversary in the middle
		return;
	    }
	}
	double mobility = 1;
	int sourcepos = (rowstart+(source-from)*rowdir)*8 + (colstart+(source-from)*coldir);
	int destpos = (rowstart+(dest-from)*rowdir)*8 + (colstart+(dest-from)*coldir);
	int move = sourcepos | (destpos << 6);
	int eval = -centralisationTable[sourcepos] + centralisationTable[destpos];
	boolean tmove = false;
	if (destcolor != 0) { // capture
	    move |= 1 << 12;
	    mobility *= 2;
	    eval += centralisationTable[destpos];
	    eval += 150;
	    tmove = true;
	}
	if (true) {
	    if (dest-1 >= from && dest-1 != source) {
		int near = (conf/(int)Math.pow (3, dest-1))%3;
		if (near == color) {
		    // possible grouping
		    eval += 50;
		    tmove = true;
		}
	    }
	    if (dest+1 <= to && dest+1 != source) {
		int near = (conf/(int)Math.pow (3, dest+1))%3;
		if (near == color) {
		    // possible grouping
		    eval += 50;
		    tmove = true;
		}
	    }
	}
	if (border || dest == from || dest == to) {
	    mobility /= 2;
	    eval -= 100;
	} else if (!border && (dest == from+1 || dest == to-1)) {
	    //eval += 50;
	}
	if (sourcecolor == 1) {
	    c.mobilityWhite += mobility;
	} else {
	    c.mobilityBlack += mobility;
	}

	move |= eval << 16;
	moves.add (move);
	if (tmove) {
	    tmoves.add (move);
	}
    }

    static void fillMoves (Configuration c, int conf, ArrayList<Integer> moves, ArrayList<Integer> tmoves, int color, int rowstart, int colstart, int rowdir, int coldir, int from, int to, boolean border) {
	// count stones
	int stones = 0;
	for (int i=from; i <= to; i++) {
	    int value = (conf/(int)Math.pow (3, i))%3;
	    if (value > 0) {
		stones++;
	    }
	}
	for (int source=from; source <= to; source++) {
	    int value = (conf/(int)Math.pow (3, source))%3;
	    if (value == color) {
		// compute possible moves
		if (source-stones >= from) {
		    fillMove (c, conf, moves, tmoves, color, rowstart, colstart, rowdir, coldir, from, to, border, source, source-stones, -1);
		}
		if (source+stones <= to) {
		    fillMove (c, conf, moves, tmoves, color, rowstart, colstart, rowdir, coldir, from, to, border, source, source+stones, 1);
		}
	    }
	}
    }

    // thanks tony
    private static boolean checkConnected (long x) {
	long reached = x & -x; // find first
	while (true) {
	    long f = reached | (reached << 8) | (reached >>> 8);
	    f |= ((f >>> 1) & ~0x8080808080808080L) | ((f << 1) & ~0x101010101010101L);
	    f &= x;
	    if (f == reached) {
		return f == x;
	    }
	    reached = f;
	}
    }

    private int qCountFrontier;

    private void setDeadline () {
	deadline = System.currentTimeMillis() + MAX_ELAPSED;
	stopSearch = false;
	qCount = 0;
	qCountFrontier = NODE_RESOLUTION;
    }

    private final boolean stopSearch () {
	if (stopSearch || qCount >= qCountFrontier) {
	    return stopSearch;
	}
	qCountFrontier = qCount + NODE_RESOLUTION;
	return USE_TIMER && (stopSearch = (changedRoot || ((System.currentTimeMillis() >= deadline) && USE_TIMER)));
    }

    private class SearchThread extends Thread {
	public void run () {
	    if (root.black != playerBlack) {
		// waiting for user input
		if (!networkPlay) {
		    if (DEBUG) {
			root.print ();
		    }
		    System.out.println ("X0-X0: ");
		}
		try {
		    inputExchanger.exchange (null);
		} catch (Exception e) {
		    e.printStackTrace ();
		}
		// apply adversary move
		Node tmp = root;
		ALOA.this.root = tmp.applymove (adversaryMove);
		tmp.unref ();
	    } else if (USE_TIMER) {
		setDeadline ();
	    }

	    BookEntry bookEntry = new BookEntry ();
	    Node predicted = null;
	    int predictionHits = 0;
	    int bookHits = 0;
	    while (true) {
		Node root = ALOA.this.root;
		changedRoot = false;

		if (DEBUG && root == predicted) {
		    System.out.println ("=== PREDICTION HIT "+(++predictionHits));
		}
		if (predicted != null) {
		    predicted.unref ();
		    predicted = null;
		}

		// first check adversary
		if (root.children().size == 0 || checkConnected (root.adv)) {
		    System.out.println ("WE LOST!");
		    break;
		} else if (checkConnected (root.me)) {
		    System.out.println ("WE WON!");
		    break;
		} else if (moveCount / 2 >= 100) {
		    System.out.println ("DRAW!");
		    break;
		}

		int nextMove;

		// lookup the opening book
		bookEntry.black = root.black;
		bookEntry.me = root.me;
		bookEntry.adv = root.adv;
		Integer bookMove = openingBook.get (bookEntry);
		if (bookMove != null) {
		    // OPENING BOOK
		    nextMove = bookMove;
		    if (DEBUG) {
			System.out.println ("=== OPENING BOOK HIT "+(++bookHits));
		    }
		} else {
		    // SEARCH
		    long start = 0L;
		    if (DEBUG) {
			System.out.println ("=== SEARCHING");
			root.print ();
			nodeCount = 0;
			leafCount = 0;
			qCount = 0;
			start = System.currentTimeMillis ();
		    }

		    Node e = transposition.get (root);
		    if (e != null) {
			e.unref ();
			transposition.remove (root);
		    }
		    transposition.put (root, root);
		    root.ref ();
		    // SEARCH ALGORITHM
		    int result = root.iidnegascout ();

		    if (DEBUG) {
			System.out.println ("Negascout ("+result+"): "+(System.currentTimeMillis()-start));
			if (USE_TIMER) {
			    System.out.println ("Reached depth: "+reachedDepth);
			}
			System.out.println ("Visited Nodes="+nodeCount+"; Leafs="+leafCount+"; Quiet="+qCount);
		    }

		    nextMove = root.pv;
		}

		boolean beforePondering = false;
		if (!stopSearch && PONDERING && moveCount/2 > 0) {
		    // don't ponder the first move
		    Node tmp = root;
		    root = root.applymove (nextMove);
		    tmp.unref ();
		    beforePondering = true;
		    if (root.pv == 0) {
			bookEntry.black = root.black;
			bookEntry.me = root.me;
			bookEntry.adv = root.adv;
			bookMove = openingBook.get (bookEntry);
			if (bookMove != null) {
			    root.pv = bookMove;
			}
		    }
		    if (root.pv != 0) {
			predicted = root.applymove (root.pv);
			predicted.iidnegascout ();
			moveCount--;
			if (DEBUG) {
			    System.out.println ("[BEFORE MOVE PONDERING] Reached depth: "+reachedDepth);
			}
		    }
		}

		if (networkPlay) {
		    String str = moveToString (nextMove);
		    socketOutput.println (str);
		}
		if (!beforePondering) {
		    // better call applymove after sending the move
		    Node tmp = root;
		    root = root.applymove (nextMove);
		    tmp.unref ();
		}
		if (DEBUG) {
		    System.out.println ("=== MOVE ("+moveToString (nextMove)+")");
		    System.out.println ("=== NEW BOARD:");
		    root.print ();
		}

		// first check us
		if (root.children().size == 0 || checkConnected (root.adv)) {
		    System.out.println ("WE WON!");
		    break;
		} else if (checkConnected (root.me)) {
		    System.out.println ("WE LOST!");
		    break;
		} else if (moveCount / 2 >= 100) {
		    System.out.println ("DRAW!");
		    break;
		} else {
		    ALOA.this.root = root;
		    System.out.println ("X0-X0: ");
		}

		if (moveCount >= weights[1]) {
		    if (DEBUG) {
			System.out.println ("* Second heuristic");
		    }
		    if (evaluationPhase == 0) {
			evaluationPhase = 9;
			transposition.clear ();
		    }
		}

		if (PONDERING) {
		    if (USE_TIMER && !networkPlay) {
			setDeadline ();
		    }
		    if (root.pv == 0) {
			bookEntry.black = root.black;
			bookEntry.me = root.me;
			bookEntry.adv = root.adv;
			bookMove = openingBook.get (bookEntry);
			if (bookMove != null) {
			    root.pv = bookMove;
			}
		    }
		    if (root.pv != 0) {
			if (!beforePondering) {
			    // pondering before move already calculated the predicted node
			    predicted = root.applymove (root.pv);
			}
			predicted.iidnegascout ();
			moveCount--;
			if (DEBUG) {
			    System.out.println ("[AFTER MOVE PONDERING] Reached depth: "+reachedDepth);
			}
		    }
		}

		// waiting for input
		try {
		    inputExchanger.exchange (null);
		} catch (Exception e) {
		    e.printStackTrace ();
		}
		// apply adversary move
		ALOA.this.root = root.applymove (adversaryMove);
		root.unref ();
	    }
	    System.exit (0);
	}
    }

    private static boolean doubleEquals (double d1, double d2) {
	return Math.abs(d1 - d2) < 0.00001;
    }

    private static double normalize (double me, double adv) {
	if (me == adv) {
	    return 0;
	}
	return (me - adv)/(Math.abs(me) + Math.abs(adv));
    }

    private static class ChildrenList {
	int[] moves = new int[12*8];
	int size;
    }

    public class Node implements Serializable {
	long me = 0L;
	long adv = 0L;
	int black;
	int refcount = 1;
	// caching
	int lowerbound = -Short.MAX_VALUE;
	int upperbound = Short.MAX_VALUE;
	int depth;
	int pv;
	public int standpat;
	boolean cached;
	// features
	short[] lines;
	int centralisationMe;
	int centralisationAdv;
	int stonesMe;
	int stonesAdv;
	int comIAxisMe;
	int comJAxisMe;
	int comIAxisAdv;
	int comJAxisAdv;
	int euler4Me, euler4Adv;
	int q3Me, q3Adv;
	int q4Me, q4Adv;

	Node (int black, long me, long adv) {
	    this.black = black;
	    this.me = me;
	    this.adv = adv;
	}

	void ref () {
	    refcount++;
	}

	void unref () {
	    if (--refcount == 0) {
		if (lines != null) {
		    releaseLines (lines);
		    lines = null;
		}
		nodePool[nodePoolTop++] = this;
	    }
	}

	private int evaluateSemiNormalized () {
	    if (standpat != 0) {
		return standpat;
	    }
	    final int quads3 = q3Me - q3Adv;
	    final int quads4 = q4Me - q4Adv;
	    final double connectedness = 1.0/Math.max (1, euler4Me/4) - 1.0/Math.max (1, euler4Adv/4);
	    final int sidetomove = 1 - (firstBlack ^ black);

	    // centralisation
	    final double centralisation = (centralisationMe/(double)stonesMe - centralisationAdv/(double)stonesAdv)/50.0;

	    // com
	    int icom = comIAxisMe / stonesMe;
	    int jcom = comJAxisMe / stonesMe;
	    final int comMe = icom*8+jcom;
	    icom = comIAxisAdv / stonesAdv;
	    jcom = comJAxisAdv / stonesAdv;
	    final int comAdv = icom*8+jcom;

	    // com centralisation
	    final double comCentralisation = (centralisationTable[comMe] - centralisationTable[comAdv])/50.0;

	    // mobility + com distances
	    double wmobility = 0;
	    double bmobility = 0;
	    int wcom = comMe;
	    int bcom = comAdv;
	    if (black == 1) {
		wcom = comAdv;
		bcom = comMe;
	    }
	    int wdistances = 0;
	    int bdistances = 0;
	    for (int i=32; --i >= 0;) {
		Configuration c = linesConf[i][lines[i]];
		wmobility += c.mobilityWhite;
		bmobility += c.mobilityBlack;
		if (i < 8) {
		    wdistances += c.comDistancesWhite[wcom];
		    bdistances += c.comDistancesBlack[bcom];
		}
	    }
	    int distancesMe;
	    int distancesAdv;
	    double mobility;
	    if (black == 1) {
		// precomputed values assume we are white
		mobility = (bmobility/stonesMe - wmobility/stonesAdv)/8.0;
		distancesMe = bdistances;
		distancesAdv = wdistances;
	    } else {
		mobility = (wmobility/stonesMe - bmobility/stonesAdv)/8.0;
		distancesMe = wdistances;
		distancesAdv = bdistances;
	    }
	    // concentration
	    int minDistances;
	    if (stonesMe < 10) {
		minDistances = stonesMe-1;
	    } else {
		minDistances = (stonesMe<<1)-10;
	    }
	    double concentration = 1.0/Math.max(1, distancesMe-minDistances);
	    if (stonesAdv < 10) {
		minDistances = stonesAdv-1;
	    } else {
		minDistances = (stonesAdv<<1)-10;
	    }
	    concentration -= 1.0/Math.max(1, distancesAdv-minDistances);

	    // material
	    double material = (stonesMe - stonesAdv)/(double)(stonesMe + stonesAdv);
	    final int phase = evaluationPhase;
	    standpat = (int)(1000*(weights[2+phase]*centralisation +
				   weights[3+phase]*comCentralisation +
				   weights[4+phase]*concentration +
				   weights[5+phase]*quads3 +
				   weights[6+phase]*quads4 +
				   weights[7+phase]*connectedness +
				   weights[8+phase]*mobility +
				   weights[9+phase]*material +
				   weights[10+phase]*sidetomove));
	    return standpat;
	}

	public Evaluation learningEvaluation (double[] weights) {
	    final double quads = normalize (q3Me+q4Me, q3Adv+q4Adv);
	    final double quads3 = normalize (q3Me, q3Adv);
	    final double quads4 = normalize (q4Me, q4Adv);
	    final double connectedness = normalize (1.0/Math.max (1, euler4Me/4), 1.0/Math.max (1, euler4Adv/4));
	    final int sidetomove = 1 - (firstBlack ^ black);

	    // centralisation
	    final double centralisation = normalize (centralisationMe/(double)stonesMe, (centralisationAdv/(double)stonesAdv));

	    // com
	    int icom = comIAxisMe / stonesMe;
	    int jcom = comJAxisMe / stonesMe;
	    final int comMe = icom*8+jcom;
	    icom = comIAxisAdv / stonesAdv;
	    jcom = comJAxisAdv / stonesAdv;
	    final int comAdv = icom*8+jcom;

	    // com centralisation
	    final double comCentralisation = normalize (centralisationTable[comMe], centralisationTable[comAdv]);

	    // mobility + com distances
	    double mobility = 0;
	    int wcom = comMe;
	    int bcom = comAdv;
	    if (black == 1) {
		wcom = comAdv;
		bcom = comMe;
	    }
	    int wdistances = 0;
	    int bdistances = 0;
	    double wmobility = 0;
	    double bmobility = 0;
	    for (int i=32; --i >= 0;) {
		Configuration c = linesConf[i][lines[i]];
		wmobility += c.mobilityWhite;
		bmobility += c.mobilityBlack;
		if (i < 8) {
		    wdistances += c.comDistancesWhite[wcom];
		    bdistances += c.comDistancesBlack[bcom];
		}
	    }
	    int distancesMe;
	    int distancesAdv;
	    if (black == 1) {
		// precomputed values assume we are white
		mobility = normalize (bmobility/(double)stonesMe, wmobility/(double)stonesAdv);
		distancesMe = bdistances;
		distancesAdv = wdistances;
	    } else {
		mobility = normalize (wmobility/(double)stonesMe, bmobility/(double)stonesAdv);
		distancesMe = wdistances;
		distancesAdv = bdistances;
	    }
	    // concentration
	    int minDistances;
	    if (stonesMe < 10) {
		minDistances = stonesMe-1;
	    } else {
		minDistances = (stonesMe<<1)-10;
	    }
	    double concentrationMe = 1.0/Math.max(1, distancesMe-minDistances);
	    if (stonesAdv < 10) {
		minDistances = stonesAdv-1;
	    } else {
		minDistances = (stonesAdv<<1)-10;
	    }
	    double concentrationAdv = 1.0/Math.max(1, distancesAdv-minDistances);
	    double concentration = normalize (concentrationMe, concentrationAdv);

	    // material
	    double material = normalize (stonesMe, stonesAdv);
	    return new Evaluation ((int)(1000*(weights[2]*centralisation +
					       weights[3]*comCentralisation +
					       weights[4]*concentration +
					       weights[5]*quads3 +
					       weights[6]*quads4 +
					       weights[7]*connectedness +
					       weights[8]*mobility +
					       weights[9]*material +
					       weights[10]*sidetomove)),
				   new double[]{0, 0, centralisation, comCentralisation, concentration, quads3, quads4, connectedness, mobility, material, sidetomove});
	}

	private int evaluateDenormalized () {
	    final int quads3 = q3Me - q3Adv;
	    final int quads4 = q4Me - q4Adv;
	    final double connectedness = 1.0/Math.max (1, euler4Me/4) - 1.0/Math.max (1, euler4Adv/4);
	    final int sidetomove = 1 - (firstBlack ^ black);

	    // centralisation
	    final int centralisation = centralisationMe - centralisationAdv;

	    // com
	    int icom = comIAxisMe / stonesMe;
	    int jcom = comJAxisMe / stonesMe;
	    final int comMe = icom*8+jcom;
	    icom = comIAxisAdv / stonesAdv;
	    jcom = comJAxisAdv / stonesAdv;
	    final int comAdv = icom*8+jcom;

	    // com centralisation
	    final int comCentralisation = centralisationTable[comMe] - centralisationTable[comAdv];

	    // mobility + com distances
	    double mobility = 0;
	    int wcom = comMe;
	    int bcom = comAdv;
	    if (black == 1) {
		wcom = comAdv;
		bcom = comMe;
	    }
	    int wdistances = 0;
	    int bdistances = 0;
	    for (int i=32; --i >= 0;) {
		Configuration c = linesConf[i][lines[i]];
		mobility += c.mobilityWhite - c.mobilityBlack;
		if (i < 8) {
		    wdistances += c.comDistancesWhite[wcom];
		    bdistances += c.comDistancesBlack[bcom];
		}
	    }
	    int distancesMe;
	    int distancesAdv;
	    if (black == 1) {
		// precomputed values assume we are white
		mobility = -mobility;
		distancesMe = bdistances;
		distancesAdv = wdistances;
	    } else {
		distancesMe = wdistances;
		distancesAdv = bdistances;
	    }
	    // concentration
	    int minDistances;
	    if (stonesMe < 10) {
		minDistances = stonesMe-1;
	    } else {
		minDistances = (stonesMe<<1)-10;
	    }
	    double concentration = 1.0/Math.max(1, distancesMe-minDistances);
	    if (stonesAdv < 10) {
		minDistances = stonesAdv-1;
	    } else {
		minDistances = (stonesAdv<<1)-10;
	    }
	    concentration -= 1.0/Math.max(1, distancesAdv-minDistances);

	    // material
	    int material = stonesMe - stonesAdv;
	    return (int)(weights[1]*centralisation +
			 weights[2]*comCentralisation +
			 weights[3]*concentration +
			 weights[4]*quads3 +
			 weights[5]*quads4 +
			 weights[6]*connectedness +
			 weights[7]*mobility +
			 weights[8]*material +
			 weights[9]*sidetomove);
	}

	private int evaluate () {
	    if (standpat != 0) {
		return standpat;
	    }
	    final double quads = normalize (q3Me+q4Me, q3Adv+q4Adv);
	    final double quads3 = normalize (q3Me, q3Adv);
	    final double quads4 = normalize (q4Me, q4Adv);
	    final double connectedness = normalize (1.0/Math.max (1, euler4Me/4), 1.0/Math.max (1, euler4Adv/4));
	    final int sidetomove = 1 - (firstBlack ^ black);

	    // centralisation
	    final double centralisation = normalize (centralisationMe/(double)stonesMe, (centralisationAdv/(double)stonesAdv));

	    // com
	    int icom = comIAxisMe / stonesMe;
	    int jcom = comJAxisMe / stonesMe;
	    final int comMe = icom*8+jcom;
	    icom = comIAxisAdv / stonesAdv;
	    jcom = comJAxisAdv / stonesAdv;
	    final int comAdv = icom*8+jcom;

	    // com centralisation
	    final double comCentralisation = normalize (centralisationTable[comMe], centralisationTable[comAdv]);

	    // mobility + com distances
	    int wcom = comMe;
	    int bcom = comAdv;
	    if (black == 1) {
		wcom = comAdv;
		bcom = comMe;
	    }
	    int wdistances = 0;
	    int bdistances = 0;
	    double wmobility = 0;
	    double bmobility = 0;
	    for (int i=32; --i >= 0;) {
		Configuration c = linesConf[i][lines[i]];
		wmobility += c.mobilityWhite;
		bmobility += c.mobilityBlack;
		if (i < 8) {
		    wdistances += c.comDistancesWhite[wcom];
		    bdistances += c.comDistancesBlack[bcom];
		}
	    }
	    final int distancesMe;
	    final int distancesAdv;
	    final double mobility;
	    if (black == 1) {
		// precomputed values assume we are white
		mobility = normalize (bmobility/(double)stonesMe, wmobility/(double)stonesAdv);
		distancesMe = bdistances;
		distancesAdv = wdistances;
	    } else {
		mobility = normalize (wmobility/(double)stonesMe, bmobility/(double)stonesAdv);
		distancesMe = wdistances;
		distancesAdv = bdistances;
	    }
	    // concentration
	    int minDistances;
	    if (stonesMe < 10) {
		minDistances = stonesMe-1;
	    } else {
		minDistances = (stonesMe<<1)-10;
	    }
	    double concentrationMe = 1.0/Math.max(1, distancesMe-minDistances);
	    if (stonesAdv < 10) {
		minDistances = stonesAdv-1;
	    } else {
		minDistances = (stonesAdv<<1)-10;
	    }
	    double concentrationAdv = 1.0/Math.max(1, distancesAdv-minDistances);
	    double concentration = normalize (concentrationMe, concentrationAdv);

	    // material
	    double material = normalize (stonesMe, stonesAdv);

	    final int phase = evaluationPhase;
	    standpat = (int)(1000*(weights[2+phase]*centralisation +
				   weights[3+phase]*comCentralisation +
				   weights[4+phase]*concentration +
				   weights[5+phase]*quads3 +
				   weights[6+phase]*quads4 +
				   weights[7+phase]*connectedness +
				   weights[8+phase]*mobility +
				   weights[9+phase]*material +
				   weights[10+phase]*sidetomove));
	    return standpat;
	}

	void computeLines () {
	    lines = createLines ();
	    for (int i=0; i < 32; i++) {
		lines[i] = 0;
	    }
	    for (int i=0; i < 8; i++) {
		for (int j=0; j < 8; j++) {
		    short color = 0;
		    int pos = i*8+j;
		    if ((me & (1L << pos)) != 0) {
			color = (short)(black+1);
		    } else if ((adv & (1L << pos)) != 0) {
			color = (short)(2-black);
		    }
		    lines[i] += color*(short)Math.pow(3,j);
		    lines[j+8] += color*(short)Math.pow(3,i);
		    lines[(7-i+j)%8+16] += color*(short)Math.pow(3,j);
		    lines[(i+j)%8+24] += color*(short)Math.pow(3,i);
		}
	    }
	}

	private void addChild (ChildrenList r, int move) {
	    // Relative History Heuristic
	    move = (move & 0xFFF) | (((move >> 16) + 100*historyHeuristic[move & 0xFFF]/(2+butterflyHeuristic[move & 0xFFF])) << 16);
	    int eval = move >> 16;

	    // insertion sort
	    int i=0;
	    for (; i < r.size; i++) {
		int val = (r.moves[i] >> 16);
		if (eval > val) {
		    // shift
		    for (int j=r.size; j > i; j--) {
			r.moves[j] = r.moves[j-1];
		    }
		    break;
		}
	    }
	    r.moves[i] = move;
	    r.size++;
	}

	ChildrenList children () {
	    ChildrenList r = createChildrenList (); // 12 stones * 8 directions
	    int size = 0;
	    for (int line=0; line < 32; line++) {
		int[] moves = linesConf[line][lines[line]].moves[black];
		for (int i=0; i < moves.length; i++) {
		    int move = moves[i];
		    addChild (r, move);
		}
	    }
	    return r;
	}

	ChildrenList tacticalChildren () {
	    ChildrenList r = createChildrenList (); // 12 stones * 8 directions
	    int size = 0;
	    for (int line=0; line<32; line++) {
		int[] moves = linesConf[line][lines[line]].tacticalMoves[black];
		for (int i=0; i < moves.length; i++) {
		    int move = moves[i];
		    addChild (r, move);
		}
	    }
	    return r;
	}

	void computeQuads () {
	    computeQuads (true, me);
	    computeQuads (false, adv);
	}

	void computeQuads (boolean tome, long me) {
	    int e = 0;
	    int q3 = 0;
	    int q4 = 0;
	    for (int i=0; i < 7; i++) {
		if (popcount ((me >> i) & 3) == 1) {
		    e++;
		}
		if (popcount ((me >> (7*8+i)) & 3) == 1) {
		    e++;
		}
		if (popcount ((me >> (i*8)) & 257) == 1) {
		    e++;
		}
		if (popcount ((me >> (i*8+7)) & 257) == 1) {
		    e++;
		}
		for (int j=0; j < 7; j++) {
		    long quad = (me >> (i*8+j)) & 0x303;
		    int count = popcount (quad);
		    if (count == 1) {
			e++;
		    } else if (count == 3) {
			e--;
			q3++;
		    } else if (count == 4) {
			q4++;
		    } else if (quad == 513 || quad == 258) {
			e -= 2;
		    }
		}
	    }
	    e += popcount (me & (-9151314442816847743L));
	    if (tome) {
		euler4Me = e;
		q3Me = q3;
		q4Me = q4;
	    } else {
		euler4Adv = e;
		q3Adv = q3;
		q4Adv = q4;
	    }
	}

	void applyQuadDiff (boolean tome, long board, int pos) {
	    int i = pos/8;
	    int j = pos%8;
	    if (j == 0) { board = (board & 0x7F7F7F7F7F7F7F7FL) << 1; ++pos; }
	    else if (j == 7) { board = (board & 0xFEFEFEFEFEFEFEFEL) >>> 1; --pos; }
	    if (i == 0) { board <<= 8; pos += 8; }
	    board >>>= pos-9;

	    int quads = (int)(board & 7);
	    quads |= (int)((board >> 5) & 56);
	    quads |= (int)((board >> 10) & 448);

	    if (tome) {
		euler4Me += euler4Table[quads];
		q3Me += q3Table[quads];
		q4Me += q4Table[quads];
	    } else {
		euler4Adv += euler4Table[quads];
		q3Adv += q3Table[quads];
		q4Adv += q4Table[quads];
	    }
	}

	void computeComMe () {
	    comIAxisMe = 0;
	    comJAxisMe = 0;
	    stonesMe = popcount (me);
	    long tmp = me;
	    while (tmp != 0) {
		// find our first occupied position
		long posbit = tmp & -tmp;
		// unset the position for the next cycle
		tmp &= ~posbit;
		int pos = (int)(Math.log (((double)(posbit-1))+1) / base2);
		int i = pos/8;
		int j = pos%8;
		comIAxisMe += i;
		comJAxisMe += j;
	    }
	}

	void computeComAdv () {
	    comIAxisAdv = 0;
	    comJAxisAdv = 0;
	    stonesAdv = popcount (adv);
	    long tmp = adv;
	    while (tmp != 0) {
		// find our first occupied position
		long posbit = tmp & -tmp;
		// unset the position for the next cycle
		tmp &= ~posbit;
		int pos = (int)(Math.log (((double)(posbit-1))+1) / base2);
		int i = pos/8;
		int j = pos%8;
		comIAxisAdv += i;
		comJAxisAdv += j;
	    }
	}

	void computeCentralisation () {
	    centralisationMe = 0;
	    centralisationAdv = 0;

	    long tmp = me;
	    while (tmp != 0) {
		// find our first occupied position
		long posbit = tmp & -tmp;
		// unset the position for the next cycle
		tmp &= ~posbit;
		int pos = (int)(Math.log (((double)(posbit-1))+1) / base2);
		centralisationMe += centralisationTable[pos];
	    }

	    tmp = adv;
	    while (tmp != 0) {
		// find first occupied position by the adversary
		long posbit = tmp & -tmp;
		// unset the position for the next cycle
		tmp &= ~posbit;
		int pos = (int)(Math.log (((double)(posbit-1))+1) / base2);
		centralisationAdv += centralisationTable[pos];
	    }
	}

	int qsearch (int depth, int alpha, int beta) {
	    qCount++;
	    // first check adversary, regolamento unical
	    if (euler4Adv/4 <= 1 && checkConnected (adv)) {
		return -Short.MAX_VALUE;
	    } else if (euler4Me/4 <= 1 && checkConnected (me)) {
		return Short.MAX_VALUE;
	    }
	    int g = evaluate ();
	    if (depth == 0) {
		return g;
	    }
	    if (g >= beta) {
		return g;
	    }
	    if (stopSearch ()) {
		return g;
	    }
	    alpha = Math.max (alpha, g);
	    final ChildrenList children = tacticalChildren ();
	    final int size = children.size;
	    int a = alpha;
	    int b = beta;
	    boolean firstChild = true;
	    for (int i=0; i < size; i++) {
		final Node child = applymove (children.moves[i]);

		int score = -child.qsearch (depth-1, -b, -a);
		if (stopSearch) {
		    child.unref ();
		    releaseChildrenList (children);
		    return g;
		}
		if (a < score && score < beta && !firstChild) {
		    score = -child.qsearch (depth-1, -beta, -a);
		    if (stopSearch) {
			child.unref ();
			releaseChildrenList (children);
			return g;
		    }
		}
		moveCount--;
		child.unref ();

		if (score > g) {
		    g = score;
		    a = Math.max (score, a);
		}
		if (beta <= a) {
		    break;
		}
		firstChild = false;
		b = a + 1;
	    }
	    releaseChildrenList (children);

	    return g;
	}

	private static final int PV_NODE = 0;
	private static final int CUT_NODE = 1;
	private static final int ALL_NODE = -1;

	int negascout (int depth, int alpha, int beta, int nodeType) {
	    nodeCount++;

	    if (depth == 0) {
		leafCount++;
		return qsearch ((int)weights[0], alpha, beta);
	    } else if ((euler4Adv/4 <= 1 && checkConnected (adv)) || moveCount/2 > 100) {
		return -Short.MAX_VALUE;
	    } else if (euler4Me/4 <= 1 && checkConnected (me)) {
		return Short.MAX_VALUE;
	    }

	    // transposition table lookup
	    if (this.depth >= depth) {
		// good hit
		if (lowerbound >= beta || lowerbound == upperbound) {
		    return lowerbound;
		}
		if (upperbound <= alpha) {
		    return upperbound;
		}
		alpha = Math.max (alpha, lowerbound);
		beta = Math.min (beta, upperbound);
	    }

	    // NULL MOVE)
	    if (NULLMOVE_HEURISTIC && nodeType != PV_NODE && depth >= 4) {
		// enabled for search depth >= 4
		Node child = nullmove ();
		int score = -child.negascout (depth-3, -beta, -alpha, -nodeType);
		child.unref ();
		if (stopSearch) {
		    return score;
		}
		if (score >= beta) {
		    return beta;
		}
	    }

	    final ChildrenList children = children ();
	    final int size = children.size;
	    if (size == 0) {
		releaseChildrenList (children);
		return -Short.MAX_VALUE;
	    }

	    if (MULTICUT_CUT && nodeType == CUT_NODE && depth >= 4) {
		// MULTI CUT - CUT NODES, M=10, C=3, R=2
		// enabled since search depth >= 5
		int c = 0, m = 0;
		for (int i=0; i < size && m < 10; i++, m++) {
		    final Node child = applymove (children.moves[i]);

		    int score = -child.negascout (depth-3, -beta, -alpha, -nodeType);
		    moveCount--;
		    child.unref ();
		    if (stopSearch) {
			releaseChildrenList (children);
			return score;
		    }
		    if (score >= beta && ++c >= 5) {
			releaseChildrenList (children);
			return beta;
		    }
		}
	    } else if (MULTICUT_ALL && nodeType == ALL_NODE && depth >= 4) {
		// MULTI CUT - ALL NODES, M=10, C=3, R=2
		// enabled since search depth >= 6
		int c = 0, m = 0;
		for (int i=0; i < size && m < 10; i++, m++) {
		    final Node child = applymove (children.moves[i]);

		    int score = -child.negascout (depth-3, -beta, -alpha, -nodeType);
		    moveCount--;
		    child.unref ();
		    if (stopSearch) {
			releaseChildrenList (children);
			return score;
		    }
		    if (score <= alpha && ++c >= 5) {
			releaseChildrenList (children);
			return alpha;
		    }
		}
	    }

	    int g = -Short.MAX_VALUE;
	    int a = alpha;
	    int b = beta;
	    int pv = this.pv;
	    if (pv == 0) {
		pv = children.moves[0];
	    }
	    final int oldpv = pv;
	    boolean firstChild = true;
	    for (int i=0; i < size; i++) {
		final int move = firstChild ? pv : children.moves[i];
		if (!firstChild && move == oldpv) {
		    continue;
		}
		final Node child = applymove (move);

		int score;
		if (LATE_MOVE_REDUCTIONS && depth >= 4 && i >= 5 && historyHeuristic[move & 0xFFF] == 0 && !firstChild && nodeType != PV_NODE) {
		    score = -child.negascout (depth-3, -b, -a, -nodeType);
		    if (stopSearch) {
			moveCount--;
			child.unref ();
			releaseChildrenList (children);
			return g;
		    }
		} else {
		    score = a+1; // hack to ensure the search below
		}
		if (score > a) {
		    score = -child.negascout (depth-1, -b, -a, firstChild ? -nodeType : (nodeType == CUT_NODE ? ALL_NODE : CUT_NODE));
		    if (stopSearch) {
			moveCount--;
			child.unref ();
			releaseChildrenList (children);
			return g;
		    }
		    if (!firstChild && ((a < score && score < beta) || (nodeType == PV_NODE && score == beta && beta == a+1))) {
			if (score == a+1) {
			    score = a;
			}
			score = -child.negascout (depth-1, -beta, -score, nodeType);
			if (stopSearch) {
			    moveCount--;
			    child.unref ();
			    releaseChildrenList (children);
			    return g;
			}
		    }
		}
		moveCount--;
		child.unref ();

		if (score > g) {
		    g = score;
		    pv = move;
		    a = Math.max (a, g);
		}
		if (beta <= a) {
		    historyHeuristic[move & 0xFFF]+=depth;
		    break;
		} else {
		    butterflyHeuristic[move & 0xFFF]+=depth;
		}
		if (firstChild) {
		    firstChild = false;
		    i = -1;
		}
		b = a + 1;
	    }
	    releaseChildrenList (children);

	    // transposition table storing
	    if (!this.cached) {
		// first time seen
		transposition.put (this, this);
		this.ref ();
		this.cached = true;
		this.depth = depth;
	    } else {
		// bypass the hashmap
		if (depth < this.depth) {
		    // worsen information, don't store
		    return g;
		} else {
		    this.depth = depth;
		}
	    }
	    if (g <= alpha) {
		upperbound = g;
		lowerbound = -Short.MAX_VALUE;
	    } else if (g > alpha && g < beta) {
		lowerbound = upperbound = g;
	    } else {
		lowerbound = g;
		upperbound = Short.MAX_VALUE;
	    }
	    this.pv = pv;

	    return g;
	}

	int iidnegascout () {
	    int d = 2;
	    int g = -Short.MAX_VALUE;
	    int oldMoveCount = moveCount;
	    while (USE_TIMER || d <= MAX_DEPTH) {
		int tmp = negascout (d, -Short.MAX_VALUE, Short.MAX_VALUE, PV_NODE);
		if (stopSearch) {
		    reachedDepth = d-1;
		    break;
		}
		g = tmp;
		d++;
	    }
	    moveCount = oldMoveCount;
	    return g;
	}

	/*
	int mtdf (int f, int depth) {
	    int g = f;
	    int upper = Short.MAX_VALUE;
	    int lower = -upper;
	    while (lower < upper) {
		int beta;
		if (g == lower) {
		    beta = g+1;
		} else {
		    beta = g;
		}
		g = alphabetaPV (depth, beta-1, beta);
		if (changedRoot || timesUp) {
		    return g;
		}
		if (g < beta) {
		    upper = g;
		} else {
		    lower = g;
		}
	    }
	    return g;
	}

	int iidmtdf (int g) {
	    int d = 2;

	    Node e = transposition.get (this);
	    if (e != null) {
		g = e.lowerbound - e.lowerbound/4;
		if (e.lowerbound == e.upperbound) {
		    d = e.depth+1;
		}
	    }
	    int oldpv = root.pv;
	    while (USE_TIMER || d <= MAX_DEPTH) {
		int tmp = mtdf (g, d);
		if (changedRoot || timesUp) {
		    reachedDepth = d-1;
		    root.pv = oldpv;
		    break;
		}
		g = tmp;
		oldpv = root.pv;
		d++;
	    }
	    return g;
	}
	*/

	void print () {
	    System.out.println ("---");
	    System.out.println ("Bitboard: me="+me+"L; adv="+adv+"L");
	    System.out.println ("Euler: me="+(euler4Me/4)+", adv="+(euler4Adv/4));
	    System.out.println ("Q3: me="+(q3Me)+", adv="+(q3Adv));
	    System.out.println ("Q4: me="+(q4Me)+", adv="+(q4Adv));
	    System.out.println ("COM: me=("+(comIAxisMe/stonesMe)+","+(comJAxisMe/stonesMe)+"), adv=("+(comIAxisAdv/stonesAdv)+","+(comJAxisAdv/stonesAdv)+")");
	    for (int i=0; i < 8; i++) {
		for (int j=0; j < 8; j++) {
		    System.out.print (" ");
		    long player = ((me >> (i*8+j)) & 1) | (((adv >> (i*8+j)) & 1) << 1);
		    System.out.print (" "+(player==0?'.':((((player&1)^black)==0?'X':'O'))));
		}
		System.out.println ();
	    }
	}

	// metodo per leggere la mossa utente in termini di coordinate sorgente e destinazione
	Node findmove (int move, boolean quiet) {
	    ChildrenList c = children ();
	    releaseChildrenList (c);
	    for (int k=c.size; --k>=0;) {
		//cerco il figlio corrispondente alla mossa desiderata
		if ((move & 0xFFF) == (c.moves[k] & 0xFFF)) {
		    Node child = applymove (move);
		    if (!quiet) {
			System.out.println (" giocatore MOVE ("+moveToString (move)+")");
			System.out.println ("NEW BOARD:");
			child.print ();
		    }
		    return child;
		}
	    }
	    return null;
	}

	Node nullmove () {
	    Node child = createNode (1-black, adv, me);
	    Node e = transposition.get (child);
	    if (e != null) {
		e.ref ();
		child.unref ();
		return e;
	    }
	    child.lines = createLines ();
	    System.arraycopy (lines, 0, child.lines, 0, 32);
	    child.centralisationMe = centralisationAdv;
	    child.centralisationAdv = centralisationMe;
	    child.stonesMe = stonesAdv;
	    child.stonesAdv = stonesMe;
	    child.comIAxisMe = comIAxisAdv;
	    child.comJAxisMe = comJAxisAdv;
	    child.comIAxisAdv = comIAxisMe;
	    child.comJAxisAdv = comJAxisMe;
	    child.euler4Me = euler4Adv;
	    child.euler4Adv = euler4Me;
	    child.q3Me = q3Adv;
	    child.q3Adv = q3Me;
	    child.q4Me = q4Adv;
	    child.q4Adv = q4Me;
	    return child;
	}

	Node applymove (int move) {
	    int from = move & 63;
	    int to = (move >> 6) & 63;
	    long movemask = (1L << from) | (1L << to);
	    Node child = createNode (1-black, adv & ~movemask, me ^ movemask);
	    moveCount++;
	    Node e = transposition.get (child);
	    if (e != null) {
		e.ref ();
		child.unref ();
		return e;
	    }

	    // ME
	    // source configuration
	    child.lines = createLines ();
	    System.arraycopy (lines, 0, child.lines, 0, 32);
	    int ifrom = from/8;
	    int jfrom = from%8;
	    int ifrompow = (black+1)*(int)Math.pow (3, ifrom);
	    int jfrompow = (black+1)*(int)Math.pow (3, jfrom);
	    child.lines[ifrom] -= jfrompow;
	    child.lines[jfrom+8] -= ifrompow;
	    child.lines[(7-ifrom+jfrom)%8+16] -= jfrompow;
	    child.lines[(ifrom+jfrom)%8+24] -= ifrompow;
	    int ito = to/8;
	    int jto = to%8;
	    int itopow = (int)Math.pow (3, ito);
	    int jtopow = (int)Math.pow (3, jto);
	    short torowconf = child.lines[ito];
	    short tocolconf = child.lines[jto+8];
	    short tomdconf = child.lines[(7-ito+jto)%8+16];
	    short tosdconf = child.lines[(ito+jto)%8+24];

	    // centralisation
	    child.centralisationAdv = centralisationMe - centralisationTable[from] + centralisationTable[to];
	    // quads
	    child.euler4Adv = euler4Me;
	    child.q3Adv = q3Me;
	    child.q4Adv = q4Me;
	    child.applyQuadDiff (false, me, from);
	    child.applyQuadDiff (false, me & ~(1L << from), to);
	    // com
	    child.comIAxisAdv = comIAxisMe - (from/8) + (to/8);
	    child.comJAxisAdv = comJAxisMe - (from%8) + (to%8);
	    child.stonesAdv = stonesMe;

	    // ADVERSARY
	    child.euler4Me = euler4Adv;
	    child.q3Me = q3Adv;
	    child.q4Me = q4Adv;
	    if (adv != child.me) { // capture
		// dest configuration
		torowconf -= (2-black)*jtopow;
		tocolconf -= (2-black)*itopow;
		tomdconf -= (2-black)*jtopow;
		tosdconf -= (2-black)*itopow;
		// centralisation
		child.centralisationMe = centralisationAdv - centralisationTable[to];
		// quads
		child.applyQuadDiff (true, adv, to);
		// com
		child.comIAxisMe = comIAxisAdv - (to/8);
		child.comJAxisMe = comJAxisAdv - (to%8);
		child.stonesMe = stonesAdv - 1;
	    } else {
		child.centralisationMe = centralisationAdv;
		child.comIAxisMe = comIAxisAdv;
		child.comJAxisMe = comJAxisAdv;
		child.stonesMe = stonesAdv;
	    }
	    // dest configuration
	    child.lines[ito] = (short)(torowconf + (black+1)*jtopow);
	    child.lines[jto+8] = (short)(tocolconf + (black+1)*itopow);
	    child.lines[(7-ito+jto)%8+16] = (short)(tomdconf + (black+1)*jtopow);
	    child.lines[(ito+jto)%8+24] = (short)(tosdconf + (black+1)*itopow);

	    return child;
	}

	@Override
	public boolean equals (Object o) {
	    Node n = (Node) o;
	    return (black == n.black && me == n.me && adv == n.adv);
	}

	@Override
	public int hashCode () {
	    return (int)((me | adv) ^ (~(me | adv) >>> 32));
	}
    }

    static class Evaluation {
	double value;
	double[] gradient;

	Evaluation (double value, double[] gradient) {
	    this.value = value;
	    this.gradient = gradient;
	}
    }

    public int search () {
	int result = root.iidnegascout ();
	int move = root.pv;
	Node tmp = root;
	root = root.applymove (move);
	tmp.unref ();
	if (moveCount >= weights[1] && evaluationPhase == 0) {
	    evaluationPhase = 9;
	    transposition.clear ();
	}
	return move;
    }

    public void apply (int move) {
	Node tmp = root;
	root = root.applymove (move);
	tmp.unref ();
    }

    public boolean blackConnected () {
	if (root.black == 1) {
	    return checkConnected (root.me);
	} else {
	    return checkConnected (root.adv);
	}
    }

    public boolean whiteConnected () {
	if (root.black == 0) {
	    return checkConnected (root.me);
	} else {
	    return checkConnected (root.adv);
	}
    }

    public void setWeights (double[] weights) {
	this.weights = weights;
	transposition.clear ();
    }

    double[] getWeights () {
	return weights;
    }

    public void print () {
	root.print ();
    }

    static Scanner sc = new Scanner (System.in);

    private static void loadOpenBook (int firstBlack) {
	try {
	    ObjectInputStream is = new ObjectInputStream (new FileInputStream ("aloaopeningbook.db"));
	    HashMap<BookEntry, Integer> book = (HashMap<BookEntry, Integer>) is.readObject ();
	    is.close ();
	    if (firstBlack == 0) {
		openingBook = new HashMap<BookEntry, Integer> ();
		for (Map.Entry<BookEntry, Integer> entry: book.entrySet ()) {
		    BookEntry be = entry.getKey ();
		    be.black = 1-be.black;
		    be.me = transpose (be.me);
		    be.adv = transpose (be.adv);
		    int bestMove = entry.getValue ();
		    int from = bestMove & 63;
		    int to = (bestMove >> 6) & 63;
		    int i = from/8;
		    int j = from%8;
		    from = j*8+i;
		    i = to/8;
		    j = to%8;
		    to = j*8+i;
		    openingBook.put (be, from | (to << 6));
		}
	    } else {
		openingBook = book;
	    }
	} catch (Exception e) {
	    openingBook = new HashMap<BookEntry, Integer> ();
	    e.printStackTrace ();
	}
    }

    public static void main (String[] args) {
	Runtime.getRuntime().traceInstructions (false);
	Runtime.getRuntime().traceMethodCalls (false);

	Properties prop = new Properties ();
	try {
	    FileInputStream is = new FileInputStream ("aloaconfig.txt");
	    prop.load (is);
	    is.close ();
	} catch (Exception e) {
	    e.printStackTrace ();
	    System.exit (1);
	}

	int playerBlack = Integer.parseInt (prop.getProperty ("playerBlack"));
	networkPlay = Boolean.valueOf (prop.getProperty ("network"));
	int firstBlack = networkPlay ? 0 : Integer.parseInt (prop.getProperty ("firstBlack"));

	if (Boolean.valueOf (prop.getProperty ("openbook"))) {
	    loadOpenBook (firstBlack);
	} else {
	    openingBook = new HashMap<BookEntry, Integer> ();
	}

	USE_TIMER = Boolean.valueOf (prop.getProperty ("timer"));
	NODE_RESOLUTION = Integer.parseInt (prop.getProperty ("noderesolution"));
	MAX_DEPTH = Integer.parseInt (prop.getProperty ("maxdepth"));
	MAX_ELAPSED = Integer.parseInt (prop.getProperty ("maxelapsed"));
	MAX_TRANSPOSITION_ENTRIES = Integer.parseInt (prop.getProperty ("maxtransposition"));
	MAX_MEMORY_NODES = Integer.parseInt (prop.getProperty ("maxnodes"));
	DEBUG = Boolean.valueOf (prop.getProperty ("debug"));
	PONDERING = Boolean.valueOf (prop.getProperty ("pondering"));

	if (Boolean.valueOf (prop.getProperty ("forwardPruning"))) {
	    MULTICUT_CUT = Boolean.valueOf (prop.getProperty ("mcc"));
	    MULTICUT_ALL = Boolean.valueOf (prop.getProperty ("mca"));
	    NULLMOVE_HEURISTIC = Boolean.valueOf (prop.getProperty ("nmh"));
	    LATE_MOVE_REDUCTIONS = Boolean.valueOf (prop.getProperty ("lmr"));
	}

	ALOA aloa = new ALOA (firstBlack, playerBlack, new double[]{3, 200,
								    0.19320688423639118, -0.22532208357000463, 1.978249618209707, 0.15843100589666084, 0.3262643765178706, 1.037848193346331, 1.5461326883590238, 4.746392718104192, 0.1238797320125999});

        String ip = prop.getProperty ("ip");
        int port = Integer.parseInt (prop.getProperty ("port"));
        if (networkPlay) {
            try {
		if (firstBlack == playerBlack) {
		    // we begin
		    ServerSocket s = new ServerSocket (port);
		    System.out.println ("OPENED LISTEN PORT "+port);
		    Socket c = s.accept ();
		    socketInput = new BufferedReader (new InputStreamReader (c.getInputStream (), "ISO-8859-1"));
		    socketOutput = new PrintWriter (new OutputStreamWriter (c.getOutputStream (), "ISO-8859-1"), true);
		} else {
		    System.out.println ("CONNECTING TO "+ip+":"+port+"...");
		    Socket c = new Socket (ip, port);
		    socketInput = new BufferedReader (new InputStreamReader (c.getInputStream (), "ISO-8859-1"));
		    socketOutput = new PrintWriter (new OutputStreamWriter (c.getOutputStream (), "ISO-8859-1"), true);
		    System.out.println ("CONNECTED");
		}
            } catch (Exception e) {
		e.printStackTrace ();
		System.exit (1);
	    }
        }

	aloa.run ();

	// NORMALIZED
	// first learning: male all'inizio, distrugge alla fine
	/*new ALOA (firstBlack, playerBlack, new double[]{4.0, 100,
	  0.19320688423639118, -0.22532208357000463, 1.978249618209707, 0.15843100589666084, 0.3262643765178706, 1.037848193346331, 1.5461326883590238, 4.746392718104192, 0.1238797320125999}).run ();*/
	// second learning: vince, ha più strategia, ma può perdere miseramente
	/*new ALOA (firstBlack, playerBlack, new double[]{4.0, 100, 0.3693391887231212, -0.3941276689005731, 1.6974056794521846, 0.1969573698282455, 0.42251754085484294, 1.1692690773245697, 1.6177411869969638, 4.415352034513357, 0.24513669607070626}).run ();*/
	// third learning: two phase, cambia dopo 20 mosse totali
	// SEMI-NORMALIZED
	/*new ALOA (firstBlack, playerBlack, new double[]{4.0, 100, 0.0, 0.0, 1.4201599903275968, -0.36512555768617777, 1.7712495235515109, 0.12582468911778763, 0.4107641235976282, 1.8234864779299158, 1.2525783089014777, 3.6387379404464464, 0.11641891244219119}).run ();*/
	// DENORMALIZED
	// new ALOA (1, 1, new double[]{5.0, 0.4, 0, 1.6, 0.8, 0, 0.2, 6.4, 0, 0.1}).run ();
    }

    public ALOA (int firstBlack, int playerBlack, double[] weights) {
	this.firstBlack = firstBlack;
	this.playerBlack = playerBlack;
	reset (firstBlack, weights);
    }

    public void reset (int firstBlack, double[] weights) {
        reset (firstBlack, weights, 36452665219186944L, 9079256848778920062L);
    }

    public void reset (int firstBlack, double[] weights, long w, long b) {
	setWeights (weights);
	moveCount = 0;
	evaluationPhase = 0;
	for (int i=0; i < 64*64; i++) {
	    historyHeuristic[i] = 0;
	    butterflyHeuristic[i] = 0;
	}

	if (root != null) {
	    root.unref ();
	}
	if (firstBlack == 0) {
	    root = createNode (firstBlack, w, b);
	} else {
	    root = createNode (firstBlack, b, w);
	}
	root.computeLines ();
        root.computeCentralisation ();
	root.computeQuads ();
	root.computeComMe ();
	root.computeComAdv ();
	transposition.clear ();
	System.gc ();
	System.out.println ("--> READY!");
    }

    public static int stringToMove (String s) {
	int fromcol = (int)(s.charAt (0) - 'A');
	int fromrow = (int)(7 - (s.charAt (1) - '1'));
	int tocol = (int)(s.charAt (3) - 'A');
	int torow = (int)(7 - (s.charAt (4) - '1'));
	return (fromrow*8+fromcol) | ((torow*8+tocol) << 6);
    }

    private static String moveToString (int move) {
	int from = move & 63;
	int to = (move >> 6) & 63;
	char fromcol = (char)((from%8) + 'A');
	char fromrow = (char)((7-from/8) + '1');
	char tocol = (char)((to%8) + 'A');
	char torow = (char)((7-to/8) + '1');
	return ""+fromcol+fromrow+"-"+tocol+torow;
    }

    class NetworkInput extends Thread {
	public void run () {
	    if (!networkPlay) {
		return;
	    }

	    while (true) {
		int move;
		String str;
		try {
		    str = socketInput.readLine ();
		    if (USE_TIMER) {
			// start timer immediately
			setDeadline ();
		    }
		    move = stringToMove (str);
		} catch (Exception e) {
		    e.printStackTrace ();
		    System.out.println ("DISCONNECTED");
		    break;
		}

		if (!networkPlay) {
		    break;
		}

		adversaryMove = move;
		changedRoot = true;
		try {
		    inputExchanger.exchange (null);
		} catch (Exception e) {
		    e.printStackTrace ();
		}
	    }
	}
    }

    class KeyboardInput extends Thread {
	public void run () {
	    while (true) {
		int move;
		try {
		    String str = sc.next ();
		    move = stringToMove (str);
		    // check legal move without using root.children() to avoid race conditions
		    boolean isLegal = false;
		    outer: for (int line=0; line < 32; line++) {
			int[] moves = linesConf[line][root.lines[line]].moves[root.black];
			for (int m: moves) {
			    if ((m & 0xFFF) == move) {
				isLegal = true;
				break outer;
			    }
			}
		    }
		    if (!isLegal) {
			System.out.println ("ILLEGAL MOVE "+str);
			continue;
		    }

		    networkPlay = false;
		    if (USE_TIMER) {
			setDeadline ();
		    }
		} catch (Exception e) {
		    e.printStackTrace ();
		    continue;
		}

		adversaryMove = move;
		changedRoot = true;
		try {
		    inputExchanger.exchange (null);
		} catch (Exception e) {
		    e.printStackTrace ();
		}
	    }
	}
    }

    static BufferedReader socketInput;
    static PrintWriter socketOutput;

    public void run () {
	new NetworkInput().start ();
	new KeyboardInput().start ();
	new SearchThread().start ();
    }
}
