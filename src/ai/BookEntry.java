package ai;

import java.io.*;

public class BookEntry implements Serializable {
    public int black;
    public long me;
    public long adv;

    public BookEntry copy () {
	BookEntry ret = new BookEntry ();
	ret.black = black;
	ret.me = me;
	ret.adv = adv;
	return ret;
    }

    @Override
    public boolean equals (Object o) {
	BookEntry n = (BookEntry) o;
	return (black == n.black && me == n.me && adv == n.adv);
    }

    @Override
    public int hashCode () {
	return (int)((me | adv) ^ (~(me | adv) >>> 32));
    }
}
