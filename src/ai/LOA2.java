/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ai;

import java.util.*;
import java.util.concurrent.Semaphore;
import java.io.*;

public class LOA2 implements Serializable {
    private double[] weights;
    private static int REACHED_DEPTH;
    // FIXME: a fine progetto va messo il confronto diretto come era prima
    //profondità che si vuole raggiungere
    private static final int deepth=5;
    //TEMPO MAX A DISPOSIZIONE
    private static final long MAX_ELAPSED = 5000;

    private volatile boolean changedRoot;
    private boolean timesUp = false;
    private Node root;
    // FIXME: non servirà nel progetto finale
    private final Semaphore inputMutex = new Semaphore (1, true);
    private int nodeCount = 0;

    // euler number*4, updated incrementally
    // white = 0, black = 1
    private final int[] euler4Number = new int[2];
    private final int[] q3Number = new int[2];
    private final int[] q4Number = new int[2];

    private final static int[] euler4Table = new int[512];
    private final static int[] q3Table = new int[512];
    private final static int[] q4Table = new int[512];

    private final int[] currentLines = new int[32];
    private final static Configuration[][] linesConf = new Configuration[32][6561];

    private static class Configuration {
	int[][] moves = new int[2][];
	double mobility; // white - black
    }

    private final static long[][] moveMasks = new long[8*8][4];
    private final static long[][] adjacentMasks = new long[8*8][4];
    private final static int[] steps = new int[] {
	8, // vertical
	1, // horizontal
	9, // main diagonal
	7, // secondary diagonal
    };

    private static final long borderMask = -35604928818740737L;

    //ASSEGNAMO I PESI ALLE POSIZIONI DELLA SCACCHIERA
    /* from LOA */
    private static final int[] centralisationLOAL = {
	0, 0, 0, 0, 0, 0, 0, 0,
	0, 1, 1, 1, 1, 1, 1, 0,
	0, 1, 5, 5, 5, 5, 1, 0,
	0, 1, 5, 10, 10, 5, 1, 0,
	0, 1, 5, 10, 10, 5, 1, 0,
	0, 1, 5, 5, 5, 5, 1, 0,
	0, 1, 1, 1, 1, 1, 1, 0,
	0, 0, 0, 0, 0, 0, 0, 0};

    /* from the pdf */
    private static final int centralisationTable[] = {
	-80, -25, -20, -20, -20, -20, -25, -80,
	-25, 10, 10, 10, 10, 10, 10, -25,
	-20, 10, 25, 25, 25, 25, 10, -20,
	-20, 10, 25, 50, 50, 25, 10, -20,
	-20, 10, 25, 50, 50, 25, 10, -20,
	-20, 10, 25, 25, 25, 25, 10, -20,
	-25, 10, 10, 10, 10, 10, 10, -25,
	-80, -25, -20, -20, -20, -20, -25, -80 };

    //non serve
    private static long hmirror (long b) {
	return ((b << 7) & 0x8080808080808080L) |
	    ((b << 5) & 0x4040404040404040L) |
	    ((b << 3) & 0x2020202020202020L) |
	    ((b << 1) & 0x1010101010101010L);
    }
    //non serve
    private static long vmirror (long b) {
	return ((b << (7*8)) & 0xFF00000000000000L) |
	    ((b << (5*8)) & 0xFF000000000000L) |
	    ((b << (3*8)) & 0xFF0000000000L) |
	    ((b << (1*8)) & 0xFF00000000L);
    }

    private static void printboard (long b) {
	System.out.println ("bitboard: "+b);
	for (int i=0; i < 8; i++) {
	    for (int j=0; j < 8; j++) {
		System.out.print (" "+((b >> (i*8+j))&1));
	    }
	    System.out.println ();
	}
    }
    //conta i bit di un long
    private static final long k1 = 0x5555555555555555L;
    private static final long k2 = 0x3333333333333333L;
    private static final long k4 = 0x0f0f0f0f0f0f0f0fL;
    private static final long h01 = 0x0101010101010101L;
    private static final int[] popcount8_table = new int[256];

    private static final double base2 = Math.log (2);

    private static int popcount2 (long x) {
	x -= ((x >>> 1) & k1);
	x = (x & k2) + ((x >>> 2) & k2);
	x = (x + (x >>> 4)) & k4;
	x += x >>> 8;
	x += x >>> 16;
	x += x >>> 32;
	return (int) x & 255;
    }

    private static int popcount (long x) {
	x -= (x >>> 1) & k1;
	x = (x & k2) + ((x >>> 2) & k2);
	x = (x + (x >>> 4)) & k4;
	return (int)((x * h01) >>> 56);
    }

    private static int popcount32 (int x) {
	x -= (x >>1) & 0x55555555;
	x = (x & 0x33333333) + ((x >>> 2) & 0x33333333);
	x = (x + (x >>> 4)) & 0x0f0f0f0f;
	x = x + (x >> 8);
	return (x + (x >> 16)) & 0x0000003F;
    }

    private static final int MAX_TRANSPOSITION_ENTRIES = 1000000;
    private final LinkedHashMap<Node, Node> transposition = new LinkedHashMap<Node, Node> (2097143) {
	@Override
	protected boolean removeEldestEntry (Map.Entry eldest) {
	    if (eldest.getValue () == root) {
		return false;
	    }
	    return size() > MAX_TRANSPOSITION_ENTRIES;
	}
    };

    //alternativa al popcount per tenere conto delle
    //pedine sulle varie direzioni
    private final byte[][] dircount = new byte[][] {
	{ 6, 2, 2, 2, 2, 2, 2, 6 }, // vertical
	{ 6, 2, 2, 2, 2, 2, 2, 6 }, // horizontal
	{ 0, 2, 2, 2, 2, 2, 2,
	  0, 2, 2, 2, 2, 2, 2, 0 }, // main diagonal
	{ 0, 2, 2, 2, 2, 2, 2,
	  0, 2, 2, 2, 2, 2, 2, 0 }, // secondary diagonal
    };

    static {
	int[] etable = new int[512];
	int[] q3table = new int[512];
	int[] q4table = new int[512];
        for (int i=0; i < 512; i++) {
	    int evalue = 0;
	    int q3value = 0;
	    int q4value = 0;
	    int quad = i & 27;
	    if (popcount (quad) == 1) {
		evalue++;
	    } else if (popcount (quad) == 3) {
		evalue--;
		q3value++;
	    } else if (quad == 17 || quad == 10) {
		evalue -= 2;
	    } else if (popcount (quad) == 4) {
		q4value++;
	    }

	    quad = i & 54;
	    if (popcount (quad) == 1) {
		evalue++;
	    } else if (popcount (quad) == 3) {
		evalue--;
		q3value++;
	    } else if (quad == 34 || quad == 20) {
		evalue -= 2;
	    } else if (popcount (quad) == 4) {
		q4value++;
	    }

	    quad = i & 216;
	    if (popcount (quad) == 1) {
		evalue++;
	    } else if (popcount (quad) == 3) {
		evalue--;
		q3value++;
	    } else if (quad == 136 || quad == 80) {
		evalue -= 2;
	    } else if (popcount (quad) == 4) {
		q4value++;
	    }

	    quad = i & 432;
	    if (popcount (quad) == 1) {
		evalue++;
	    } else if (popcount (quad) == 3) {
		evalue--;
		q3value++;
	    } else if (quad == 272 || quad == 160) {
		evalue -= 2;
	    } else if (popcount (quad) == 4) {
		q4value++;
	    }

	    etable[i] = evalue;
	    q3table[i] = q3value;
	    q4table[i] = q4value;
	}

	for (int i=0; i < 512; i++) {
	    euler4Table[i] = -etable[i] + etable[i ^ 16];
	    q3Table[i] = -q3table[i] + q3table[i ^ 16];
	    q4Table[i] = -q4table[i] + q4table[i ^ 16];
	}

	// popcount 8-bit table
	
        for (int i=0; i < 256; i++) {
	    popcount8_table[i] = popcount (i);
	}

	// move masks
	for (int i=0; i < 8; i++) {
	    for (int j=0; j < 8; j++) {
		long[] m = moveMasks[(i<<3)+j];
		long[] a = adjacentMasks[(i<<3)+j];
		// vertical
		for (int ii=0; ii < 8; ii++) m[0] |= 1L << ((ii<<3)+j);
		a[0] = 0;
		if (i > 0) a[0] |= 1L << ((i-1)*8+j);
		if (i < 8) a[0] |= 1L << ((i+1)*8+j);
		// horizontal
		for (int jj=0; jj < 8; jj++) m[1] |= 1L << ((i<<3)+jj);
		a[1] = 0;
		if (j > 0) a[1] |= 1L << ((i<<3)+j-1);
		if (j < 8) a[1] |= 1L << ((i<<3)+j+1);
		// main diagonal
		for (int ii=Math.max(0,i-j), jj=Math.max(0,j-i); ii < 8 && jj < 8; ii++, jj++) m[2] |= 1L << ((ii<<3)+jj);
		a[2] = 0;
		if (i > 0 && j > 0) a[2] |= 1L << ((i-1)*8+j-1);
		if (i < 8 && j < 8) a[2] |= 1L << ((i+1)*8+j+1);
		// secondary diagonal
		for (int ii=Math.max(0,i-7+j), jj=Math.min(7,j+i); ii < 8 && jj >= 0; ii++, jj--) m[3] |= 1L << ((ii<<3)+jj);
		a[3] = 0;
		if (i > 0 && j < 8) a[3] |= 1L << ((i-1)*8+j+1);
		if (i < 8 && j > 0) a[3] |= 1L << ((i+1)*8+j-1);
	    }
	}

	// configurations
	int n = 6561;
	for (int conf=0; conf < n; conf++) {
	    for (int i=0; i < 8; i++) {
		// rows
		Configuration c = new Configuration ();
		ArrayList<Integer> wmoves = new ArrayList<Integer> ();
		ArrayList<Integer> bmoves = new ArrayList<Integer> ();
		fillConf (conf, wmoves, bmoves, i, 0, 0, 1, 0, 7);
		c.moves[0] = toArray (wmoves);
		c.moves[1] = toArray (bmoves);
		linesConf[i][conf] = c;

		// columns
		c = new Configuration ();
		wmoves = new ArrayList<Integer> ();
		bmoves = new ArrayList<Integer> ();
		fillConf (conf, wmoves, bmoves, 0, i, 1, 0, 0, 7);
		c.moves[0] = toArray (wmoves);
		c.moves[1] = toArray (bmoves);
		linesConf[i+8][conf] = c;

		// main diagonal
		c = new Configuration ();
		wmoves = new ArrayList<Integer> ();
		bmoves = new ArrayList<Integer> ();
		fillConf (conf, wmoves, bmoves, 7-i, 0, 1, 1, 0, i);
		fillConf (conf, wmoves, bmoves, 0, i+1, 1, 1, i+1, 7);
		c.moves[0] = toArray (wmoves);
		c.moves[1] = toArray (bmoves);
		linesConf[i+16][conf] = c;

		// secondary diagonal
		c = new Configuration ();
		wmoves = new ArrayList<Integer> ();
		bmoves = new ArrayList<Integer> ();
		fillConf (conf, wmoves, bmoves, 0, i, 1, -1, 0, i);
		fillConf (conf, wmoves, bmoves, i+1, 7, 1, -1, i+1, 7);
		c.moves[0] = toArray (wmoves);
		c.moves[1] = toArray (bmoves);
		linesConf[i+24][conf] = c;
	    }
	}
    }

    static int[] toArray (ArrayList<Integer> list) {
	int[] r = new int[list.size()];
	for (int i=0; i < r.length; i++) {
	    r[i] = list.get (i);
	}
	return r;
    }

    static void fillConf (int conf, ArrayList<Integer> wmoves, ArrayList<Integer> bmoves, int rowstart, int colstart, int rowdir, int coldir, int from, int to) {
	// white=1, black=2
	fillMoves (conf, wmoves, 1, rowstart, colstart, rowdir, coldir, from, to);
	fillMoves (conf, bmoves, 2, rowstart, colstart, rowdir, coldir, from, to);
    }

    static void fillMove (int conf, ArrayList<Integer> moves, int color, int rowstart, int colstart, int rowdir, int coldir, int from, int to, int source, int dest, int dir) {
	int destcolor = (conf/(int)Math.pow (3, dest))%3;
	if (destcolor == color) {
	    return;
	}
	for (int j=source+dir; j != dest; j+=dir) {
	    int middle = (conf/(int)Math.pow (3, j))%3;
	    if (middle != 0 && middle != color) {
		// adversary in the middle
		return;
	    }
	}
	int sourcepos = (rowstart+(source-from)*rowdir)*8 + (colstart+(source-from)*coldir);
	int destpos = (rowstart+(dest-from)*rowdir)*8 + (colstart+(dest-from)*coldir);
	int move = sourcepos | (destpos << 6);
	if (destcolor != 0) { // capture
	    move |= 1 << 12;
	}
	moves.add (move);
    }

    static void fillMoves (int conf, ArrayList<Integer> moves, int color, int rowstart, int colstart, int rowdir, int coldir, int from, int to) {
	// count stones
	int stones = 0;
	for (int i=from; i <= to; i++) {
	    int value = (conf/(int)Math.pow (3, i))%3;
	    if (value > 0) {
		stones++;
	    }
	}
	for (int source=from; source <= to; source++) {
	    int value = (conf/(int)Math.pow (3, source))%3;
	    if (value == color) {
		// compute possible moves
		if (source-stones >= from) {
		    fillMove (conf, moves, color, rowstart, colstart, rowdir, coldir, from, to, source, source-stones, -1);
		}
		if (source+stones <= to) {
		    fillMove (conf, moves, color, rowstart, colstart, rowdir, coldir, from, to, source, source+stones, 1);
		}
	    }
	}
    }

    // thanks tony
    private static boolean checkConnected (long x) {
	long reached = x & -x; // find first
	while (true) {
	    long f = reached | (reached << 8) | (reached >>> 8);
	    f |= ((f >>> 1) & ~0x8080808080808080L) | ((f << 1) & ~0x101010101010101L);
	    f &= x;
	    if (f == reached) {
		return f == x;
	    }
	    reached = f;
	}
    }

    private static class ChildrenList {
	// at most 12 stones * 8 directions
	Node[] nodes = new Node[12*8];
	int size;
	int pv;
    }

    private class TimerThread extends Thread {
	public void run () {
	    try {
		sleep (MAX_ELAPSED);
	    } catch (Exception e) {
		e.printStackTrace ();
	    }
	    timesUp = true;
	}
    }

    private class SearchThread extends Thread {
	public void run () {
	    while (true) {
		Node root = LOA2.this.root;
		changedRoot = false;

		System.out.println ("SEARCHING MOVE (me="+root.me+"L; adv="+root.adv+"L):");
		System.out.println ("Evaluation: "+root.evaluate());
		System.out.println ("Euler: white="+(euler4Number[0]/4)+", black="+(euler4Number[1]/4));
		System.out.println ("Q3: white="+(q3Number[0])+", black="+(q3Number[1]));
		System.out.println ("Q4: white="+(q4Number[0])+", black="+(q4Number[1]));
		if (root.black == 1) {
		    System.out.println ("COM: white=("+(root.comIAxisAdv/root.stonesAdv)+","+(root.comJAxisAdv/root.stonesAdv)+"), black=("+(root.comIAxisMe/root.stonesMe)+","+(root.comJAxisMe/root.stonesMe)+")");
		} else {
		    System.out.println ("COM: white=("+(root.comIAxisMe/root.stonesMe)+","+(root.comJAxisMe/root.stonesMe)+"), black=("+(root.comIAxisAdv/root.stonesAdv)+","+(root.comJAxisAdv/root.stonesAdv)+")");
		}
		root.print ();

		long start = System.currentTimeMillis ();
		timesUp = false;
		TimerThread t = new TimerThread ();
		//t.start ();
		/*
		 * tipo di ricerca
		 * int result = root.iidmtdf ();
		 */
		nodeCount = 0;
		transposition.remove (root);
		int result = root.iidmtdf ();
		System.out.println ("MTD(f)+IID ("+result+"): "+(System.currentTimeMillis()-start));
		System.out.println ("Node generated "+nodeCount);
		Node tmp = root.nextmove (false);
		if (tmp == null) {
		    System.out.println ("WE LOST!");
		    break;
		} else {
		    root = tmp;
		}

		if (root.children().size == 0 || checkConnected (root.adv)) {
		    System.out.println ("WE WON!");
		    break;
		} else if (checkConnected (root.me)) {
		    System.out.println ("WE LOST!");
		    break;
		} else {
		    LOA2.this.root = root;
		    System.out.println ("si sj di dj: ");
		}

		/* SEARCH WHILE IDLE
		 * NOTA: nel progetto finale il limite di tempo non servirà, ci serve solo sapere capire quando riceviamo la mossa dell'avversario
		 * in questo caso simuliamo la risposta dell'avversario suppendo che impieghi proprio LOA.MAX_ELAPSED*/
		start = System.currentTimeMillis ();
		timesUp = false;
		t = new TimerThread ();
		//t.start ();
		//root.iidmtdf ();
		/* Caso in cui terminiamo il tempo ma l'utente non ha ancora inserito la mossa. NOTA: non servirà nel progetto finale */
		if (!changedRoot) {
		    // waiting for user input
		    try {
			inputMutex.acquire ();
			inputMutex.release ();
		    } catch (Exception e) {
			e.printStackTrace ();
		    }
		}
	    }
	}
    }

    static double computeMobility (long me, long adv) {
	long occupied = me | adv;
	double mobility = 0;
	long tmp = me;
	while (tmp != 0) {
	    // find our first occupied position
	    long posbit = tmp & -tmp;
	    // unset the position for the next cycle
	    tmp &= ~posbit;
	    int pos = (int)(Math.log (((double)(posbit-1))+1) / base2);
	    long[] masks = moveMasks[pos];
	    long[] adjacent = adjacentMasks[pos];
	    // destination from this position
	    for (int dir=0; dir < 4; dir++) {
		long a = adjacent[dir];
		if ((a & adv) == a) {
		    // blocked in this direction
		    continue;
		}
		long mask = masks[dir];
		long match = mask & occupied;
		int stones;
		if (dir == 1) {
		    // horizontal
		    stones = popcount8_table[(int)((match >> ((pos/8)*8)) & 0xFF)];
		} else {
		    stones = popcount (match);
		}

		// first direction, >>> is to avoid sign extension
		int step = steps[dir]*stones;
		long newpos = (posbit >>> step) & mask;
		if (newpos != 0 && (newpos & me) == 0) {
		    long middleadv = ((~((newpos<<1)-1)) & (posbit-1)) & (adv & mask);
		    if (middleadv == 0) {
			double m = 1;
			if ((newpos & adv) != 0) {
			    // capture
			    m *= 2;
			}
			if ((newpos & borderMask) != 0) {
			    // moving to the edge
			    m /= 2;
			}
			mobility += m;
		    }
		}
		// opposite direction
		newpos = (posbit << step) & mask;
		if (newpos != 0 && (newpos & me) == 0) {
		    long middleadv = ((~((posbit<<1)-1)) & (newpos-1)) & (adv & mask);
		    if (middleadv == 0) {
			double m = 1;
			if ((newpos & adv) != 0) {
			    // capture
			    m *= 2;
			}
			if ((newpos & borderMask) != 0) {
			    // moving to the edge
			    m /= 2;
			}
			mobility += m;
		    }
		}
	    }
	}
	return mobility;
    }

    private class Node implements Serializable {
	long me = 0L;
	long adv = 0L;
	int black;
	int lowerbound;
	int upperbound;
	int depth;
	int centralisation;
	int stonesMe;
	int stonesAdv;
	int comIAxisMe;
	int comJAxisMe;
	int comIAxisAdv;
	int comJAxisAdv;
	int comDistancesMe;
	int comDistancesAdv;
	int move;
	int pv;

	Node (int black, long me, long adv, int move) {
	    this.black = black;
	    this.me = me;
	    this.adv = adv;
	    this.move = move;
	}

	private int concentration () {
	    if (comDistancesMe == 0) {
		computeComDistancesMe ();
	    }
	    if (comDistancesAdv == 0) {
		computeComDistancesAdv ();
	    }

	    int concentration;
	    int minDistances;

	    if (stonesMe < 10) {
		minDistances = stonesMe-1;
	    } else {
		minDistances = (stonesMe<<1)-10;
	    }
	    concentration = 1000/(comDistancesMe + 1 - minDistances);
	    if (stonesAdv < 10) {
		minDistances = stonesAdv-1;
	    } else {
		minDistances = (stonesAdv<<1)-10;
	    }
	    concentration -= 1000/(comDistancesAdv + 1 - minDistances);
	    return concentration;
	}

	private int evaluate () {
	    int quads3 = q3Number[black] - q3Number[1-black];
	    int quads4 = q4Number[black] - q4Number[1-black];
	    double groups = 1.0/Math.max (1, euler4Number[black]/4) - 1.0/Math.max (1, euler4Number[1-black]/4);
	    double mobility = computeMobility (me, adv) - computeMobility (adv, me);
	    return (int)(weights[0]*centralisation +
			 weights[1]*concentration() +
			 weights[2]*quads3 +
			 weights[3]*quads4 +
			 weights[4]*groups + 
			 weights[5]*mobility);
	}

	ChildrenList children () {
	    ChildrenList r = new ChildrenList ();
	    long occupied = me | adv;
	    long maxvalue = -Short.MAX_VALUE;

	    long tmp = me;
	    while (tmp != 0) {
		// find our first occupied position
		long posbit = tmp & -tmp;
		// unset the position for the next cycle
		tmp &= ~posbit;
		int pos = (int)(Math.log (((double)(posbit-1))+1) / base2);
		/*int i = pos/8;
		  int j = pos%8;*/

		long[] masks = moveMasks[pos];
		long[] adjacent = adjacentMasks[pos];
		// destination from this position
		for (int dir=0; dir < 4; dir++) {
		    long a = adjacent[dir];
		    if ((a & adv) == a) {
			// blocked in this direction
			continue;
		    }
		    long mask = masks[dir];
		    long match = mask & occupied;
		    /*int dirpos = j;
		      if (dir == 1) dirpos = i;
		      else if (dir == 2) dirpos = 7-i+j;
		      else if (dir == 3) dirpos = i+j;
		      int stones = dircount[dir][dirpos];*/
		    int stones;
		    if (dir == 1) {
			// horizontal
			stones = popcount8_table[(int)((match >> ((pos/8)*8)) & 0xFF)];
		    } else {
			stones = popcount (match);
		    }

		    // first direction, >>> is to avoid sign extension
		    int step = steps[dir]*stones;
		    long newpos = (posbit >>> step) & mask;
		    if (newpos != 0 && (newpos & me) == 0) {
			long middleadv = ((~((newpos<<1)-1)) & (posbit-1)) & (adv & mask);
			if (middleadv == 0) {
			    int dest = (int)(Math.log (((double)(newpos-1))+1) / base2);
			    long movemask = posbit | newpos;
			    Node child = new Node (1-black, adv & ~movemask, me ^ movemask, pos | (dest << 6));
			    r.nodes[r.size] = child;

			    int cent = -centralisation + centralisationTable[pos] - centralisationTable[dest];
			    if (adv != child.me) {
				// capture
				cent -= centralisationTable[dest];
				child.move |= 1 << 12;
			    }

			    child.centralisation = cent;
			    // MAX CHILD FIRST
			    if (-cent > maxvalue) {
				r.pv = r.size;
				maxvalue = -cent;
			    }
			    r.size++;
			}
		    }
		    // opposite direction
		    newpos = (posbit << step) & mask;
		    if (newpos != 0 && (newpos & me) == 0) {
			long middleadv = ((~((posbit<<1)-1)) & (newpos-1)) & (adv & mask);
			if (middleadv == 0) {
			    int dest = (int)(Math.log (((double)(newpos-1))+1) / base2);
			    long movemask = posbit | newpos;
			    Node child = new Node (1-black, adv & ~movemask, me ^ movemask, pos | (dest << 6));
			    r.nodes[r.size] = child;

			    int cent = -centralisation + centralisationTable[pos] - centralisationTable[dest];
			    if (adv != child.me) {
				// capture
				cent -= centralisationTable[dest];
			    }

			    child.centralisation = cent;
			    // MAX CHILD FIRST
			    if (-cent > maxvalue) {
				r.pv = r.size;
				maxvalue = -cent;
			    }
			    r.size++;
			}
		    }
		}
	    }
	    return r;
	}

	void computeLines () {
	    for (int i=0; i < 8; i++) {
		for (int j=0; j < 8; j++) {
		    int color = 0;
		    int pos = i*8+j;
		    if ((me & (1L << pos)) != 0) {
			color = black+1;
		    } else if ((adv & (1L << pos)) != 0) {
			color = 2-black;
		    }
		    currentLines[i] += color*(int)Math.pow(3,j);
		    currentLines[j+8] += color*(int)Math.pow(3,i);
		    currentLines[(7-i+j)%8+16] += color*(int)Math.pow(3,j);
		    currentLines[(i+j)%8+24] += color*(int)Math.pow(3,i);
		}
	    }
	}

	void computeQuads () {
	    computeQuads (black, me);
	    computeQuads (1-black, adv);
	}

	void computeQuads (int black, long me) {
	    int e = 0;
	    int q3 = 0;
	    int q4 = 0;
	    for (int i=0; i < 7; i++) {
		if (popcount ((me >> i) & 3) == 1) {
		    e++;
		}
		if (popcount ((me >> (7*8+i)) & 3) == 1) {
		    e++;
		}
		if (popcount ((me >> (i*8)) & 257) == 1) {
		    e++;
		}
		if (popcount ((me >> (i*8+7)) & 257) == 1) {
		    e++;
		}
		for (int j=0; j < 7; j++) {
		    long quad = (me >> (i*8+j)) & 0x303;
		    int count = popcount (quad);
		    if (count == 1) {
			e++;
		    } else if (count == 3) {
			e--;
			q3++;
		    } else if (count == 4) {
			q4++;
		    } else if (quad == 513 || quad == 258) {
			e -= 2;
		    }
		}
	    }
	    e += popcount (me & (-9151314442816847743L));
	    euler4Number[black] = e;
	    q3Number[black] = q3;
	    q4Number[black] = q4;
	}

	void computeComMe () {
	    comIAxisMe = 0;
	    comJAxisMe = 0;
	    stonesMe = popcount (me);
	    long tmp = me;
	    while (tmp != 0) {
		// find our first occupied position
		long posbit = tmp & -tmp;
		// unset the position for the next cycle
		tmp &= ~posbit;
		int pos = (int)(Math.log (((double)(posbit-1))+1) / base2);
		int i = pos/8;
		int j = pos%8;
		comIAxisMe += i;
		comJAxisMe += j;
	    }
	}

	void computeComAdv () {
	    comIAxisAdv = 0;
	    comJAxisAdv = 0;
	    stonesAdv = popcount (adv);
	    long tmp = adv;
	    while (tmp != 0) {
		// find our first occupied position
		long posbit = tmp & -tmp;
		// unset the position for the next cycle
		tmp &= ~posbit;
		int pos = (int)(Math.log (((double)(posbit-1))+1) / base2);
		int i = pos/8;
		int j = pos%8;
		comIAxisAdv += i;
		comJAxisAdv += j;
	    }
	}

	void computeComDistancesMe () {
	    int distances = 0;
	    int icom = comIAxisMe / stonesMe;
	    int jcom = comJAxisMe / stonesMe;
	    long tmp = me;
	    while (tmp != 0) {
		// find our first occupied position
		long posbit = tmp & -tmp;
		// unset the position for the next cycle
		tmp &= ~posbit;
		int pos = (int)(Math.log (((double)(posbit-1))+1) / base2);
		int i = pos/8;
		int j = pos%8;
		int difrow = Math.abs (icom - i);
		int difcol = Math.abs (jcom - j);
		distances += Math.max (difrow, difcol);
	    }
	    comDistancesMe = distances;
	}

	void computeComDistancesAdv () {
	    int distances = 0;
	    int icom = comIAxisAdv / stonesAdv;
	    int jcom = comJAxisAdv / stonesAdv;
	    long tmp = adv;
	    while (tmp != 0) {
		// find our first occupied position
		long posbit = tmp & -tmp;
		// unset the position for the next cycle
		tmp &= ~posbit;
		int pos = (int)(Math.log (((double)(posbit-1))+1) / base2);
		int i = pos/8;
		int j = pos%8;
		int difrow = Math.abs (icom - i);
		int difcol = Math.abs (jcom - j);
		distances += Math.max (difrow, difcol);
	    }
	    comDistancesAdv = distances;
	}

	void computeCentralisation () {
	    centralisation = 0;

	    long tmp = me;
	    while (tmp != 0) {
		// find our first occupied position
		long posbit = tmp & -tmp;
		// unset the position for the next cycle
		tmp &= ~posbit;
		int pos = (int)(Math.log (((double)(posbit-1))+1) / base2);
		centralisation += centralisationTable[pos];
	    }

	    tmp = adv;
	    while (tmp != 0) {
		// find first occupied position by the adversary
		long posbit = tmp & -tmp;
		// unset the position for the next cycle
		tmp &= ~posbit;
		int pos = (int)(Math.log (((double)(posbit-1))+1) / base2);
		centralisation -= centralisationTable[pos];
	    }
	}

	int alphabetaPV (int depth, int alpha, int beta) {
	    if (euler4Number[black]/4 <= 1 && checkConnected (me)) {
		return Short.MAX_VALUE;
	    } else if (euler4Number[1-black]/4 <= 1 && checkConnected (adv)) {
		return -Short.MAX_VALUE;
	    } else if (depth == 0) {
		return evaluate ();
	    }

	    // principal variation
	    int pv = -1;

	    // transposition table lookup
	    Node e = transposition.get (this);
	    if (e != null) {
		if (e.depth >= depth) {
		    // good hit
		    if (e.lowerbound >= beta || e.lowerbound == e.upperbound) {
			return e.lowerbound;
		    }
		    if (e.upperbound <= alpha) {
			return e.upperbound;
		    }
		    alpha = Math.max (alpha, e.lowerbound);
		    beta = Math.min (beta, e.upperbound);
		}
		pv = e.pv;
	    }

	    boolean firstChild = true;
	    int g = -Short.MAX_VALUE;
	    int a = alpha;
	    int oldpv = pv;
	    int lineIndex = 0;
	    int moveIndex = 0;
	    Configuration c = linesConf[lineIndex][currentLines[lineIndex]];
	    while (true) {
		int move;
		if (firstChild && pv != -1) {
		    move = pv;
		} else {
		    firstChild = false;
		    if (moveIndex >= c.moves[black].length) {
			lineIndex++;
			if (lineIndex >= 32) {
			    break;
			}
			moveIndex = 0;
			c = linesConf[lineIndex][currentLines[lineIndex]];
			continue;
		    }
		    move = c.moves[black][moveIndex];
		    if (move == oldpv) {
			// skip PV
			moveIndex++;
			continue;
		    }
		}

		int from = move & 63;
		int to = (move >> 6) & 63;

		int euler4me = euler4Number[black];
		int euler4adv = euler4Number[1-black];
		int q3me = q3Number[black];
		int q3adv = q3Number[1-black];
		int q4me = q4Number[black];
		int q4adv = q4Number[1-black];

		int fromrowconf = currentLines[from/8];
		int fromcolconf = currentLines[from%8+8];
		int frommdconf = currentLines[(7-from/8+from%8)%8+16];
		int fromsdconf = currentLines[(from/8+from%8)%8+24];
		int torowconf = currentLines[to/8];
		int tocolconf = currentLines[to%8+8];
		int tomdconf = currentLines[(7-to/8+to%8)%8+16];
		int tosdconf = currentLines[(to/8+to%8)%8+24];

		Node child = applymove (move);

		/*
		  long movemask = n.me ^ child.adv;
		  int from = (int)(Math.log (((double)((n.me & movemask)-1))+1) / Node.base2);
		  int to = (int)(Math.log (((double)((child.adv & movemask)-1))+1) / Node.base2);
		  int fromi = from/8;
		  int fromj = from%8;
		  int toi = to/8;
		  int toj = to%8;

		  // apply dircount

		  --Node.dircount[0][fromj];
		  --Node.dircount[1][fromi];
		  --Node.dircount[2][7-fromi+fromj];
		  --Node.dircount[3][fromi+fromj];
		  if ((n.adv & movemask) == 0) {
		  ++Node.dircount[0][toj];
		  ++Node.dircount[1][toi];
		  ++Node.dircount[2][7-toi+toj];
		  ++Node.dircount[3][toi+toj];
		  }*/
		nodeCount++;
		int score = -child.alphabetaPV (depth-1, -beta, -a);

		// undo quads
		euler4Number[black] = euler4me;
		euler4Number[1-black] = euler4adv;
		q3Number[black] = q3me;
		q3Number[1-black] = q3adv;
		q4Number[black] = q4me;
		q4Number[1-black] = q4adv;

		// undo configuration
		currentLines[from/8] = fromrowconf;
		currentLines[from%8+8] = fromcolconf;
		currentLines[(7-from/8+from%8)%8+16] = frommdconf;
		currentLines[(from/8+from%8)%8+24] = fromsdconf;
		currentLines[to/8] = torowconf;
		currentLines[to%8+8] = tocolconf;
		currentLines[(7-to/8+to%8)%8+16] = tomdconf;
		currentLines[(to/8+to%8)%8+24] = tosdconf;

		// undo dircount
		/*
		  Node.dircount[0][fromj]++;
		  Node.dircount[1][fromi]++;
		  Node.dircount[2][7-fromi+fromj]++;
		  Node.dircount[3][fromi+fromj]++;
		  if ((n.adv & movemask) == 0) {
		  Node.dircount[0][toj]--;
		  Node.dircount[1][toi]--;
		  Node.dircount[2][7-toi+toj]--;
		  Node.dircount[3][toi+toj]--;
		  }
		*/
		if (changedRoot || timesUp) {
		    return g;
		}
		if (score > g) {
		    g = score;
		    pv = move;
		    a = Math.max (a, g);
		}
		if (beta <= a) {
		    break;
		}
		if (firstChild) {
		    firstChild = false;
		} else {
		    moveIndex++;
		}
	    }

	    // transposition table storing
	    if (e == null) {
		e = this;
		// first time seen
		transposition.put (e, e);
		e.depth = depth;
	    } else {
		// bypass the hashmap
		if (depth < e.depth) {
		    // worsen information, don't store
		    return g;
		} else {
		    e.depth = depth;
		}
	    }
	    if (g <= alpha) {
		e.upperbound = g;
		e.lowerbound = -Short.MAX_VALUE;
	    } else if (g > alpha && g < beta) {
		e.lowerbound = e.upperbound = g;
	    } else {
		e.lowerbound = g;
		e.upperbound = Short.MAX_VALUE;
	    }
	    e.pv = pv;

	    return g;
	}
	/*
	int negascout (int depth, int alpha, int beta) {
	    if ((euler4Number[black] & 0xFF)/4 <= 1 && checkConnected (me)) {
		return Short.MAX_VALUE;
	    } else if ((euler4Number[1-black] & 0xFF)/4 <= 1 && checkConnected (adv)) {
		return -Short.MAX_VALUE;
	    } else if (depth == 0) {
		return evaluate ();
	    }

	    // principal variation
	    int pv = -1;

	    // transposition table lookup
	    Node e = transposition.get (this);
	    if (e != null) {
		if (e.depth >= depth) {
		    // good hit
		    if (e.lowerbound >= beta || e.lowerbound == e.upperbound) {
			return e.lowerbound;
		    }
		    if (e.upperbound <= alpha) {
			return e.upperbound;
		    }
		    alpha = Math.max (alpha, e.lowerbound);
		    beta = Math.min (beta, e.upperbound);
		}
		pv = e.pv;
	    }

	    ChildrenList children = children ();
	    boolean firstChild = true;
	    int g = -Short.MAX_VALUE;
	    int a = alpha;
	    int b = beta;
	    if (pv == -1) {
		pv = children.pv;
	    }
	    int oldpv = pv;
	    int size = children.size;
	    for (int i=pv; i < size; i++) {
		if (i == oldpv && !firstChild) {
		    // skip PV
		    continue;
		}

		int euler4me = euler4Number[black];
		int euler4adv = euler4Number[1-black];

		Node child = applymove (children, i);

		nodeCount++;
		int score = -child.negascout (depth-1, -b, -a);
		if (score > a && score < beta && !firstChild) {
		    score = -child.negascout (depth-1, -beta, -a);
		}

		// undo quads
		euler4Number[black] = euler4me;
		euler4Number[1-black] = euler4adv;

		if (changedRoot || timesUp) {
		    return g;
		}
		if (score > g) {
		    g = score;
		    pv = i;
		    a = Math.max (a, g);
		}
		if (beta <= a) {
		    break;
		}
		b = a + 1;
		if (firstChild) {
		    i=-1;
		    firstChild = false;
		}
	    }

	    // transposition table storing
	    if (e == null) {
		e = this;
		// first time seen
		transposition.put (e, e);
		e.depth = depth;
	    } else {
		// bypass the hashmap
		if (depth < e.depth) {
		    // worsen information, don't store
		    return g;
		} else {
		    e.depth = depth;
		}
	    }
	    if (g <= alpha) {
		e.upperbound = g;
		e.lowerbound = -Short.MAX_VALUE;
	    } else if (g > alpha && g < beta) {
		e.lowerbound = e.upperbound = g;
	    } else {
		e.lowerbound = g;
		e.upperbound = Short.MAX_VALUE;
	    }
	    e.pv = pv;

	    return g;
	}
	*/
	int mtdf (int f, int depth) {
	    int g = f;
	    int upper = Short.MAX_VALUE;
	    int lower = -upper;
	    while (lower < upper) {
		int beta;
		if (g == lower) {
		    beta = g+1;
		} else {
		    beta = g;
		}
		g = alphabetaPV (depth, beta-1, beta);
		if (changedRoot || timesUp) {
		    return g;
		}
		if (g < beta) {
		    upper = g;
		} else {
		    lower = g;
		}
	    }
	    return g;
	}

	int iidmtdf () {
	    int g = -Short.MAX_VALUE;
	    int d = 2;

	    Node e = transposition.get (this);
	    if (e != null) {
		if (e.lowerbound == e.upperbound) {
		    g = e.lowerbound;
		    d = e.depth+1;
		} else {
		    d = e.depth;
		}
	    }
	    while (d <= deepth) {
		int tmp = mtdf (g, d);
		if (changedRoot || timesUp) {
		    REACHED_DEPTH = d-1;
		    System.out.println ("DEPTH REACHED: "+(d-1));
		    return g;
		}
		g = tmp;
		d++;
	    }
	    return g;
	}
	/*
	int iidnegascout () {
	    int g = -Short.MAX_VALUE;
	    int d = 2;

	    Node e = transposition.get (this);
	    if (e != null) {
		if (e.lowerbound == e.upperbound) {
		    g = e.lowerbound;
		    d = e.depth+1;
		} else {
		    d = e.depth;
		}
	    }
	    while (d <= deepth) {
		int tmp = negascout (d, -Short.MAX_VALUE, Short.MAX_VALUE);
		if (changedRoot || timesUp) {
		    REACHED_DEPTH = d-1;
		    System.out.println ("DEPTH REACHED: "+(d-1));
		    return g;
		}
		g = tmp;
		d++;
	    }
	    return g;
	}
	*/
	int iidalphabeta () {
	    int g = -Short.MAX_VALUE;
	    int d = 2;

	    Node e = transposition.get (this);
	    if (e != null) {
		if (e.lowerbound == e.upperbound) {
		    g = e.lowerbound;
		    d = e.depth+1;
		} else {
		    d = e.depth;
		}
	    }
	    while (d <= deepth) {
		int tmp = alphabetaPV (d, -Short.MAX_VALUE, Short.MAX_VALUE);
		if (changedRoot || timesUp) {
		    REACHED_DEPTH = d-1;
		    System.out.println ("DEPTH REACHED: "+(d-1));
		    return g;
		}
		g = tmp;
		d++;
	    }
	    return g;
	}

	void print () {
	    System.out.println ("---");
	    for (int i=0; i < 8; i++) {
		for (int j=0; j < 8; j++) {
		    System.out.print (" ");
		    long player = ((me >> (i*8+j)) & 1) | (((adv >> (i*8+j)) & 1) << 1);
		    System.out.print (" "+(player==0?'.':((((player&1)^black)==0?'X':'O'))));
		}
		System.out.println ();
	    }
	}

	Node nextmove (boolean quiet) {
	    Node e = transposition.get (this);
	    if (e == null) {
		return null;
	    }
	    ChildrenList children = children ();
	    if (children.size == 0) {
		return null;
	    }
	    Node child = e.applymove (e.pv);
	    if (!quiet) {
		int from = child.move & 63;
		int to = (child.move >> 6) & 63;
		int fromi = from/8;
		int fromj = from%8;
		int toi = to/8;
		int toj = to%8;
		System.out.println ("MOVE ("+fromi+","+fromj+"->"+toi+","+toj+", pv="+e.pv+"):");
		System.out.println ("NEW BOARD:");
		child.print ();
	    }
	    return child;
	}

	// metodo per leggere la mossa utente in termini di coordinate sorgente e destinazione
	Node findmove (int i, int j, int iD, int jD, boolean quiet) {
	    /*
	    ChildrenList c = children ();
	    for (int k=c.size; --k>=0;) {
		int from = c.nodes[k].move & 63;
		int to = (c.nodes[k].move >> 6) & 63;
		int fromi = from/8;
		int fromj = from%8;
		int toi = to/8;
		int toj = to%8;
		//cerco il figlio corrispondente alla mossa desiderata
		if (fromi == i && fromj == j && toi == iD && toj == jD){
		    Node child = applymove (c, k);
		    if (!quiet) {
			System.out.println (" giocatore MOVE ("+fromi+","+fromj+"->"+toi+","+toj+"):");
			System.out.println ("NEW BOARD:");
			child.print ();
		    }
		    return child;
		}
		}*/
	    return null;
	}

	Node applymove (int move) {
	    int from = move & 63;
	    int to = (move >> 6) & 63;
	    int capture = move >> 12;

	    long movemask = (1L << from) | (1L << to);
	    Node child = new Node (1-black, adv & ~movemask, me ^ movemask, move);

	    // configuration
	    currentLines[from/8] -= (black+1)*(int)Math.pow(3,from%8);
	    currentLines[from%8+8] -= (black+1)*(int)Math.pow(3,from/8);
	    currentLines[(7-from/8+from%8)%8+16] -= (black+1)*(int)Math.pow(3,from%8);
	    currentLines[(from/8+from%8)%8+24] -= (black+1)*(int)Math.pow(3,from/8);
	    int torowconf = currentLines[to/8];
	    int tocolconf = currentLines[to%8+8];
	    int tomdconf = currentLines[(7-to/8+to%8)%8+16];
	    int tosdconf = currentLines[(to/8+to%8)%8+24];

	    // centralisation
	    int cent = -centralisation + centralisationTable[from] - centralisationTable[to];

	    // quads
	    applyQuadDiff (black, me, from);
	    applyQuadDiff (black, me & ~(1L << from), to);

	    // com
	    child.comIAxisAdv = comIAxisMe - (from/8) + (to/8);
	    child.comJAxisAdv = comJAxisMe - (from%8) + (to%8);
	    child.stonesAdv = stonesMe;
	    if (comDistancesMe != 0) {
		int icom = comIAxisMe / stonesMe;
		int jcom = comJAxisMe / stonesMe;
		if (icom == child.comIAxisAdv/stonesMe && jcom == child.comJAxisAdv/stonesMe) {
		    // concentration
		    int olddifrow = Math.abs (icom - from/8);
		    int olddifcol = Math.abs (jcom - from%8);
		    int newdifrow = Math.abs (icom - to/8);
		    int newdifcol = Math.abs (jcom - to%8);
		    child.comDistancesAdv = comDistancesMe - Math.max (olddifrow, olddifcol) + Math.max (newdifrow, newdifcol);
		}
	    }

	    if (capture == 1) { // capture
		// configuration
		torowconf -= (2-black)*(int)Math.pow(3,to%8);
		tocolconf -= (2-black)*(int)Math.pow(3,to/8);
		tomdconf -= (2-black)*(int)Math.pow(3,to%8);
		tosdconf -= (2-black)*(int)Math.pow(3,to/8);

		// centralisation
		cent -= centralisationTable[to];

		// quads
		applyQuadDiff (1-black, adv, to);
		// com
		child.comIAxisMe = comIAxisAdv - (to/8);
		child.comJAxisMe = comJAxisAdv - (to%8);
		child.stonesMe = stonesAdv - 1;

		// concentration
		if (comDistancesAdv != 0) {
		    int icom = comIAxisAdv / stonesAdv;
		    int jcom = comJAxisAdv / stonesAdv;
		    if (icom == child.comIAxisMe/child.stonesMe && jcom == child.comJAxisMe/child.stonesMe) {
			int difrow = Math.abs (icom - to/8);
			int difcol = Math.abs (jcom - to%8);
			child.comDistancesMe = comDistancesAdv - Math.max (difrow, difcol);
		    }
		}
	    } else {
		child.comIAxisMe = comIAxisAdv;
		child.comJAxisMe = comJAxisAdv;
		child.stonesMe = stonesAdv;
		child.comDistancesMe = comDistancesAdv;
	    }
	    child.centralisation = cent;

	    currentLines[to/8] = torowconf + (black+1)*(int)Math.pow(3,to%8);
	    currentLines[to%8+8] = tocolconf + (black+1)*(int)Math.pow(3,to/8);
	    currentLines[(7-to/8+to%8)%8+16] = tomdconf + (black+1)*(int)Math.pow(3,to%8);
	    currentLines[(to/8+to%8)%8+24] = tosdconf + (black+1)*(int)Math.pow(3,to/8);

	    return child;
	}

	@Override
	public boolean equals (Object o) {
	    Node n = (Node) o;
	    return (black == n.black && me == n.me && adv == n.adv);
	}

	@Override
	public int hashCode () {
	    return (int)((me | adv) ^ (~(me | adv) >>> 32));
	}
    }

    void applyQuadDiff (int black, long board, int pos) {
	int i = pos/8;
	int j = pos%8;
	if (j == 0) { board = (board & 0x7F7F7F7F7F7F7F7FL) << 1; ++pos; }
	else if (j == 7) { board = (board & 0xFEFEFEFEFEFEFEFEL) >>> 1; --pos; }
	if (i == 0) { board <<= 8; pos += 8; }
	board >>>= pos-9;

	int quads = (int)(board & 7);
	quads |= (int)((board >> 5) & 56);
	quads |= (int)((board >> 10) & 448);

	euler4Number[black] += euler4Table[quads];
	q3Number[black] += q3Table[quads];
	q4Number[black] += q4Table[quads];
    }

    public static void main (String[] args) {
	new LOA2 (1, new double[]{1, 3, 30, 40, 170, 30}).run ();
    }

    static class Evaluation {
	int value;
	double[] gradient;

	Evaluation (int value, double[] gradient) {
	    this.value = value;
	    this.gradient = gradient;
	}
    }

    public int search () {
	int result = root.iidmtdf ();
	root = root.nextmove (true);
	return root.move;
    }

    public Evaluation evaluation () {
	int concentration = root.concentration ();
	int quads3 = q3Number[root.black] - q3Number[1-root.black];
	int quads4 = q4Number[root.black] - q4Number[1-root.black];
	double groups = 1.0/Math.max (1, euler4Number[root.black]/4) - 1.0/Math.max (1, euler4Number[1-root.black]/4);
	double mobility = computeMobility (root.me, root.adv) - computeMobility (root.adv, root.me);
	return new Evaluation ((int)(weights[0]*root.centralisation+
				     weights[1]*concentration+
				     weights[2]*quads3+
				     weights[3]*quads4+
				     weights[4]*groups+
				     weights[5]*mobility),
			       new double[]{root.centralisation, concentration, quads3, quads4, groups, mobility});
    }

    public void apply (int move) {
	int from = move & 63;
	int to = (move >> 6) & 63;
	int fromi = from/8;
	int fromj = from%8;
	int toi = to/8;
	int toj = to%8;
	root = root.findmove (fromi, fromj, toi, toj, true);
    }

    public boolean blackConnected () {
	if (root.black == 1) {
	    return checkConnected (root.me);
	} else {
	    return checkConnected (root.adv);
	}
    }

    public boolean whiteConnected () {
	if (root.black == 0) {
	    return checkConnected (root.me);
	} else {
	    return checkConnected (root.adv);
	}
    }

    public void setWeights (double[] weights) {
	this.weights = weights;
	transposition.clear ();
    }

    double[] getWeights () {
	return weights;
    }

    public void print () {
	root.print ();
	System.out.println ("Euler: white="+(euler4Number[0]/4)+", black="+(euler4Number[1]/4));
	System.out.println ("Q3: white="+(q3Number[0])+", black="+(q3Number[1]));
	System.out.println ("Q4: white="+(q4Number[0])+", black="+(q4Number[1]));
    }

    public LOA2 (int black, double[] weights) {
	reset (black, weights);
    }

    public void reset (int black, double[] weights) {
	setWeights (weights);

	long w, b;
        w = 36452665219186944L;
        b = 9079256848778920062L;
	// white moves first
	if (black == 0) {
	    root = new Node (black, w, b, 0);
	} else {
	    root = new Node (black, b, w, 0);
	}
	root.computeLines ();
        root.computeCentralisation ();
	root.computeQuads ();
	root.computeComMe ();
	root.computeComAdv ();
	root.computeComDistancesMe ();
	root.computeComDistancesAdv ();
	transposition.clear ();
    }

    public void run () {
        // FIXME: il mutex non servirà a fine progetto
        try {
          inputMutex.acquire ();
        } catch (Exception e) {
          e.printStackTrace ();
        }

        SearchThread search = new SearchThread ();
        search.start ();

        Scanner sc = new Scanner (System.in);

        while (true) {
            Node tmp;
            while (true) {
                int iSource = sc.nextInt ();
                int jSource = sc.nextInt ();

                int iDest=sc.nextInt();
                int jDest=sc.nextInt();

		tmp = root.findmove (iSource, jSource,iDest,jDest, false);

                if (tmp == null) {
                    System.out.println ("ILLEGAL MOVE");
                } else {
                    break;
                }
            }
            root = tmp;

            changedRoot = true;
            try {
                inputMutex.release ();
                inputMutex.acquire ();
            } catch (Exception e) {
                e.printStackTrace ();
            }
        }
    }
}
