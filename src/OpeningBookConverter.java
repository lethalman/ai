import java.io.*;
import java.util.*;
import ai.BookEntry;

/* USAGE: GMOpeningBookFilename ALOAOpeningBookFilename
   Converte l'opening book dal formato di GM in quello di ALOA
*/

class Node {
    int black;
    long me;
    long adv;
    OpeningBookEntry entry;
    int move;

    private static final double base2 = Math.log (2);

    private final static long[][] moveMasks = new long[8*8][4];
    private final static int[] steps = new int[] {
       8, // vertical
       1, // horizontal
       9, // main diagonal
       7, // secondary diagonal
    };

    private static final long k1 = 0x5555555555555555L;
    private static final long k2 = 0x3333333333333333L;
    private static final long k4 = 0x0f0f0f0f0f0f0f0fL;
    private static final long h01 = 0x0101010101010101L;

    private static int popcount (long x) {
	x -= (x >>> 1) & k1;
	x = (x & k2) + ((x >>> 2) & k2);
	x = (x + (x >>> 4)) & k4;
	return (int)((x * h01) >>> 56);
    }

    static {
	for (int i=0; i < 8; i++) {
	    for (int j=0; j < 8; j++) {
		long[] m = moveMasks[(i<<3)+j];
		// vertical
		for (int ii=0; ii < 8; ii++) m[0] |= 1L << ((ii<<3)+j);
		// horizontal
		for (int jj=0; jj < 8; jj++) m[1] |= 1L << ((i<<3)+jj);
		// main diagonal
		for (int ii=Math.max(0,i-j), jj=Math.max(0,j-i); ii < 8 && jj < 8; ii++, jj++) m[2] |= 1L << ((ii<<3)+jj);
		// secondary diagonal
		for (int ii=Math.max(0,i-7+j), jj=Math.min(7,j+i); ii < 8 && jj >= 0; ii++, jj--) m[3] |= 1L << ((ii<<3)+jj);
	    }
	}
    }

    public Node (int black, long me, long adv, int move) {
	this.black = black;
	this.me = me;
	this.adv = adv;
	this.move = move;
    }

    ArrayList<Node> children;

    ArrayList<Node> children () {
	if (children != null) {
	    return children;
	}

	ArrayList<Node> r = new ArrayList<Node> ();

	if (getEntry() == null) {
	    return r;
	}

	if (getEntry().depth == 1) {
	    int from = getEntry().best_Move & 63;
	    int to = (getEntry().best_Move >> 6) & 63;
	    int nfrom = (from % 8)*8 + ((from / 8));
	    int nto = (to % 8)*8 + ((to / 8));
	    long movemask = (1L << nfrom) | (1L << nto);
	    r.add (new Node (1-black, adv & ~movemask, me ^ movemask, nfrom | (nto << 6)));
	    return r;
	}

	long occupied = me | adv;
	long tmp = me;
	while (tmp != 0) {
	    // find our first occupied position
	    long posbit = tmp & -tmp;
	    // unset the position for the next cycle
	    tmp &= ~posbit;
	    int pos = (int)(Math.log (((double)(posbit-1))+1) / base2);
	    long[] masks = moveMasks[pos];
	    // destination from this position
	    for (int dir=0; dir < 4; dir++) {
		long mask = masks[dir];
		long match = mask & occupied;
		int stones = popcount (match);

		// first direction, >>> is to avoid sign extension
		int step = steps[dir]*stones;
		long newpos = (posbit >>> step) & mask;
		if (newpos != 0 && (newpos & me) == 0) {
		    long middleadv = ((~((newpos<<1)-1)) & (posbit-1)) & (adv & mask);
		    if (middleadv == 0) {
			long movemask = posbit | newpos;
			int dest = (int)(Math.log (((double)(newpos-1))+1) / base2);
			addChild (r, new Node (1-black, adv & ~movemask, me ^ movemask, pos | (dest << 6)));
		    }
		}
		newpos = (posbit << step) & mask;
		if (newpos != 0 && (newpos & me) == 0) {
		    long middleadv = ((~((posbit<<1)-1)) & (newpos-1)) & (adv & mask);
		    if (middleadv == 0) {
			long movemask = posbit | newpos;
			int dest = (int)(Math.log (((double)(newpos-1))+1) / base2);
			addChild (r, new Node (1-black, adv & ~movemask, me ^ movemask, pos | (dest << 6)));
		    }
		}
	    }
	}
	Collections.<Node>sort (r, black == 1 ? blackComparator : whiteComparator);
	children = r;
	return r;
    }

    void addChild (ArrayList<Node> r, Node child) {
	if (child.getEntry () != null) {
	    r.add (child);
	}
    }

    void print () {
	System.out.println ("---");
	System.out.println ("Bitboard: me="+me+"L; adv="+adv+"L");
	for (int i=0; i < 8; i++) {
	    for (int j=0; j < 8; j++) {
		System.out.print (" ");
		long player = ((me >> (i*8+j)) & 1) | (((adv >> (i*8+j)) & 1) << 1);
		System.out.print (" "+(player==0?'.':((((player&1)^black)==0?'X':'O'))));
	    }
	    System.out.println ();
	}
    }

    private static long hmirror (long b) {
	return ((b >>> 1) & 0x808080808080808L) |
	    ((b >>> 3) & 0x404040404040404L) |
	    ((b >>> 5) & 0x202020202020202L) |
	    ((b >>> 7) & 0x101010101010101L) |
	    ((b << 7) & 0x8080808080808080L) |
	    ((b << 5) & 0x4040404040404040L) |
	    ((b << 3) & 0x2020202020202020L) |
	    ((b << 1) & 0x1010101010101010L);
    }

    private static long vmirror (long b) {
	return ((b >>> (7*8)) & 0xFFL) |
	    ((b >>> (5*8)) & 0xFF00L) |
	    ((b >>> (3*8)) & 0xFF0000L) |
	    ((b >>> (1*8)) & 0xFF000000L) |
	    ((b << (7*8)) & 0xFF00000000000000L) |
	    ((b << (5*8)) & 0xFF000000000000L) |
	    ((b << (3*8)) & 0xFF0000000000L) |
	    ((b << (1*8)) & 0xFF00000000L);
    }

    public static short hmirrorMove (short move) {
	int from_row = move & 0x7;
	int from_col = (move >>> 3) & 0x7;
	int to_row = (move >>> 6) & 0x7;
	int to_col = move >>> 9;
	from_col = (7 - from_col);
	to_col = (7 - to_col);
	return (short) (from_row | from_col << 3 | to_row << 6 | to_col << 9);
    }

    public static short vmirrorMove (short move) {
	int from_row = move & 0x7;
	int from_col = (move >>> 3) & 0x7;
	int to_row = (move >>> 6) & 0x7;
	int to_col = move >>> 9;
	from_row = (7 - from_row);
	to_row = (7 - to_row);
	return (short) (from_row | from_col << 3 | to_row << 6 | to_col << 9);
    }

    OpeningBookEntry getEntry () {
	if (entry != null) {
	    return entry;
	}
	OpeningBookEntry e = new OpeningBookEntry ();
	e.posB = black == 1 ? me : adv;
	e.posR = black == 1 ? adv : me;
	entry = OpeningBookConverter.bookEntries.get (e);
	if (entry != null) {
	    return entry;
	}
	e.posB = hmirror (e.posB);
	e.posR = hmirror (e.posR);
	entry = OpeningBookConverter.bookEntries.get (e);
	if (entry != null) {
	    entry = entry.copy ();
	    entry.best_Move = hmirrorMove (entry.best_Move);
	    return entry;
	}

	e.posB = hmirror (e.posB);
	e.posR = hmirror (e.posR);
	e.posB = vmirror (e.posB);
	e.posR = vmirror (e.posR);
	entry = OpeningBookConverter.bookEntries.get (e);
	if (entry != null) {
	    entry = entry.copy ();
	    entry.best_Move = vmirrorMove (entry.best_Move);
	    return entry;
	}

	e.posB = hmirror (e.posB);
	e.posR = hmirror (e.posR);
	entry = OpeningBookConverter.bookEntries.get (e);
	if (entry != null) {
	    entry = entry.copy ();
	    entry.best_Move = hmirrorMove (vmirrorMove (entry.best_Move));
	    return entry;
	}

	return null;
    }

    public static HashMap<Node, Node> openBook = new HashMap<Node, Node> ();

    public static Comparator<Node> blackComparator = new Comparator<Node> () {
	@Override
	public int compare (Node a, Node b) {
	    if (a.getEntry().Value < b.getEntry().Value) {
		return 1;
	    } else if (a.getEntry().Value > b.getEntry().Value) {
		return -1;
	    } else {
		return 0;
	    }
	}
    };

    public static Comparator<Node> whiteComparator = new Comparator<Node> () {
	@Override
	public int compare (Node a, Node b) {
	    return blackComparator.compare (b, a);
	}
    };

    @Override
    public int hashCode () {
	return (int)((me | adv) ^ (~(me | adv) >>> 32));
    }   

    @Override
    public boolean equals (Object o) {
	Node n = (Node) o;
	return (black == n.black && me == n.me && adv == n.adv);
    }
}

class OpeningBookEntry implements Serializable {
    private static final long serialVersionUID = -137771967928656700L;
    public long hash;
    public byte depth;
    public short Value;
    public short best_Move;
    public OpeningBookEntry next;
    public long posB;
    public long posR;

    public OpeningBookEntry copy () {
	OpeningBookEntry be = new OpeningBookEntry ();
	be.depth = depth;
	be.Value = Value;
	be.best_Move = best_Move;
	be.posB = posB;
	be.posR = posR;
	return be;
    }

    @Override
    public boolean equals (Object o) {
	OpeningBookEntry n = (OpeningBookEntry) o;
	return (posB == n.posB && posR == n.posR);
    }

    @Override
    public int hashCode () {
	return (int)((posB | posR) ^ (~(posB | posR) >>> 32));
    }
}

class OpeningBookTable implements Serializable {
    private static final long serialVersionUID = 8140495183635111979L;
    public OpeningBookEntry[] book;
    public int storedPositions = 0;
}

public class OpeningBookConverter {
    public static HashMap<OpeningBookEntry, OpeningBookEntry> bookEntries = new HashMap<OpeningBookEntry, OpeningBookEntry> ();

    public static OpeningBookTable load (String filename) {
	try {
	    ObjectInputStream is = new ObjectInputStream (new FileInputStream (filename));
	    OpeningBookTable o = (OpeningBookTable) is.readObject ();
	    is.close ();
	    return o;
	} catch (Exception e) {
	    e.printStackTrace ();
	    return null;
	}
    }

    public static void printboard (long b) {
	System.out.println ("bitboard: "+b);
	for (int i=0; i < 8; i++) {
	    for (int j=0; j < 8; j++) {
		System.out.print (" "+((b >> (i*8+j))&1));
	    }
	    System.out.println ();
	}
    }

    static void printentry (OpeningBookEntry e) {
	if (e == null) {
	    return;
	}
	System.out.println ("B");
	printboard (e.posB);
	System.out.println ("R");
	printboard (e.posR);
	System.out.println ("Value: "+e.Value);
	System.out.println ("Best move: "+e.best_Move);
    }

    public static void main (String[] args) {
	OpeningBookTable book = load (args[0]);
	System.out.println ("stored positions: "+book.storedPositions);
	for (OpeningBookEntry e: book.book) {
	    OpeningBookEntry f = e;
	    while (f != null) {
		bookEntries.put (f, f);
		f = f.next;
	    }
	}
	System.out.println ("effective positions: "+bookEntries.size ());

	Node root = new Node (1, 9079256848778920062L, 36452665219186944L, 0);
	//dump (root, 0);
	export (root, args[1]);
    }

    private static String moveToString (int move) {
	int from = move & 63;
	int to = (move >> 6) & 63;
	char fromcol = (char)((from%8) + 'A');
	char fromrow = (char)((7-from/8) + '1');
	char tocol = (char)((to%8) + 'A');
	char torow = (char)((7-to/8) + '1');
	return ""+fromcol+fromrow+"-"+tocol+torow;
    }

    static void dump (Node n, int depth) {
	for (Node child: n.children ()) {
	    for (int i=0; i < depth; i++) {
		System.out.print ("-");
	    }
	    OpeningBookEntry e = child.getEntry ();
	    System.out.println (" "+moveToString (child.move));
	    dump (child, depth+1);
	}
    }

    static void exportRecursive (Node n, HashMap<BookEntry, Integer> book) {
	OpeningBookEntry e = n.getEntry ();
	if (e != null) {
	    BookEntry be = new BookEntry ();
	    be.black = n.black;
	    be.me = n.me;
	    be.adv = n.adv;
	    // convert best move
	    int gmfrom = e.best_Move & 63;
	    int gmto = (e.best_Move >> 6) & 63;
	    int from = (gmfrom % 8)*8 + ((gmfrom / 8));
	    int to = (gmto % 8)*8 + ((gmto / 8));
	    book.put (be, from | (to << 6));
	}
	for (Node child: n.children ()) {
	    exportRecursive (child, book);
	}
    }

    static void export (Node n, String filename) {
	HashMap<BookEntry, Integer> book = new HashMap<BookEntry, Integer> ();
	exportRecursive (n, book);
	System.out.println ("Writing...");
	try {
	    ObjectOutputStream os = new ObjectOutputStream (new FileOutputStream (filename));
	    os.writeObject (book);
	    os.close ();
	    System.out.println ("Book size: "+book.size ());
	} catch (Exception e) {
	    e.printStackTrace ();
	}
    }
}
