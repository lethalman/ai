import java.io.*;
import java.util.*;
import ai.BookEntry;

/* USAGE: nomefile mossa1 mossa2 ... mossaN mossamigliore
   Inserisce nell'opening book "nomefile" lo stato ottenuto
   dall'applicazione delle N mosse con la mossa migliore specificata
*/
public class OpeningBookInserter {
    public static int stringToMove (String s) {
	int fromcol = (int)(s.charAt (0) - 'A');
	int fromrow = (int)(7 - (s.charAt (1) - '1'));
	int tocol = (int)(s.charAt (3) - 'A');
	int torow = (int)(7 - (s.charAt (4) - '1'));
	return (fromrow*8+fromcol) | ((torow*8+tocol) << 6);
    }

    private static String moveToString (int move) {
	int from = move & 63;
	int to = (move >> 6) & 63;
	char fromcol = (char)((from%8) + 'A');
	char fromrow = (char)((7-from/8) + '1');
	char tocol = (char)((to%8) + 'A');
	char torow = (char)((7-to/8) + '1');
	return ""+fromcol+fromrow+"-"+tocol+torow;
    }

    static void print (BookEntry be) {
	System.out.println ("---");
	System.out.println ("Bitboard: me="+be.me+"L; adv="+be.adv+"L");
	for (int i=0; i < 8; i++) {
	    for (int j=0; j < 8; j++) {
		System.out.print (" ");
		long player = ((be.me >> (i*8+j)) & 1) | (((be.adv >> (i*8+j)) & 1) << 1);
		System.out.print (" "+(player==0?'.':((((player&1)^be.black)==0?'X':'O'))));
	    }
	    System.out.println ();
	}
    }

    private static long hmirror (long b) {
	return ((b >>> 1) & 0x808080808080808L) |
	    ((b >>> 3) & 0x404040404040404L) |
	    ((b >>> 5) & 0x202020202020202L) |
	    ((b >>> 7) & 0x101010101010101L) |
	    ((b << 7) & 0x8080808080808080L) |
	    ((b << 5) & 0x4040404040404040L) |
	    ((b << 3) & 0x2020202020202020L) |
	    ((b << 1) & 0x1010101010101010L);
    }

    private static long vmirror (long b) {
	return ((b >>> (7*8)) & 0xFFL) |
	    ((b >>> (5*8)) & 0xFF00L) |
	    ((b >>> (3*8)) & 0xFF0000L) |
	    ((b >>> (1*8)) & 0xFF000000L) |
	    ((b << (7*8)) & 0xFF00000000000000L) |
	    ((b << (5*8)) & 0xFF000000000000L) |
	    ((b << (3*8)) & 0xFF0000000000L) |
	    ((b << (1*8)) & 0xFF00000000L);
    }

    public static int hmirrorMove (int move) {
	int from = move & 63;
	int to = (move >> 6) & 63;
	int from_row = from / 8;
	int from_col = from % 8;
	int to_row = to / 8;
	int to_col = to % 8;
	from_col = (7 - from_col);
	from = from_row*8 + from_col;
	to_col = (7 - to_col);
	to = to_row*8 + to_col;
	return from | (to << 6);
    }

    public static int vmirrorMove (int move) {
	int from = move & 63;
	int to = (move >> 6) & 63;
	int from_row = from / 8;
	int from_col = from % 8;
	int to_row = to / 8;
	int to_col = to % 8;
	from_row = (7 - from_row);
	from = from_row*8 + from_col;
	to_row = (7 - to_row);
	to = to_row*8 + to_col;
	return from | (to << 6);
    }

    public static void main (String[] args) {
	try {
	    ObjectInputStream is = new ObjectInputStream (new FileInputStream (args[0]));
	    HashMap<BookEntry, Integer> book = (HashMap<BookEntry, Integer>) is.readObject ();
	    is.close ();

	    BookEntry be = new BookEntry ();
	    be.black = 1;
	    be.me = 9079256848778920062L;
	    be.adv = 36452665219186944L;
	    for (int i=1; i < args.length-1; i++) {
		int move = stringToMove (args[i]);
		int from = move & 63;
		int to = (move >> 6) & 63;
		long movemask = (1L << from) | (1L << to);
		be.black = 1-be.black;
		long adv = be.adv;
		be.adv = be.me ^ movemask;
		be.me = adv &~ movemask;
	    }
	    int bestMove = stringToMove (args[args.length-1]);
	    System.out.println ("INSERTED MOVE "+moveToString (bestMove)+" FROM:");
	    print (be);
	    book.put (be, bestMove);
	    // mirror horizontally
	    be = be.copy ();
	    be.me = hmirror (be.me);
	    be.adv = hmirror (be.adv);
	    System.out.println ("INSERTED MOVE "+moveToString (hmirrorMove (bestMove))+" FROM:");
	    print (be);
	    book.put (be, hmirrorMove (bestMove));
	    // mirror vertically
	    be = be.copy ();
	    be.me = hmirror (be.me);
	    be.adv = hmirror (be.adv);
	    be.me = vmirror (be.me);
	    be.adv = vmirror (be.adv);
	    System.out.println ("INSERTED MOVE "+moveToString (vmirrorMove (bestMove))+" FROM:");
	    print (be);
	    book.put (be, vmirrorMove (bestMove));
	    // mirror both
	    be = be.copy ();
	    be.me = hmirror (be.me);
	    be.adv = hmirror (be.adv);
	    System.out.println ("INSERTED MOVE "+moveToString (hmirrorMove (vmirrorMove (bestMove)))+" FROM:");
	    print (be);
	    book.put (be, hmirrorMove (vmirrorMove (bestMove)));

	    System.out.println ("WRITING...");
	    ObjectOutputStream os = new ObjectOutputStream (new FileOutputStream (args[0]));
	    os.writeObject (book);
	    os.close ();
	    System.out.println ("NEW OPENING BOOK SIZE: "+book.size ());
	} catch (Exception e) {
	    e.printStackTrace ();
	}
    }
}
