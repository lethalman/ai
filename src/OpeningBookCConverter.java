import java.io.*;
import java.util.*;
import ai.BookEntry;

/* USAGE: JALOAOpeningBookFilename CALOAOpeningBookFilename
   Converte l'opening book dal formato di Java in quello di C
*/

public class OpeningBookCConverter {
    public static void main (String[] args) {
	try {
	    ObjectInputStream is = new ObjectInputStream (new FileInputStream (args[0]));
	    HashMap<BookEntry, Integer> book = (HashMap<BookEntry, Integer>) is.readObject ();
	    is.close ();

	    DataOutputStream os = new DataOutputStream (new FileOutputStream (args[1]));
	    for (Map.Entry<BookEntry, Integer> e: book.entrySet ()) {
		BookEntry be = e.getKey ();
		Integer i = e.getValue ();
		os.writeBoolean (be.black == 1);
		os.writeLong (be.me);
		os.writeLong (be.adv);
		os.writeInt (i);
	    }
	    os.close ();
	} catch (Exception e) {
	    e.printStackTrace ();
	}
    }
}
