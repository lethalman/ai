
TOPDIR = .
SRCDIR = $(TOPDIR)/src
BINDIR = $(TOPDIR)/bin
JAVAC = javac
JAVA = java
CLASSPATH = $(BINDIR)
MAINCLASS = $(BINDIR)/ai/Main.class
MAINSRCS = $(wildcard $(SRCDIR)/ai/Main.java)
MAIN = ai.Main

OTHELLOCLASS = $(BINDIR)/ai/Othello.class
OTHELLOSRCS = $(wildcard $(SRCDIR)/ai/Othello.java)
OTHELLO = ai.Othello

OTHELLOBITCLASS = $(BINDIR)/ai/OthelloBit.class
OTHELLOBITSRCS = $(wildcard $(SRCDIR)/ai/OthelloBit.java)
OTHELLOBIT = ai.OthelloBit

OTHELLOTIMECLASS = $(BINDIR)/ai/OthelloTime.class
OTHELLOTIMESRCS = $(wildcard $(SRCDIR)/ai/OthelloTime.java)
OTHELLOTIME = ai.OthelloTime

ALOACLASS = $(BINDIR)/ai/ALOA.class
ALOASRCS = $(SRCDIR)/ai/ALOA.java $(SRCDIR)/ai/BookEntry.java $(SRCDIR)/OpeningBookConverter.java $(SRCDIR)/OpeningBookInserter.java $(SRCDIR)/OpeningBookCConverter.java
ALOA = ai.ALOA

LOA2CLASS = $(BINDIR)/ai/LOA2.class
LOA2SRCS = $(wildcard $(SRCDIR)/ai/LOA2.java)
LOA2 = ai.LOA2

LEARNERCLASS = $(BINDIR)/ai/Learner.class
LEARNERSRCS = $(wildcard $(SRCDIR)/ai/Learner.java)
LEARNER = ai.Learner

LOANCLASS = $(BINDIR)/ai/LOAN.class
LOANSRCS = $(wildcard $(SRCDIR)/ai/LOAN.java)
LOAN = ai.LOAN

all: $(ALOACLASS) $(LOA2CLASS) $(LEARNERCLASS)

$(MAINCLASS): $(MAINSRCS)
	mkdir -p $(BINDIR)
	$(JAVAC) -d $(BINDIR) -sourcepath $(SRCDIR) -g $(MAINSRCS)

runmain: $(MAINCLASS)
	$(JAVA) -cp $(CLASSPATH) $(MAIN)

$(OTHELLOCLASS): $(OTHELLOSRCS)
	mkdir -p $(BINDIR)
	$(JAVAC) -d $(BINDIR) -sourcepath $(SRCDIR) -g $(OTHELLOSRCS)

othello: $(OTHELLOCLASS)
	$(JAVA) -cp $(CLASSPATH) $(OTHELLO)

$(OTHELLOBITCLASS): $(OTHELLOBITSRCS)
	mkdir -p $(BINDIR)
	$(JAVAC) -d $(BINDIR) -sourcepath $(SRCDIR) -g $(OTHELLOBITSRCS)

$(OTHELLOTIMECLASS): $(OTHELLOTIMESRCS)
	mkdir -p $(BINDIR)
	$(JAVAC) -d $(BINDIR) -sourcepath $(SRCDIR) -g $(OTHELLOTIMESRCS)

$(ALOACLASS): $(ALOASRCS)
	mkdir -p $(BINDIR)
	$(JAVAC) -d $(BINDIR) -sourcepath $(SRCDIR) -g $(ALOASRCS)

$(LOA2CLASS): $(LOA2SRCS)
	mkdir -p $(BINDIR)
	$(JAVAC) -d $(BINDIR) -sourcepath $(SRCDIR) -g $(LOA2SRCS)

$(LEARNERCLASS): $(LEARNERSRCS)
	mkdir -p $(BINDIR)
	$(JAVAC) -d $(BINDIR) -sourcepath $(SRCDIR) -g $(LEARNERSRCS)

$(LOANCLASS): $(LOANSRCS)
	mkdir -p $(BINDIR)
	$(JAVAC) -d $(BINDIR) -sourcepath $(SRCDIR) -g $(LOANSRCS)

run: $(ALOACLASS)
	$(JAVA) -Xmx300m -Xms300m -cp $(CLASSPATH) $(ALOA)

$(BINDIR)/othelloc: $(SRCDIR)/ai/OthelloBit.c
	gcc -o $(BINDIR)/othelloc $(SRCDIR)/ai/OthelloBit.c -lm -g -Ofast -lrt -fomit-frame-pointer

runc: $(BINDIR)/othelloc
	$(BINDIR)/othelloc
